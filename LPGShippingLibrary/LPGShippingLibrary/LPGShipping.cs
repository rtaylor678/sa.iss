﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;


namespace LPGShipping
{
    //TABLE 1
    public class LPGShipping_Company_Information
    {
        public string CompanyName { get; set; }
        public int CompanyID { get; set; }
        public string Location { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }


    //TABLE 1
    public class LPGShipping_T1_FleetInformation
    {
        public int UnitID { get; set; }
        public string CompanyID { get; set; }
        public string ShipName { get; set; }
        public string ShipType { get; set; }
        public string ShipSubType { get; set; }
        public string PrimaryService { get; set; }
        public string Flag { get; set; }
        public int? CapacityDWT { get; set; }
        public int? CapacityCBM { get; set; }
        public int? NumTanks { get; set; }
        public string TankType { get; set; }
        public string LGCGroups { get; set; }
        public int? PressureMAX { get; set; }
        public int? TempMIN { get; set; }

    }

    //TABLE 2
    public class LPGShipping_T2_ShipConfiguration
    {
        public int SubmissionID { get; set; }
        public string CompanyID { get; set; }
        public int UnitID { get; set; }
        public string RefrigerationType { get; set; }
        public int? TotalRecompressionHP { get; set; }
        public int? CompressorCount { get; set; }
        public string RecompressionDriveType { get; set; }
        public int? NumMainEngines { get; set; }
        public int? NumAuxEngines { get; set; }
        public int? NumGenerators { get; set; }
        public int? NumAddGenerators { get; set; }
        public int? NumEvaporators { get; set; }
        public int? NumWaterProdUnits { get; set; }
        public int? NumCargoPumps { get; set; }
    }

    //TABLE2
    public class LPGShipping_T2_Engines
    {
        public int SubmissionID { get; set; }
        public string CompanyID { get; set; }
        public int UnitID { get; set; }
        public int EngineNo { get; set; }
        public int? Horsepower { get; set; }
        public string EngineType { get; set; }
        public string Fuel { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public Boolean Auxillary { get; set; }
    }


    //TABLE 2
    public class LPGShipping_T2_Generators
    {
        public int SubmissionID { get; set; }
        public string CompanyID { get; set; }
        public int? UnitID { get; set; }
        public int? GeneratorNo { get; set; }
        public int? Kilowatts { get; set; }
        public string Fuel { get; set; }
        public Boolean Additional { get; set; }
    }


    //TABLE 2
    public class LPGShipping_T2_Evaporators
    {
        public int SubmissionID { get; set; }
        public string CompanyID { get; set; }
        public int? UnitID { get; set; }
        public int? EvaporatorNo { get; set; }
        public int? CapacityGPM { get; set; }
        public string EvaporatorType { get; set; }
        public Boolean OtherWaterProductionUnit { get; set; }
    }

    //TABLE 2
    public class LPGShipping_T2_CargoPumps
    {
        public int SubmissionID { get; set; }
        public string CompanyID { get; set; }
        public int? UnitID { get; set; }
        public int? CargoPumpNo { get; set; }
        public string PumpType { get; set; }
        public int? FlowRate { get; set; }
        public string MotorType { get; set; }
        public int? Horsepower { get; set; }
    }

    //TABLE 3
    public class LPGShipping_ShipData
    {
        public int SubmissionID { get; set; }
        public string CompanyID { get; set; }
        public int? UnitID { get; set; }
        public string ShipName { get; set; }

        public int? RSCRUDE_RailcarBBL { get; set; }
        public string RSCRUDE_RailcarProductType { get; set; }
        public int? RSCRUDE_BargeBerthBBL { get; set; }
        public string RSCRUDE_BargeBerthProductType { get; set; }
        public int? RSCRUDE_TankerBerthBBL { get; set; }
        public string RSCRUDE_TankerBerthProductType { get; set; }
        public int? RSCRUDE_GasCarrierBBL { get; set; }
        public string RSCRUDE_GasCarrierProductType { get; set; }
        public int? RSCRUDE_OffshoreBuoyBBL { get; set; }
        public string RSCRUDE_OffshoreBuoyProductType { get; set; }
        public int? RSCRUDE_OtherBBL { get; set; }
        public string RSCRUDE_OtherProductType { get; set; }

        public int? RSPROD_RailcarBBL { get; set; }
        public string RSPROD_RailcarProductType { get; set; }
        public int? RSPROD_BargeBerthBBL { get; set; }
        public string RSPROD_BargeBerthProductType { get; set; }
        public int? RSPROD_TankerBerthBBL { get; set; }
        public string RSPROD_TankerBerthProductType { get; set; }
        public int? RSPROD_GasCarrierBBL { get; set; }
        public string RSPROD_GasCarrierProductType { get; set; }
        public int? RSPROD_OffshoreBuoyBBL { get; set; }
        public string RSPROD_OffshoreBuoyProductType { get; set; }
        public int? RSPROD_OtherBBL { get; set; }
        public string RSPROD_OtherProductType { get; set; }

        public int? SeaMilesLoaded { get; set; }
        public int? SeaMilesLoadedTimeSpent { get; set; }
        public int? SeaMilesLoadedTimeAuthorized { get; set; }
        public int? SeaMilesUnloaded { get; set; }
        public int? SeaMilesUnloadedTimeSpent { get; set; }
        public int? SeaMilesUnloadedTimeAuthorized { get; set; }

        public int? LayTimeLoading { get; set; }
        public int? LayTimeUnloading { get; set; }
        public int? DelayVesselLoading { get; set; }
        public int? DelayVesselUnloading { get; set; }
        public int? DelayTerminalLoading { get; set; }
        public int? DelayTerminalUnloading { get; set; }
        public int? UncontrollableDelayLoading { get; set; }
        public int? UncontrollableDelayUnloading { get; set; }
        public int? DemurrageClaimedLoading { get; set; }
        public int? DemurrageClaimedLoadingAmount { get; set; }
        public int? DemurrageClaimedUnloading { get; set; }
        public int? DemurrageClaimedUnloadingAmount { get; set; }
        public int? DemurragePaidLoading { get; set; }
        public int? DemurragePaidLoadingCost { get; set; }
        public int? DemurragePaidUnloading { get; set; }
        public int? DemurragePaidUnloadingCost { get; set; }

        public int? DryDockHours { get; set; }

        //TABLE 4,5 & 6
        public int? MaintCompanyHours { get; set; }
        public int? MaintCompanyCost { get; set; }
        public int? MaintContractorHours { get; set; }
        public int? MaintContractorCost { get; set; }
        public int? NonDryDockMaintCost { get; set; }
        public int? NonMaintCompanyHours { get; set; }
        public int? NonMaintCompanyCost { get; set; }
        public int? NonMaintContractorHours { get; set; }
        public int? NonMaintContractorCost { get; set; }
        public int? AdminGAFeesHours { get; set; }
        public int? AdminGAFeesCost { get; set; }
        public int? RegistrationCost { get; set; }
        public int? SundriesCost { get; set; }
        public int? InsurancePICost { get; set; }
        public int? InsuranceMarineCost { get; set; }
        public int? StoresLubeCost { get; set; }
        public int? StoresOtherCost { get; set; }
        public int? CrewProvisionsCost { get; set; }
        public int? CrewOtherCost { get; set; }
        public int? MiscCost { get; set; }
        public string MiscCostComments { get; set; }
        public int? TotalNonEnergyOpex { get; set; }

        //TABLE 8
        public string EnergyType1VolumeUnit { get; set; }
        public string EnergyType2VolumeUnit { get; set; }
        public string EnergyType3VolumeUnit { get; set; }

        public int? EnergyType1Volume { get; set; }
        public int? EnergyType2Volume { get; set; }
        public int? EnergyType3Volume { get; set; }

        public int? EnergyType1AvgCost { get; set; }
        public int? EnergyType2AvgCost { get; set; }
        public int? EnergyType3AvgCost { get; set; }

        public int? EnergyType1Cost { get; set; }
        public int? EnergyType2Cost { get; set; }
        public int? EnergyType3Cost { get; set; }

        public int? EnergyType1VolumeBTU { get; set; }
        public int? EnergyType2VolumeBTU { get; set; }
        public int? EnergyType3VolumeBTU { get; set; }

        public int? EnergyType1BTU { get; set; }
        public int? EnergyType2BTU { get; set; }
        public int? EnergyType3BTU { get; set; }

        public string CargoBurnedVolumeUnit { get; set; }
        public string BunkersSoldVolumeUnit { get; set; }
        public string OtherFuelAcquiredVolumeUnit { get; set; }
        public string OtherFuelSoldVolumeUnit { get; set; }

        public int? CargoBurnedVolume { get; set; }
        public int? BunkersSoldVolume { get; set; }
        public int? OtherFuelAcquiredVolume { get; set; }
        public int? OtherFuelSoldVolume { get; set; }

        public int? CargoBurnedAvgCost { get; set; }
        public int? BunkersSoldAvgCost { get; set; }
        public int? OtherFuelAcquiredAvgCost { get; set; }
        public int? OtherFuelSoldAvgCost { get; set; }

        public int? CargoBurnedCost { get; set; }
        public int? BunkersSoldCost { get; set; }
        public int? OtherFuelAcquiredCost { get; set; }
        public int? OtherFuelSoldCost { get; set; }

        public int? CargoBurnedVolumeBTU { get; set; }
        public int? BunkersSoldVolumeBTU { get; set; }
        public int? OtherFuelAcquiredVolumeBTU { get; set; }
        public int? OtherFuelSoldVolumeBTU { get; set; }

        public int? CargoBurnedBTU { get; set; }
        public int? BunkersSoldBTU { get; set; }
        public int? OtherFuelAcquiredBTU { get; set; }
        public int? OtherFuelSoldBTU { get; set; }

        public int? TotalFuelCost { get; set; }
        public int? TotalFuelEnergy { get; set; }


        public int? PurchasedElectricityMW { get; set; }
        public int? PurchasedElectricityCost { get; set; }
        public int? VesselProducedElectricityMW { get; set; }
        public int? VesselProducedElectricityCost { get; set; }
        public int? VesselProducedElectricityMJ { get; set; }
        public int? SoldElectricityMW { get; set; }
        public int? SoldElectricityCost { get; set; }


        //TABLE 10
        public int? TotalHrsRegulatoryDowntime { get; set; }
        public int? TotalHrsScheduledDowntime { get; set; }
        public int? TotalHrsMechanicalDowntime { get; set; }
        public int? OtherDowntime { get; set; }
        public string OtherDowntimeComments { get; set; }

        //TABLE 11
        public int? TotalPhysicalHydrocarbonHauled { get; set; }
        public int? TotalPhysicalHydrocarbonLoss { get; set; }
        public int? TotalPhysicalHydrocarbonLossPercent { get; set; }
        public int? EstimatedVentingLoss { get; set; }
        public int? EstimatedVentingLossPercent { get; set; }
        public string MonthlyDataNotes { get; set; }
    }


    public class LPGShipping_Utilization
    {
        public int SubmissionID { get; set; }
        public string CompanyID { get; set; }
        public int? UnitID { get; set; }
        public string ShipName { get; set; }
        public string Month { get; set; }
        public int? Year { get; set; }
        public int? ShipCapacityCBM { get; set; }
        public int? ShipHoursReported { get; set; }
        public decimal? ShipCapacityCBMHR { get; set; }
        public int? Trips { get; set; }
        public int? TotalAuthVolumeCBM { get; set; }
        public int? TotalAuthorizedHrs { get; set; }
        public int? TotalAuthorizedShippingCapacity { get; set; }
        public decimal? TotalRealizedShippingCapacityCBMHR { get; set; }


    }

    public class LPGShipping_UtilizationTrips
    {
        public int SubmissionID { get; set; }
        public string CompanyID { get; set; }
        public int? UnitID { get; set; }
        public string ShipName { get; set; }
        public int? TripNo { get; set; }
        public decimal? VolumeCBM { get; set; }
        public int? AuthorizedHours { get; set; }
        public decimal? AuthorizedShippingCapacityCBMHR { get; set; }
        public int? ActualVoyageHours { get; set; }
        public decimal? RealizedShippingCapacityCBMHR { get; set; }
        public string TripType { get; set; }
        public string Comment { get; set; }

    }


    public class LPGShipping
    {

        public LPGShipping(string cs)
        {
            CurrentLogFile = cs;
        }

        string CurrentLogFile = null;
        public string Error = null;
        string ShipConfigRange = Properties.Settings.Default.ShipConfigRange;
        string FleetInfoRange = Properties.Settings.Default.FleetInfoRange;
        string MainEngineRange = Properties.Settings.Default.MainEngineRange;
        string AuxEngineRange = Properties.Settings.Default.AuxEngineRange;
        string MainGeneratorRange = Properties.Settings.Default.MainGeneratorRange;
        string AddGeneratorRange = Properties.Settings.Default.AddGeneratorRange;
        string EvaporatorRange = Properties.Settings.Default.EvaporatorRange;
        string WaterProdUnitRange = Properties.Settings.Default.WaterProdUnitRange;
        string DryDockMaintRange = Properties.Settings.Default.DryDockMaintRange;
        string NumCargoPumpsRange = Properties.Settings.Default.NumCargoPumpRange;
        string FleetLevelInputSheet = Properties.Settings.Default.FleetLevelInputSheet;
        string NumEnginesRange = Properties.Settings.Default.NumEnginesRange;
        string NumAuxEnginesRange = Properties.Settings.Default.NumAuxEnginesRange;
        string NumGeneratorsRange = Properties.Settings.Default.NumGeneratorsRange;
        string NumAddGeneratorsRange = Properties.Settings.Default.NumAddGeneratorsRange;
        string NumEvaporatorsRange = Properties.Settings.Default.NumEvaporatorsRange;
        string NumWaterProdUnitsRange = Properties.Settings.Default.NumWaterProdUnitsRange;
        string UnitIDRange = Properties.Settings.Default.UnitIDRange;
        string Utilization = Properties.Settings.Default.UtilizationSheet;

        string LPGShippingPath = null;
        public int CompanyID = 0;
        public int SubmissionID = 0;
            

            //Open Excel Application
            Excel.Application excel;
            Excel.Workbook excelWorkbook;

        //Initialize NumberShips Variable
        int NumberShips = 0;

        //Initialize Connection and Command
        SqlConnection cn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();
        

        string CONNECTSTR = Properties.Settings.Default.CONN;
        

        public string ImportLPGShipping(string LPGShippingPath)
        {
            string rtn = "";
            cn.ConnectionString = CONNECTSTR;
            cn.Open();
            
            //Open Workbook
            excel = new Excel.Application();

            excelWorkbook = excel.Workbooks.Open(LPGShippingPath,
                0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "",
                true, false, 0, true, false, false);

            
            

            //Initialize the Objects
            LPGShipping_Company_Information Company = new LPGShipping_Company_Information();
            List<LPGShipping_T1_FleetInformation> Ships = new List<LPGShipping_T1_FleetInformation>();
            List<LPGShipping_T2_ShipConfiguration> Configs = new List<LPGShipping_T2_ShipConfiguration>();
            List<LPGShipping_ShipData> ShipData = new List<LPGShipping_ShipData>();
            List<LPGShipping_T2_Engines> Engines = new List<LPGShipping_T2_Engines>();
            List<LPGShipping_T2_Engines> AuxEngines = new List<LPGShipping_T2_Engines>();
            List<LPGShipping_T2_Generators> Generators = new List<LPGShipping_T2_Generators>();
            List<LPGShipping_T2_Generators> AdditionalGenerators = new List<LPGShipping_T2_Generators>();
            List<LPGShipping_T2_Evaporators> Evaporators = new List<LPGShipping_T2_Evaporators>();
            List<LPGShipping_T2_Evaporators> WaterProdUnits = new List<LPGShipping_T2_Evaporators>();
            List<LPGShipping_T2_CargoPumps> CargoPumps = new List<LPGShipping_T2_CargoPumps>();
            List<LPGShipping_Utilization> ShipUtilization = new List<LPGShipping_Utilization>();
            List<LPGShipping_UtilizationTrips> ShipUtilizationTrips = new List<LPGShipping_UtilizationTrips>();

            Helper.WriteLog("--------------------------------------------------------------------------------------------------------------------------------------------",@"LPGShipping.log");
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": LPGShipping Import Started",CurrentLogFile);
            Company = GetCompanyInformation();
            CompanyID = WriteCompanyInformation(Company);
            
            //Get Next SubmissionID
            SubmissionID = Helper.GetNextSubmissionID(CONNECTSTR, CompanyID);
            Helper.WriteSubmission(CONNECTSTR, SubmissionID, CompanyID);
            //Read Data from Excel to Objects
            Ships = GetFleetInformation();
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Ships Read",CurrentLogFile);
            Configs = GetShipConfiguration();
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Config Read",CurrentLogFile);


            WriteCompanyInformation(Company);
            //Write Objects to Database
            WriteShips(Ships, SubmissionID);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Ships Written to Database",CurrentLogFile);
            WriteShipConfig(Configs);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Configs Written to Database",CurrentLogFile);

            int ShipNo = 1;
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Reading Engines, Generators, Evaporators & Cargo Pumps",CurrentLogFile);
            //Read & Write Engines, Generators, Evaporators and Cargo Pumps
            foreach (LPGShipping_T1_FleetInformation s in Ships)
            {

                Engines.Clear();
                AuxEngines.Clear();
                Generators.Clear();
                AdditionalGenerators.Clear();
                Evaporators.Clear();
                CargoPumps.Clear();
                ShipData.Clear();

                Engines = GetShipMainEngines(ShipNo);
                AuxEngines = GetShipAuxEngines(ShipNo);
                Generators = GetShipGenerators(ShipNo);
                AdditionalGenerators = GetShipAdditionalGenerators(ShipNo);
                Evaporators = GetShipEvaporators(ShipNo);
                WaterProdUnits = GetShipWaterProdUnits(ShipNo);
                CargoPumps = GetCargoPumps(ShipNo);
                if (Helper.WorkSheetExist(s.ShipName + " Data",excelWorkbook))
                {
                    ShipData = GetShipData(s);
                }
                else
                {
                    Error = "{" + s.ShipName + " Data] WorkSheets is Missing";
                }

                if (Helper.WorkSheetExist(s.ShipName + " Utilization", excelWorkbook))
                {
                    ShipUtilization = GetShipUtilization(s);
                    WriteShipUtilization(ShipUtilization);
                    ShipUtilizationTrips = GetShipUtilizationTrips(s);
                    WriteShipUtilizationTrips(ShipUtilizationTrips);
                }
                else
                {
                    Error = "{" + s.ShipName + " Utilization] WorkSheet is Missing";
                }



                WriteEngines(Engines, false);
                WriteEngines(AuxEngines, true);
                WriteGenerators(Generators, false);
                WriteGenerators(AdditionalGenerators, true);
                WriteEvaporators(Evaporators, false);
                WriteEvaporators(WaterProdUnits, true);
                WriteCargoPumps(CargoPumps);
                WriteShipData(ShipData);



                ShipNo++;
                if (ShipNo > NumberShips) break;
            }
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Engines, Generators, Evaporators and Cargo Pumps Written to Database",CurrentLogFile);

            rtn = "Complete";
            Helper.WriteLog("SubmissionID:" + SubmissionID + " : " + ShipNo + " ships imported - LPGShipping Import Complete",CurrentLogFile);

            //Close Connection
            cn.Close();

            //Close and Destroy Excel Object
            excelWorkbook.Close(SaveChanges: false);
            excelWorkbook = null;

            excel = null;
            return rtn;
        }

        

        private void WriteShipData(List<LPGShipping_ShipData> configs)
        {
            
          cmd.CommandText = "LPGShipping_InsertShipData";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (LPGShipping_ShipData config in configs)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", config.UnitID));
                cmd.Parameters.Add(new SqlParameter("@ShipName", (Write(config.ShipName))));
                cmd.Parameters.Add(new SqlParameter("@AdminGAFeesCost", (config.AdminGAFeesCost == null) ? 0 : config.AdminGAFeesCost));
                cmd.Parameters.Add(new SqlParameter("@AdminGAFeesHours", (config.AdminGAFeesHours == null) ? 0 : config.AdminGAFeesHours));
                cmd.Parameters.Add(new SqlParameter("@CrewOtherCost", (config.CrewOtherCost == null) ? 0 : config.CrewOtherCost));
                cmd.Parameters.Add(new SqlParameter("@CrewProvisionsCost", (config.CrewProvisionsCost == null) ? 0 : config.CrewProvisionsCost));
                cmd.Parameters.Add(new SqlParameter("@DelayTerminalLoading", (config.DelayTerminalLoading == null) ? 0 : config.DelayTerminalLoading));
                cmd.Parameters.Add(new SqlParameter("@DelayTerminalUnloading", (config.DelayTerminalUnloading == null) ? 0 : config.DelayTerminalUnloading));
                cmd.Parameters.Add(new SqlParameter("@DelayVesselLoading", (config.DelayVesselLoading == null) ? 0 : config.DelayVesselLoading));
                cmd.Parameters.Add(new SqlParameter("@DelayVesselUnloading", (config.DelayVesselUnloading == null) ? 0 : config.DelayVesselUnloading));
                cmd.Parameters.Add(new SqlParameter("@DemurrageClaimedLoading", (config.DemurrageClaimedLoading == null) ? 0 : config.DemurrageClaimedLoading));
                cmd.Parameters.Add(new SqlParameter("@DemurrageClaimedLoadingAmount", (config.DemurrageClaimedLoadingAmount == null) ? 0 : config.DemurrageClaimedLoadingAmount));
                cmd.Parameters.Add(new SqlParameter("@DemurrageClaimedUnloading", (config.DemurrageClaimedUnloading == null) ? 0 : config.DemurrageClaimedUnloading));
                cmd.Parameters.Add(new SqlParameter("@DemurrageClaimedUnloadingAmount", (config.DemurrageClaimedUnloadingAmount == null) ? 0 : config.DemurrageClaimedUnloadingAmount));
                cmd.Parameters.Add(new SqlParameter("@DemurragePaidLoading", (config.DemurragePaidLoading == null) ? 0 : config.DemurragePaidLoading));
                cmd.Parameters.Add(new SqlParameter("@DemurragePaidLoadingCost", (config.DemurragePaidLoadingCost == null) ? 0 : config.DemurragePaidLoadingCost));
                cmd.Parameters.Add(new SqlParameter("@DemurragePaidUnloading", (config.DemurragePaidUnloading == null) ? 0 : config.DemurragePaidUnloading));
                cmd.Parameters.Add(new SqlParameter("@DemurragePaidUnloadingCost", (config.DemurragePaidUnloadingCost == null) ? 0 : config.DemurragePaidUnloadingCost));
                cmd.Parameters.Add(new SqlParameter("@DryDockHours", (config.DryDockHours == null) ? 0 : config.DryDockHours));
                cmd.Parameters.Add(new SqlParameter("@EnergyType1AvgCost", (config.EnergyType1AvgCost == null) ? 0 : config.EnergyType1AvgCost));
                cmd.Parameters.Add(new SqlParameter("@EnergyType2AvgCost", (config.EnergyType2AvgCost == null) ? 0 : config.EnergyType2AvgCost));
                cmd.Parameters.Add(new SqlParameter("@EnergyType3AvgCost", (config.EnergyType3AvgCost == null) ? 0 : config.EnergyType3AvgCost));
                cmd.Parameters.Add(new SqlParameter("@EnergyType1BTU", (config.EnergyType1BTU == null) ? 0 : config.EnergyType1BTU));
                cmd.Parameters.Add(new SqlParameter("@EnergyType2BTU", (config.EnergyType2BTU == null) ? 0 : config.EnergyType2BTU));
                cmd.Parameters.Add(new SqlParameter("@EnergyType3BTU", (config.EnergyType3BTU == null) ? 0 : config.EnergyType3BTU));
                cmd.Parameters.Add(new SqlParameter("@EnergyType1Cost", (config.EnergyType1Cost == null) ? 0 : config.EnergyType1Cost));
                cmd.Parameters.Add(new SqlParameter("@EnergyType2Cost", (config.EnergyType2Cost == null) ? 0 : config.EnergyType2Cost));
                cmd.Parameters.Add(new SqlParameter("@EnergyType3Cost", (config.EnergyType3Cost == null) ? 0 : config.EnergyType3Cost));
                cmd.Parameters.Add(new SqlParameter("@EnergyType1Volume", (config.EnergyType1Volume == null) ? 0 : config.EnergyType1Volume));
                cmd.Parameters.Add(new SqlParameter("@EnergyType2Volume", (config.EnergyType2Volume == null) ? 0 : config.EnergyType2Volume));
                cmd.Parameters.Add(new SqlParameter("@EnergyType3Volume", (config.EnergyType3Volume == null) ? 0 : config.EnergyType3Volume));
                cmd.Parameters.Add(new SqlParameter("@EnergyType1VolumeBTU", (config.EnergyType1VolumeBTU == null) ? 0 : config.EnergyType1VolumeBTU));
                cmd.Parameters.Add(new SqlParameter("@EnergyType2VolumeBTU", (config.EnergyType2VolumeBTU == null) ? 0 : config.EnergyType2VolumeBTU));
                cmd.Parameters.Add(new SqlParameter("@EnergyType3VolumeBTU", (config.EnergyType3VolumeBTU == null) ? 0 : config.EnergyType3VolumeBTU));
                cmd.Parameters.Add(new SqlParameter("@EnergyType1VolumeUnit", (config.EnergyType1VolumeUnit == null) ? "" : config.EnergyType1VolumeUnit));
                cmd.Parameters.Add(new SqlParameter("@EnergyType2VolumeUnit", (config.EnergyType2VolumeUnit == null) ? "" : config.EnergyType2VolumeUnit));
                cmd.Parameters.Add(new SqlParameter("@EnergyType3VolumeUnit", (config.EnergyType3VolumeUnit == null) ? "" : config.EnergyType3VolumeUnit));
                cmd.Parameters.Add(new SqlParameter("@CargoBurnedVolumeUnit", (config.CargoBurnedVolumeUnit == null) ? "" : config.CargoBurnedVolumeUnit));
                cmd.Parameters.Add(new SqlParameter("@BunkersSoldVolumeUnit", (config.BunkersSoldVolumeUnit == null) ? "" : config.BunkersSoldVolumeUnit));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelAcquiredVolumeUnit", (config.OtherFuelAcquiredVolumeUnit == null) ? "" : config.OtherFuelAcquiredVolumeUnit));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelSoldVolumeUnit", (config.OtherFuelSoldVolumeUnit == null) ? "" : config.OtherFuelSoldVolumeUnit));
                cmd.Parameters.Add(new SqlParameter("@CargoBurnedVolume", (config.CargoBurnedVolume == null) ? 0 : config.CargoBurnedVolume));
                cmd.Parameters.Add(new SqlParameter("@BunkersSoldVolume", (config.BunkersSoldVolume == null) ? 0 : config.BunkersSoldVolume));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelAcquiredVolume", (config.OtherFuelAcquiredVolume == null) ? 0 : config.OtherFuelAcquiredVolume));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelSoldVolume", (config.OtherFuelSoldVolume == null) ? 0 : config.OtherFuelSoldVolume));
                cmd.Parameters.Add(new SqlParameter("@CargoBurnedAvgCost", (config.CargoBurnedAvgCost == null) ? 0 : config.CargoBurnedAvgCost));
                cmd.Parameters.Add(new SqlParameter("@BunkersSoldAvgCost", (config.BunkersSoldAvgCost == null) ? 0 : config.BunkersSoldAvgCost));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelAcquiredAvgCost", (config.OtherFuelAcquiredAvgCost == null) ? 0 : config.OtherFuelAcquiredAvgCost));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelSoldAvgCost", (config.OtherFuelSoldAvgCost == null) ? 0 : config.OtherFuelSoldAvgCost));
                cmd.Parameters.Add(new SqlParameter("@CargoBurnedCost", (config.CargoBurnedCost == null) ? 0 : config.CargoBurnedCost));
                cmd.Parameters.Add(new SqlParameter("@BunkersSoldCost", (config.BunkersSoldCost == null) ? 0 : config.BunkersSoldCost));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelAcquiredCost", (config.OtherFuelAcquiredCost == null) ? 0 : config.OtherFuelAcquiredCost));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelSoldCost", (config.OtherFuelSoldCost == null) ? 0 : config.OtherFuelSoldCost));
                cmd.Parameters.Add(new SqlParameter("@CargoBurnedVolumeBTU", (config.CargoBurnedVolumeBTU == null) ? 0 : config.CargoBurnedVolumeBTU));
                cmd.Parameters.Add(new SqlParameter("@BunkersSoldVolumeBTU", (config.BunkersSoldVolumeBTU == null) ? 0 : config.BunkersSoldVolumeBTU));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelAcquiredVolumeBTU", (config.OtherFuelAcquiredVolumeBTU == null) ? 0 : config.OtherFuelAcquiredVolumeBTU));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelSoldVolumeBTU", (config.OtherFuelSoldVolumeBTU == null) ? 0 : config.OtherFuelSoldVolumeBTU));
                cmd.Parameters.Add(new SqlParameter("@CargoBurnedBTU", (config.CargoBurnedBTU == null) ? 0 : config.CargoBurnedBTU));
                cmd.Parameters.Add(new SqlParameter("@BunkersSoldBTU", (config.BunkersSoldBTU == null) ? 0 : config.BunkersSoldBTU));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelAcquiredBTU", (config.OtherFuelAcquiredBTU == null) ? 0 : config.OtherFuelAcquiredBTU));
                cmd.Parameters.Add(new SqlParameter("@OtherFuelSoldBTU", (config.OtherFuelSoldBTU == null) ? 0 : config.OtherFuelSoldBTU));
                cmd.Parameters.Add(new SqlParameter("@EstimatedVentingLoss", (config.EstimatedVentingLoss == null) ? 0 : config.EstimatedVentingLoss));
                cmd.Parameters.Add(new SqlParameter("@EstimatedVentingLossPercent", (config.EstimatedVentingLossPercent == null) ? 0 : config.EstimatedVentingLossPercent));
                cmd.Parameters.Add(new SqlParameter("@InsuranceMarineCost", (config.InsuranceMarineCost == null) ? 0 : config.InsuranceMarineCost));
                cmd.Parameters.Add(new SqlParameter("@InsurancePICost", (config.InsurancePICost == null) ? 0 : config.InsurancePICost));
                cmd.Parameters.Add(new SqlParameter("@LayTimeLoading", (config.LayTimeLoading == null) ? 0 : config.LayTimeLoading));
                cmd.Parameters.Add(new SqlParameter("@LayTimeUnloading", (config.LayTimeUnloading == null) ? 0 : config.LayTimeUnloading));
                cmd.Parameters.Add(new SqlParameter("@MaintCompanyCost", (config.MaintCompanyCost == null) ? 0 : config.MaintCompanyCost));
                cmd.Parameters.Add(new SqlParameter("@MaintCompanyHours", (config.MaintCompanyHours == null) ? 0 : config.MaintCompanyHours));
                cmd.Parameters.Add(new SqlParameter("@MaintContractorCost", (config.MaintContractorCost == null) ? 0 : config.MaintContractorCost));
                cmd.Parameters.Add(new SqlParameter("@MaintContractorHours", (config.MaintContractorHours == null) ? 0 : config.MaintContractorHours));
                cmd.Parameters.Add(new SqlParameter("@MiscCost", (config.MiscCost == null) ? 0 : config.MiscCost));
                cmd.Parameters.Add(new SqlParameter("@MiscCostComments", (config.MiscCostComments == null) ? "" : config.MiscCostComments));
                cmd.Parameters.Add(new SqlParameter("@MonthlyDataNotes", (config.MonthlyDataNotes == null) ? "" : config.MonthlyDataNotes));
                cmd.Parameters.Add(new SqlParameter("@NonDryDockMaintCost", (config.NonDryDockMaintCost == null) ? 0 : config.NonDryDockMaintCost));
                cmd.Parameters.Add(new SqlParameter("@NonMaintCompanyCost", (config.NonMaintCompanyCost == null) ? 0 : config.NonMaintCompanyCost));
                cmd.Parameters.Add(new SqlParameter("@NonMaintCompanyHours", (config.NonMaintCompanyHours == null) ? 0 : config.NonMaintCompanyHours));
                cmd.Parameters.Add(new SqlParameter("@NonMaintContractorCost", (config.NonMaintContractorCost == null) ? 0 : config.NonMaintContractorCost));
                cmd.Parameters.Add(new SqlParameter("@NonMaintContractorHours", (config.NonMaintContractorHours == null) ? 0 : config.NonMaintContractorHours));
                cmd.Parameters.Add(new SqlParameter("@OtherDowntime", (config.OtherDowntime == null) ? 0 : config.OtherDowntime));
                cmd.Parameters.Add(new SqlParameter("@OtherDowntimeComments", (config.OtherDowntimeComments == null) ? "" : config.OtherDowntimeComments));
                cmd.Parameters.Add(new SqlParameter("@PurchasedElectricityCost", (config.PurchasedElectricityCost == null) ? 0 : config.PurchasedElectricityCost));
                cmd.Parameters.Add(new SqlParameter("@PurchasedElectricityMW", (config.PurchasedElectricityMW == null) ? 0 : config.PurchasedElectricityMW));
                cmd.Parameters.Add(new SqlParameter("@RegistrationCost", (config.RegistrationCost == null) ? 0 : config.RegistrationCost));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_BargeBerthBBL", (config.RSCRUDE_BargeBerthBBL == null) ? 0 : config.RSCRUDE_BargeBerthBBL));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_BargeBerthProductType", (config.RSCRUDE_BargeBerthProductType == null) ? "" : config.RSCRUDE_BargeBerthProductType));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_GasCarrierBBL", (config.RSCRUDE_GasCarrierBBL == null) ? 0 : config.RSCRUDE_GasCarrierBBL));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_GasCarrierProductType", (config.RSCRUDE_GasCarrierProductType == null) ? "" : config.RSCRUDE_GasCarrierProductType));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_OffshoreBuoyBBL", (config.RSCRUDE_OffshoreBuoyBBL == null) ? 0 : config.RSCRUDE_OffshoreBuoyBBL));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_OffshoreBuoyProductType", (config.RSCRUDE_OffshoreBuoyProductType == null) ? "" : config.RSCRUDE_OffshoreBuoyProductType));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_OtherBBL", (config.RSCRUDE_OtherBBL == null) ? 0 : config.RSCRUDE_OtherBBL));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_OtherProductType", (config.RSCRUDE_OtherProductType == null) ? "" : config.RSCRUDE_OtherProductType));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_RailcarBBL", (config.RSCRUDE_RailcarBBL == null) ? 0 : config.RSCRUDE_RailcarBBL));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_RailcarProductType", (config.RSCRUDE_RailcarProductType == null) ? "" : config.RSCRUDE_RailcarProductType));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_TankerBerthBBL", (config.RSCRUDE_TankerBerthBBL == null) ? 0 : config.RSCRUDE_TankerBerthBBL));
                cmd.Parameters.Add(new SqlParameter("@RSCRUDE_TankerBerthProductType", (config.RSCRUDE_TankerBerthProductType == null) ? "" : config.RSCRUDE_TankerBerthProductType));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_BargeBerthBBL", (config.RSPROD_BargeBerthBBL == null) ? 0 : config.RSPROD_BargeBerthBBL));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_BargeBerthProductType", (config.RSPROD_BargeBerthProductType == null) ? "" : config.RSPROD_BargeBerthProductType));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_GasCarrierBBL", (config.RSPROD_GasCarrierBBL == null) ? 0 : config.RSPROD_GasCarrierBBL));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_GasCarrierProductType", (config.RSPROD_GasCarrierProductType == null) ? "" : config.RSPROD_GasCarrierProductType));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_OffshoreBuoyBBL", (config.RSPROD_OffshoreBuoyBBL == null) ? 0 : config.RSPROD_OffshoreBuoyBBL));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_OffshoreBuoyProductType", (config.RSPROD_OffshoreBuoyProductType == null) ? "" : config.RSPROD_OffshoreBuoyProductType));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_OtherBBL", (config.RSPROD_OtherBBL == null) ? 0 : config.RSPROD_OtherBBL));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_OtherProductType", (config.RSPROD_OtherProductType == null) ? "" : config.RSPROD_OtherProductType));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_RailcarBBL", (config.RSPROD_RailcarBBL == null) ? 0 : config.RSPROD_RailcarBBL));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_RailcarProductType", (config.RSPROD_RailcarProductType == null) ? "" : config.RSPROD_RailcarProductType));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_TankerBerthBBL", (config.RSPROD_TankerBerthBBL == null) ? 0 : config.RSPROD_TankerBerthBBL));
                cmd.Parameters.Add(new SqlParameter("@RSPROD_TankerBerthProductType", (config.RSPROD_TankerBerthProductType == null) ? "" : config.RSPROD_TankerBerthProductType));
                cmd.Parameters.Add(new SqlParameter("@SeaMilesLoaded", (config.SeaMilesLoaded == null) ? 0 : config.SeaMilesLoaded));
                cmd.Parameters.Add(new SqlParameter("@SeaMilesLoadedTimeAuthorized", (config.SeaMilesLoadedTimeAuthorized == null) ? 0 : config.SeaMilesLoadedTimeAuthorized));
                cmd.Parameters.Add(new SqlParameter("@SeaMilesLoadedTimeSpent", (config.SeaMilesLoadedTimeSpent == null) ? 0 : config.SeaMilesLoadedTimeSpent));
                cmd.Parameters.Add(new SqlParameter("@SeaMilesUnloaded", (config.SeaMilesUnloaded == null) ? 0 : config.SeaMilesUnloaded));
                cmd.Parameters.Add(new SqlParameter("@SeaMilesUnloadedTimeAuthorized", (config.SeaMilesUnloadedTimeAuthorized == null) ? 0 : config.SeaMilesUnloadedTimeAuthorized));
                cmd.Parameters.Add(new SqlParameter("@SeaMilesUnloadedTimeSpent", (config.SeaMilesUnloadedTimeSpent == null) ? 0 : config.SeaMilesUnloadedTimeSpent));

                cmd.Parameters.Add(new SqlParameter("@SoldElectricityCost", (config.SoldElectricityCost == null) ? 0 : config.SoldElectricityCost));
                cmd.Parameters.Add(new SqlParameter("@SoldElectricityMW", (config.SoldElectricityMW == null) ? 0 : config.SoldElectricityMW));
                cmd.Parameters.Add(new SqlParameter("@StoresLubeCost", (config.StoresLubeCost == null) ? 0 : config.StoresLubeCost));
                cmd.Parameters.Add(new SqlParameter("@StoresOtherCost", (config.StoresOtherCost == null) ? 0 : config.StoresOtherCost));
                cmd.Parameters.Add(new SqlParameter("@SundriesCost", (config.SundriesCost == null) ? 0 : config.SundriesCost));
                cmd.Parameters.Add(new SqlParameter("@TotalFuelCost", (config.TotalFuelCost == null) ? 0 : config.TotalFuelCost));
                cmd.Parameters.Add(new SqlParameter("@TotalFuelEnergy", (config.TotalFuelEnergy == null) ? 0 : config.TotalFuelEnergy));
                cmd.Parameters.Add(new SqlParameter("@TotalHrsMechanicalDowntime", (config.TotalHrsMechanicalDowntime == null) ? 0 : config.TotalHrsMechanicalDowntime));
                cmd.Parameters.Add(new SqlParameter("@TotalHrsRegulatoryDowntime", (config.TotalHrsRegulatoryDowntime == null) ? 0 : config.TotalHrsRegulatoryDowntime));
                cmd.Parameters.Add(new SqlParameter("@TotalHrsScheduledDowntime", (config.TotalHrsScheduledDowntime == null) ? 0 : config.TotalHrsScheduledDowntime));
                cmd.Parameters.Add(new SqlParameter("@TotalNonEnergyOpex", (config.TotalNonEnergyOpex == null) ? 0 : config.TotalNonEnergyOpex));
                cmd.Parameters.Add(new SqlParameter("@TotalPhysicalHydrocarbonHauled", (config.TotalPhysicalHydrocarbonHauled == null) ? 0 : config.TotalPhysicalHydrocarbonHauled));
                cmd.Parameters.Add(new SqlParameter("@TotalPhysicalHydrocarbonLoss", (config.TotalPhysicalHydrocarbonLoss == null) ? 0 : config.TotalPhysicalHydrocarbonLoss));
                cmd.Parameters.Add(new SqlParameter("@TotalPhysicalHydrocarbonLossPercent", (config.TotalPhysicalHydrocarbonLossPercent == null) ? 0 : config.TotalPhysicalHydrocarbonLossPercent));
                cmd.Parameters.Add(new SqlParameter("@UncontrollableDelayLoading", (config.UncontrollableDelayLoading == null) ? 0 : config.UncontrollableDelayLoading));
                cmd.Parameters.Add(new SqlParameter("@UncontrollableDelayUnloading", (config.UncontrollableDelayUnloading == null) ? 0 : config.UncontrollableDelayUnloading));
                cmd.Parameters.Add(new SqlParameter("@VesselProducedElectricityCost", (config.VesselProducedElectricityCost == null) ? 0 : config.VesselProducedElectricityCost));
                cmd.Parameters.Add(new SqlParameter("@VesselProducedElectricityMJ", (config.VesselProducedElectricityMJ == null) ? 0 : config.VesselProducedElectricityMJ));
                cmd.Parameters.Add(new SqlParameter("@VesselProducedElectricityMW", (config.VesselProducedElectricityMW == null) ? 0 : config.VesselProducedElectricityMW));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteShipData";
                    return;
                }

                

            }

        }


        public void WriteShipConfig(List<LPGShipping_T2_ShipConfiguration> configs)
        {

          cmd.CommandText = "LPGShipping_InsertShipConfiguration";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (LPGShipping_T2_ShipConfiguration config in configs)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", config.UnitID));
                cmd.Parameters.Add(new SqlParameter("@RefrigerationType", (config.RefrigerationType == null) ? "" : config.RefrigerationType));
                cmd.Parameters.Add(new SqlParameter("@TotalRecompressionHP", (config.TotalRecompressionHP == null) ? 0 : config.TotalRecompressionHP));
                cmd.Parameters.Add(new SqlParameter("@CompressorCount", (config.CompressorCount == null) ? 0 : config.CompressorCount));
                cmd.Parameters.Add(new SqlParameter("@CompressionDriveType", (config.RecompressionDriveType == null) ? "" : config.RecompressionDriveType));
                cmd.Parameters.Add(new SqlParameter("@NumMainEngines", (config.NumMainEngines == null) ? 0 : config.NumMainEngines));
                cmd.Parameters.Add(new SqlParameter("@NumAuxEngines", (config.NumAuxEngines == null) ? 0 : config.NumAuxEngines));
                cmd.Parameters.Add(new SqlParameter("@NumGenerators", (config.NumGenerators == null) ? 0 : config.NumGenerators));
                cmd.Parameters.Add(new SqlParameter("@NumAddGenerators", (config.NumAddGenerators == null) ? 0 : config.NumAddGenerators));
                cmd.Parameters.Add(new SqlParameter("@NumEvaporators", (config.NumEvaporators == null) ? 0 : config.NumEvaporators));
                cmd.Parameters.Add(new SqlParameter("@NumWaterProdUnits", (config.NumWaterProdUnits == null) ? 0 : config.NumWaterProdUnits));
                cmd.Parameters.Add(new SqlParameter("@NumCargoPumps", (config.NumCargoPumps == null) ? 0 : config.NumCargoPumps));


                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteShipConfig";
                    return;
                }

               


            }

        }


        public int WriteCompanyInformation(LPGShipping_Company_Information company)
        {
            int rtn=0;
            cmd.Connection = cn;
            cmd.CommandText = "LPGShipping_InsertCompanyInformation";
            cmd.CommandType = CommandType.StoredProcedure;

            if (company.CompanyName.Length > 0)
            {
                cmd.Parameters.Clear();

                cmd.Parameters.Add(new SqlParameter("@Company", company.CompanyName));
                cmd.Parameters.Add(new SqlParameter("@Location", (company.Location == null) ? "" : company.Location));
                cmd.Parameters.Add(new SqlParameter("@CoordName", (company.ContactName == null) ? "" : company.ContactName));
                cmd.Parameters.Add(new SqlParameter("@CoordTitle", (company.ContactTitle == null) ? "" : company.ContactTitle));
                cmd.Parameters.Add(new SqlParameter("@CoordPhone", (company.Phone == null) ? "" : company.Phone));
                cmd.Parameters.Add(new SqlParameter("@CoordEmail", (company.Email == null) ? "" : company.Email));


                try
                {
                    rtn = Convert.ToInt32(cmd.ExecuteScalar());
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteShipConfig";
                    return rtn;
                }
            }

            return rtn;

           
        }
        public void WriteShipUtilization(List<LPGShipping_Utilization> configs)
        {
            cmd.Connection = cn;
          cmd.CommandText = "LPGShipping_InsertShipUtilization";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (LPGShipping_Utilization config in configs)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID",CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", config.UnitID));
                cmd.Parameters.Add(new SqlParameter("@ShipName", (config.ShipName == null) ? "" : config.ShipName));
                
                cmd.Parameters.Add(new SqlParameter("@Month", (config.Month == null) ? "" : config.Month));
                cmd.Parameters.Add(new SqlParameter("@Year", (config.Year == null) ? 0 : config.Year));

                cmd.Parameters.Add(new SqlParameter("@ShipCapacityCBM", (config.ShipCapacityCBM == null) ? 0 : config.ShipCapacityCBM));
                cmd.Parameters.Add(new SqlParameter("@ShipHoursReported", (config.ShipHoursReported == null) ? 0 : config.ShipHoursReported));
                cmd.Parameters.Add(new SqlParameter("@ShipCapacityCBMHR", (config.ShipCapacityCBMHR == null) ? 0 : config.ShipCapacityCBMHR));

                cmd.Parameters.Add(new SqlParameter("@TotalAuthVolumeCBM", (config.TotalAuthVolumeCBM == null) ? 0 : config.TotalAuthVolumeCBM));
                cmd.Parameters.Add(new SqlParameter("@TotalAuthorizedHrs", (config.TotalAuthorizedHrs == null) ? 0 : config.TotalAuthorizedHrs));
                cmd.Parameters.Add(new SqlParameter("@TotalAuthorizedShippingCapacity", (config.TotalAuthorizedShippingCapacity == null) ? 0 : config.TotalAuthorizedShippingCapacity));
                cmd.Parameters.Add(new SqlParameter("@TotalRealizedShippingCapacityCBMHR", (config.TotalRealizedShippingCapacityCBMHR == null) ? 0 : config.TotalRealizedShippingCapacityCBMHR));


                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteShipConfig";
                    return;
                }

                


            }

        }


        public void WriteShipUtilizationTrips(List<LPGShipping_UtilizationTrips> configs)
        {
            cmd.Connection = cn;
          cmd.CommandText = "[LPGShipping_InsertShipUtilizationTrips]";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (LPGShipping_UtilizationTrips config in configs)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", config.UnitID));
                cmd.Parameters.Add(new SqlParameter("@ShipName", (config.ShipName == null) ? "" : config.ShipName));
                cmd.Parameters.Add(new SqlParameter("@TripNo", (config.TripNo == null) ? 0 : config.TripNo));
                cmd.Parameters.Add(new SqlParameter("@VolumeCBM", (config.VolumeCBM == null) ? 0 : config.VolumeCBM));
                cmd.Parameters.Add(new SqlParameter("@AuthorizedHours", (config.AuthorizedHours == null) ? 0 : config.AuthorizedHours));

                cmd.Parameters.Add(new SqlParameter("@AuthorizedShippingCapacityCBMHR", (config.AuthorizedShippingCapacityCBMHR == null) ? 0 : config.AuthorizedShippingCapacityCBMHR));
                cmd.Parameters.Add(new SqlParameter("@ActualVoyageHours", (config.ActualVoyageHours == null) ? 0 : config.ActualVoyageHours));
                cmd.Parameters.Add(new SqlParameter("@RealizedShippingCapacityCBMHR", (config.RealizedShippingCapacityCBMHR == null) ? 0 : config.RealizedShippingCapacityCBMHR));

                cmd.Parameters.Add(new SqlParameter("@TripType", (config.TripType == null) ? "" : config.TripType));
                cmd.Parameters.Add(new SqlParameter("@Comment", (config.Comment == null) ? "" : config.Comment));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteShipUtilizationTrips";
                    return;
                }

               

            }
        }




        public void WriteEngines(List<LPGShipping_T2_Engines> configs, Boolean Aux)
        {
            cmd.Connection = cn;
          cmd.CommandText = "[LPGShipping_InsertShipEngine]";
            cmd.CommandType = CommandType.StoredProcedure;
            int count = 1;
            foreach (LPGShipping_T2_Engines config in configs)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", config.UnitID));
                cmd.Parameters.Add(new SqlParameter("@EngineNo", count));
                cmd.Parameters.Add(new SqlParameter("@Horsepower", (config.Horsepower == null) ? 0 : config.Horsepower));
                cmd.Parameters.Add(new SqlParameter("@EngineType", (config.EngineType == null) ? "" : config.EngineType));
                cmd.Parameters.Add(new SqlParameter("@Fuel", (config.Fuel == null) ? "" : config.Fuel));
                cmd.Parameters.Add(new SqlParameter("@Manufacturer", (config.Manufacturer == null) ? "" : config.Manufacturer));
                cmd.Parameters.Add(new SqlParameter("@Model", (config.Model == null) ? "" : config.Model));
                cmd.Parameters.Add(new SqlParameter("@Auxillary", Aux));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteEngines";
                    return;
                }

                

                count++;
            }

        }


        public void WriteCargoPumps(List<LPGShipping_T2_CargoPumps> configs)
        {
            cmd.Connection = cn;
          cmd.CommandText = "[LPGShipping_InsertCargoPump]";
            cmd.CommandType = CommandType.StoredProcedure;
            int count = 1;
            foreach (LPGShipping_T2_CargoPumps config in configs)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", config.UnitID));
                cmd.Parameters.Add(new SqlParameter("@CargoPumpNo", count));
                cmd.Parameters.Add(new SqlParameter("@Horsepower", (config.Horsepower == null) ? 0 : config.Horsepower));
                cmd.Parameters.Add(new SqlParameter("@PumpType", (config.PumpType == null) ? "" : config.PumpType));
                cmd.Parameters.Add(new SqlParameter("@MotorType", (config.MotorType == null) ? "" : config.MotorType));
                cmd.Parameters.Add(new SqlParameter("@Flowrate", (config.FlowRate == null) ? 0 : config.FlowRate));


                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteCargoPumps";
                    return;
                }

                

                count++;
            }

        }


        public void WriteGenerators(List<LPGShipping_T2_Generators> configs, Boolean Aux)
        {
            
            cmd.Connection = cn;
          cmd.CommandText = "[LPGShipping_InsertShipGenerator]";
            cmd.CommandType = CommandType.StoredProcedure;
            int count = 1;
            foreach (LPGShipping_T2_Generators config in configs)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", config.UnitID));
                cmd.Parameters.Add(new SqlParameter("@GeneratorNo", count));
                cmd.Parameters.Add(new SqlParameter("@Kilowatts", (config.Kilowatts == null) ? 0 : config.Kilowatts));
                cmd.Parameters.Add(new SqlParameter("@Fuel", (config.Fuel == null) ? "" : config.Fuel));
                cmd.Parameters.Add(new SqlParameter("@Additional", Aux));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteGenerators";
                    return;
                }

                

                count++;
            }

        }



        public void WriteEvaporators(List<LPGShipping_T2_Evaporators> configs, Boolean Aux)
        {


          cmd.CommandText = "[LPGShipping_InsertShipEvaporator]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;
            int count = 1;
            foreach (LPGShipping_T2_Evaporators config in configs)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", config.UnitID));
                cmd.Parameters.Add(new SqlParameter("@EvaporatorNo", count));
                cmd.Parameters.Add(new SqlParameter("@GPM", (config.CapacityGPM == null) ? 0 : config.CapacityGPM));
                cmd.Parameters.Add(new SqlParameter("@Type", (config.EvaporatorType == null) ? "" : config.EvaporatorType));
                cmd.Parameters.Add(new SqlParameter("@OtherWaterProdUnit", Aux));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteEvaporators";
                    return;
                }

                

                count++;
            }

        }
        public void WriteShips(List<LPGShipping_T1_FleetInformation> ships, int sid)
        {

          cmd.CommandText = "[LPGShipping_InsertFleetInformation]";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;


            foreach (LPGShipping_T1_FleetInformation ship in ships)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", sid));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", ship.UnitID));
                cmd.Parameters.Add(new SqlParameter("@ShipName", Write(ship.ShipName)));
                cmd.Parameters.Add(new SqlParameter("@ShipType", Write(ship.ShipType)));
                cmd.Parameters.Add(new SqlParameter("@ShipSubType", Write(ship.ShipSubType)));
                cmd.Parameters.Add(new SqlParameter("@PrimaryService", Write(ship.PrimaryService)));
                cmd.Parameters.Add(new SqlParameter("@Flag", Write(ship.Flag)));
                cmd.Parameters.Add(new SqlParameter("@CapacityCBM", Write(ship.CapacityCBM)));
                cmd.Parameters.Add(new SqlParameter("@CapacityDWT", Write(ship.CapacityDWT)));
                cmd.Parameters.Add(new SqlParameter("@NumTanks", Write(ship.NumTanks)));
                cmd.Parameters.Add(new SqlParameter("@TankType", Write(ship.TankType)));
                cmd.Parameters.Add(new SqlParameter("@LGCGrouping", Write(ship.LGCGroups)));
                cmd.Parameters.Add(new SqlParameter("@PressureMaxBAR", Write(ship.PressureMAX)));
                cmd.Parameters.Add(new SqlParameter("@TemperatureMIN", Write(ship.TempMIN)));
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteShips";
                    return;
                }

                

            }



        }

        public List<LPGShipping_T2_ShipConfiguration> GetShipConfiguration()
        {
            int i = 1;
            List<LPGShipping_T2_ShipConfiguration> scfg = new List<LPGShipping_T2_ShipConfiguration>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(FleetLevelInputSheet);
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("ShipConfiguration");

                int NumCols = rFleet.Columns.Count;



                for (i = 1; i <= NumCols; i++)
                {

                    
                    LPGShipping_T2_ShipConfiguration config = new LPGShipping_T2_ShipConfiguration();

                    config.UnitID = Convert.ToInt32(xlWorkSheet.get_Range(UnitIDRange).Cells[1, i].value);
                    config.RefrigerationType = rFleet.Cells[1, i].Value;
                    config.TotalRecompressionHP = Convert.ToInt32(rFleet.Cells[2, i].Value);
                    config.CompressorCount = Convert.ToInt32(rFleet.Cells[3, i].Value);
                    config.RecompressionDriveType = rFleet.Cells[4, i].Value();
                    config.NumMainEngines = Convert.ToInt32(xlWorkSheet.get_Range("NumMainEngines").Cells[1, i].Value());
                    config.NumAuxEngines = Convert.ToInt32(xlWorkSheet.get_Range("NumAuxEngines").Cells[1, i].Value());
                    config.NumGenerators = Convert.ToInt32(xlWorkSheet.get_Range("NumMainGenerators").Cells[1, i].Value());
                    config.NumAddGenerators = Convert.ToInt32(xlWorkSheet.get_Range("NumAddGenerators").Cells[1, i].Value());
                    config.NumEvaporators = Convert.ToInt32(xlWorkSheet.get_Range("NumEvaporators").Cells[1, i].Value());
                    config.NumWaterProdUnits = Convert.ToInt32(xlWorkSheet.get_Range("NumWaterProdUnits").Cells[1, i].Value());
                    config.NumCargoPumps = Convert.ToInt32(xlWorkSheet.get_Range("NumCargoPumps").Cells[1, i].Value());

                    scfg.Add(config);

                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipConfiguration(" + i + ")";
                
            }

            return scfg;


        }

        public List<LPGShipping_ShipData> GetShipData(LPGShipping_T1_FleetInformation ship)
        {

            int i = 1;
            List<LPGShipping_ShipData> lpg = new List<LPGShipping_ShipData>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;

            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets[ship.ShipName + " Data"];
            try
            {


                LPGShipping_ShipData config = new LPGShipping_ShipData();

                config.UnitID = ship.UnitID;
                config.ShipName = ship.ShipName;
                config.RSCRUDE_RailcarBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSCRUDE_RailcarBBL").Value2);
                config.RSCRUDE_RailcarProductType = xlWorkSheet.get_Range("RSCRUDE_RailcarProductType").Value2.ToString();
                config.RSCRUDE_BargeBerthBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSCRUDE_BargeBerthBBL").Value2);
                config.RSCRUDE_BargeBerthProductType = xlWorkSheet.get_Range("RSCRUDE_BargeBerthProductType").Value2.ToString();
                config.RSCRUDE_TankerBerthBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSCRUDE_TankerBerthBBL").Value2);
                config.RSCRUDE_TankerBerthProductType = xlWorkSheet.get_Range("RSCRUDE_TankerBerthProductType").Value2.ToString();
                config.RSCRUDE_GasCarrierBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSCRUDE_GasCarrierBBL").Value2);
                config.RSCRUDE_GasCarrierProductType = xlWorkSheet.get_Range("RSCRUDE_GasCarrierProductType").Value2.ToString();
                config.RSCRUDE_OffshoreBuoyBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSCRUDE_OffshoreBuoyBBL").Value2);
                config.RSCRUDE_OffshoreBuoyProductType = xlWorkSheet.get_Range("RSCRUDE_OffshoreBuoyProductType").Value2.ToString();
                config.RSCRUDE_OtherBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSCRUDE_OtherBBL").Value2);
                config.RSCRUDE_OtherProductType = xlWorkSheet.get_Range("RSCRUDE_OtherProductType").Value2.ToString();

                config.RSPROD_RailcarBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSPROD_RailcarBBL").Value2);
                config.RSPROD_RailcarProductType = xlWorkSheet.get_Range("RSPROD_RailcarProductType").Value2.ToString();
                config.RSPROD_BargeBerthBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSPROD_BargeBerthBBL").Value2);
                config.RSPROD_BargeBerthProductType = xlWorkSheet.get_Range("RSPROD_BargeBerthProductType").Value2.ToString();
                config.RSPROD_TankerBerthBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSPROD_TankerBerthBBL").Value2);
                config.RSPROD_TankerBerthProductType = xlWorkSheet.get_Range("RSPROD_TankerBerthProductType").Value2.ToString();
                config.RSPROD_GasCarrierBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSPROD_GasCarrierBBL").Value2);
                config.RSPROD_GasCarrierProductType = xlWorkSheet.get_Range("RSPROD_GasCarrierProductType").Value2.ToString();
                config.RSPROD_OffshoreBuoyBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSPROD_OffshoreBuoyBBL").Value2);
                config.RSPROD_OffshoreBuoyProductType = xlWorkSheet.get_Range("RSPROD_OffshoreBuoyProductType").Value2.ToString();
                config.RSPROD_OtherBBL = Convert.ToInt32(xlWorkSheet.get_Range("RSPROD_OtherBBL").Value2);
                config.RSPROD_OtherProductType = xlWorkSheet.get_Range("RSPROD_OtherProductType").Value2.ToString();


                config.SeaMilesLoaded = Convert.ToInt32(xlWorkSheet.get_Range("SeaMilesLoaded").Value2);
                config.SeaMilesUnloaded = Convert.ToInt32(xlWorkSheet.get_Range("SeaMilesUnloaded").Value2);
                config.SeaMilesLoadedTimeSpent = Convert.ToInt32(xlWorkSheet.get_Range("SeaMilesLoadedTimeSpent").Value2);
                config.SeaMilesUnloadedTimeSpent = Convert.ToInt32(xlWorkSheet.get_Range("SeaMilesUnloadedTimeSpent").Value2);
                config.SeaMilesLoadedTimeAuthorized = Convert.ToInt32(xlWorkSheet.get_Range("SeaMilesLoadedTimeAuthorized").Value2);
                config.SeaMilesUnloadedTimeAuthorized = Convert.ToInt32(xlWorkSheet.get_Range("SeaMilesUnloadedTimeAuthorized").Value2);

                config.LayTimeLoading = Convert.ToInt32(xlWorkSheet.get_Range("LayTimeLoading").Value2);
                config.LayTimeUnloading = Convert.ToInt32(xlWorkSheet.get_Range("LayTimeUnloading").Value2);
                config.DelayVesselLoading = Convert.ToInt32(xlWorkSheet.get_Range("DelayVesselLoading").Value2);
                config.DelayVesselUnloading = Convert.ToInt32(xlWorkSheet.get_Range("DelayVesselUnloading").Value2);
                config.DelayTerminalLoading = Convert.ToInt32(xlWorkSheet.get_Range("DelayTerminalLoading").Value2);
                config.UncontrollableDelayLoading = Convert.ToInt32(xlWorkSheet.get_Range("UncontrollableDelayLoading").Value2);
                config.UncontrollableDelayUnloading = Convert.ToInt32(xlWorkSheet.get_Range("UncontrollableDelayUnloading").Value2);

                config.DemurrageClaimedLoading = Convert.ToInt32(xlWorkSheet.get_Range("DemurrageClaimedLoading").Value2);
                config.DemurrageClaimedLoadingAmount = Convert.ToInt32(xlWorkSheet.get_Range("DemurrageClaimedLoadingAmount").Value2);
                config.DemurrageClaimedUnloading = Convert.ToInt32(xlWorkSheet.get_Range("DemurrageClaimedUnloading").Value2);
                config.DemurrageClaimedUnloadingAmount = Convert.ToInt32(xlWorkSheet.get_Range("DemurrageClaimedUnloadingAmount").Value2);
                config.DemurragePaidLoading = Convert.ToInt32(xlWorkSheet.get_Range("DemurragePaidLoading").Value2);
                config.DemurragePaidLoadingCost = Convert.ToInt32(xlWorkSheet.get_Range("DemurragePaidLoadingAmount").Value2);
                config.DemurragePaidUnloading = Convert.ToInt32(xlWorkSheet.get_Range("DemurragePaidUnloading").Value2);
                config.DemurragePaidUnloadingCost = Convert.ToInt32(xlWorkSheet.get_Range("DemurragePaidUnloadingAmount").Value2);



                config.DryDockHours = Convert.ToInt32(xlWorkSheet.get_Range("DryDockHours").Value2);

                config.MaintCompanyHours = Convert.ToInt32(xlWorkSheet.get_Range("MaintCompanyHours").Value2);
                config.MaintCompanyCost = Convert.ToInt32(xlWorkSheet.get_Range("MaintCompanyCost").Value2);
                config.MaintContractorHours = Convert.ToInt32(xlWorkSheet.get_Range("MaintContractorHours").Value2);
                config.MaintContractorCost = Convert.ToInt32(xlWorkSheet.get_Range("MaintContractorCost").Value2);
                config.NonDryDockMaintCost = Convert.ToInt32(xlWorkSheet.get_Range("NonDryDockMaintCost").Value2);

                config.NonMaintCompanyHours = Convert.ToInt32(xlWorkSheet.get_Range("NonMaintCompanyHours").Value2);
                config.NonMaintCompanyCost = Convert.ToInt32(xlWorkSheet.get_Range("NonMaintCompanyCost").Value2);
                config.NonMaintContractorHours = Convert.ToInt32(xlWorkSheet.get_Range("NonMaintContractorHours").Value2);

                config.AdminGAFeesHours = Convert.ToInt32(xlWorkSheet.get_Range("AdminGAFeesHours").Value2);
                config.AdminGAFeesCost = Convert.ToInt32(xlWorkSheet.get_Range("AdminGAFeesCost").Value2);


                config.RegistrationCost = Convert.ToInt32(xlWorkSheet.get_Range("RegistrationCost").Value2);
                config.SundriesCost = Convert.ToInt32(xlWorkSheet.get_Range("SundriesCost").Value2);
                config.InsurancePICost = Convert.ToInt32(xlWorkSheet.get_Range("InsurancePICost").Value2);
                config.InsuranceMarineCost = Convert.ToInt32(xlWorkSheet.get_Range("InsuranceMarineCost").Value2);
                config.StoresLubeCost = Convert.ToInt32(xlWorkSheet.get_Range("StoresLubeCost").Value2);

                config.StoresOtherCost = Convert.ToInt32(xlWorkSheet.get_Range("StoresOtherCost").Value2);
                config.CrewProvisionsCost = Convert.ToInt32(xlWorkSheet.get_Range("CrewProvisionsCost").Value2);
                config.CrewOtherCost = Convert.ToInt32(xlWorkSheet.get_Range("CrewOtherCost").Value2);
                config.MiscCost = Convert.ToInt32(xlWorkSheet.get_Range("MiscCost").Value2);
                config.MiscCostComments = xlWorkSheet.get_Range("MiscCostComments").Value2.ToString();

                config.EnergyType1VolumeUnit = xlWorkSheet.get_Range("EnergyType1VolumeUnit").Value2.ToString();
                config.EnergyType2VolumeUnit = xlWorkSheet.get_Range("EnergyType2VolumeUnit").Value2.ToString();
                config.EnergyType3VolumeUnit = xlWorkSheet.get_Range("EnergyType3VolumeUnit").Value2.ToString();

                config.EnergyType1Volume = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType1Volume").Value2);
                config.EnergyType2Volume = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType2Volume").Value2);
                config.EnergyType3Volume = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType3Volume").Value2);

                config.EnergyType1AvgCost = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType1AvgCost").Value2);
                config.EnergyType2AvgCost = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType2AvgCost").Value2);
                config.EnergyType3AvgCost = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType3AvgCost").Value2);

                config.EnergyType1Cost = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType1Cost").Value2);
                config.EnergyType2Cost = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType2Cost").Value2);
                config.EnergyType3Cost = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType3Cost").Value2);

                config.EnergyType1VolumeBTU = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType1VolumeBTU").Value2);
                config.EnergyType2VolumeBTU = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType2VolumeBTU").Value2);
                config.EnergyType3VolumeBTU = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType3VolumeBTU").Value2);

                config.EnergyType1BTU = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType1BTU").Value2);
                config.EnergyType2BTU = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType2BTU").Value2);
                config.EnergyType3BTU = Convert.ToInt32(xlWorkSheet.get_Range("EnergyType3BTU").Value2);


                config.CargoBurnedVolumeUnit = xlWorkSheet.get_Range("CargoBurnedVolumeUnit").Value2.ToString();
                config.BunkersSoldVolumeUnit = xlWorkSheet.get_Range("BunkersSoldVolumeUnit").Value2.ToString();
                config.OtherFuelAcquiredVolumeUnit = xlWorkSheet.get_Range("OtherFuelAcquiredVolumeUnit").Value2.ToString();
                config.OtherFuelSoldVolumeUnit = xlWorkSheet.get_Range("OtherFuelSoldVolumeUnit").Value2.ToString();

                config.CargoBurnedVolume = Convert.ToInt32(xlWorkSheet.get_Range("CargoBurnedVolume").Value2);
                config.BunkersSoldVolume = Convert.ToInt32(xlWorkSheet.get_Range("BunkersSoldVolume").Value2);
                config.OtherFuelAcquiredVolume = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelAcquiredVolume").Value2);
                config.OtherFuelSoldVolume = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelSoldVolume").Value2);

                config.CargoBurnedAvgCost = Convert.ToInt32(xlWorkSheet.get_Range("CargoBurnedAvgCost").Value2);
                config.BunkersSoldAvgCost = Convert.ToInt32(xlWorkSheet.get_Range("BunkersSoldAvgCost").Value2);
                config.OtherFuelAcquiredAvgCost = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelAcquiredAvgCost").Value2);
                config.OtherFuelSoldAvgCost = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelSoldAvgCost").Value2);

                config.CargoBurnedCost = Convert.ToInt32(xlWorkSheet.get_Range("CargoBurnedCost").Value2);
                config.BunkersSoldCost = Convert.ToInt32(xlWorkSheet.get_Range("BunkersSoldCost").Value2);
                config.OtherFuelAcquiredCost = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelAcquiredCost").Value2);
                config.OtherFuelSoldCost = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelSoldCost").Value2);

                config.CargoBurnedVolumeBTU = Convert.ToInt32(xlWorkSheet.get_Range("CargoBurnedVolumeBTU").Value2);
                config.BunkersSoldVolumeBTU = Convert.ToInt32(xlWorkSheet.get_Range("BunkersSoldVolumeBTU").Value2);
                config.OtherFuelAcquiredVolumeBTU = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelAcquiredVolumeBTU").Value2);
                config.OtherFuelSoldVolumeBTU = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelSoldVolumeBTU").Value2);

                config.CargoBurnedBTU = Convert.ToInt32(xlWorkSheet.get_Range("CargoBurnedBTU").Value2);
                config.BunkersSoldBTU = Convert.ToInt32(xlWorkSheet.get_Range("BunkersSoldBTU").Value2);
                config.OtherFuelAcquiredBTU = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelAcquiredBTU").Value2);
                config.OtherFuelSoldBTU = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelSoldBTU").Value2);

                config.TotalFuelCost = Convert.ToInt32(xlWorkSheet.get_Range("TotalFuelCost").Value2);
                config.TotalFuelEnergy = Convert.ToInt32(xlWorkSheet.get_Range("TotalFuelEnergy").Value2);

                config.PurchasedElectricityMW = Convert.ToInt32(xlWorkSheet.get_Range("PurchasedElectricityMW").Value2);
                config.PurchasedElectricityCost = Convert.ToInt32(xlWorkSheet.get_Range("PurchasedElectricityCost").Value2);
                config.VesselProducedElectricityMW = Convert.ToInt32(xlWorkSheet.get_Range("VesselProducedElectricityMW").Value2);

                config.VesselProducedElectricityCost = Convert.ToInt32(xlWorkSheet.get_Range("VesselProducedElectricityCost").Value2);
                config.VesselProducedElectricityMJ = Convert.ToInt32(xlWorkSheet.get_Range("VesselProducedElectricityMJ").Value2);
                config.SoldElectricityMW = Convert.ToInt32(xlWorkSheet.get_Range("SoldElectricityMW").Value2);
                config.SoldElectricityCost = Convert.ToInt32(xlWorkSheet.get_Range("SoldElectricityCost").Value2);

                config.TotalHrsRegulatoryDowntime = Convert.ToInt32(xlWorkSheet.get_Range("TotalHrsRegulatoryDowntime").Value2);
                config.TotalHrsScheduledDowntime = Convert.ToInt32(xlWorkSheet.get_Range("TotalHrsScheduledDowntime").Value2);
                config.TotalHrsMechanicalDowntime = Convert.ToInt32(xlWorkSheet.get_Range("TotalHrsMechanicalDowntime").Value2);
                config.OtherDowntime = Convert.ToInt32(xlWorkSheet.get_Range("OtherDowntime").Value2);

                config.OtherDowntimeComments = xlWorkSheet.get_Range("OtherDowntimeComments").Value2.ToString();
                config.TotalPhysicalHydrocarbonHauled = Convert.ToInt32(xlWorkSheet.get_Range("TotalPhysicalHydrocarbonHauled").Value2);
                config.TotalPhysicalHydrocarbonLoss = Convert.ToInt32(xlWorkSheet.get_Range("TotalPhysicalHydrocarbonLoss").Value2);
                config.TotalPhysicalHydrocarbonLossPercent = Convert.ToInt32(xlWorkSheet.get_Range("TotalPhysicalHydrocarbonLossPercent").Value2);

                config.EstimatedVentingLoss = Convert.ToInt32(xlWorkSheet.get_Range("EstimatedVentingLoss").Value2);
                config.EstimatedVentingLossPercent = Convert.ToInt32(xlWorkSheet.get_Range("EstimatedVentingLossPercent").Value2);
                Excel.Range notesRange = xlWorkSheet.get_Range("MonthlyDataNotes");
                for (int c = 1; c < notesRange.Rows.Count; c++)
                    config.MonthlyDataNotes += notesRange.Cells[c, 1].Value.ToString() + "/";


                lpg.Add(config);



            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipData(" + i + ")";
            }



            return lpg;



        }



        public LPGShipping_Company_Information GetCompanyInformation()
        {

            
            LPGShipping_Company_Information comp = new LPGShipping_Company_Information();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Company-Level Input"];
            try
            {

                comp.CompanyName = xlWorkSheet.get_Range("Company").Value2;
                comp.Location = xlWorkSheet.get_Range("Location").Value2;
                comp.ContactName = xlWorkSheet.get_Range("CoordName").Value2;
                comp.ContactTitle = xlWorkSheet.get_Range("CoordTitle").Value2;
                comp.Phone = xlWorkSheet.get_Range("CoordPhone").Value2.ToString();
                comp.Email = xlWorkSheet.get_Range("CoordEmail").Value2;
                


            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetCompanyInformation";
            }

            return comp;

        }
        public List<LPGShipping_Utilization> GetShipUtilization(LPGShipping_T1_FleetInformation ship)
        {

            int i = 1;
            List<LPGShipping_Utilization> lpg = new List<LPGShipping_Utilization>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets[ship.ShipName + " Utilization"];
            try
            {


                LPGShipping_Utilization config = new LPGShipping_Utilization();

                config.UnitID = ship.UnitID;

                config.ShipName = ship.ShipName;
                config.Month = xlWorkSheet.get_Range("Month").Value2;
                config.Year = Convert.ToInt32(xlWorkSheet.get_Range("Year").Value2);
                config.ShipCapacityCBM = Convert.ToInt32(xlWorkSheet.get_Range("ShipCapacityCBM").Value2);
                config.ShipHoursReported = Convert.ToInt32(xlWorkSheet.get_Range("ShipHoursReported").Value2);
                config.ShipCapacityCBMHR = Convert.ToDecimal(xlWorkSheet.get_Range("ShipCapacityCBMHR").Value2);
                config.TotalAuthVolumeCBM = Convert.ToInt32(xlWorkSheet.get_Range("TotalAuthVolumeCBM").Value2);
                config.TotalAuthorizedHrs = Convert.ToInt32(xlWorkSheet.get_Range("TotalAuthorizedHrs").Value2);
                config.TotalAuthorizedShippingCapacity = Convert.ToInt32(xlWorkSheet.get_Range("TotalAuthorizedShippingCapacity").Value2);
                config.TotalRealizedShippingCapacityCBMHR = Convert.ToDecimal(xlWorkSheet.get_Range("TotalRealizedShippingCapacityCBMHR").Value2);

                lpg.Add(config);


            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipUtilization(" + i + ")";
            }

            return lpg;

        }


        public List<LPGShipping_T1_FleetInformation> GetFleetInformation()
        {

            int i = 1;
            List<LPGShipping_T1_FleetInformation> lpg = new List<LPGShipping_T1_FleetInformation>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(FleetLevelInputSheet);
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("FleetInformation");

                int NumCols = rFleet.Columns.Count;




                for (i = 1; i <= NumCols; i++)
                {
                    if (rFleet.Cells[2, i].Value() != null)
                    {
                        LPGShipping_T1_FleetInformation config = new LPGShipping_T1_FleetInformation();

                        config.UnitID = Convert.ToInt32(xlWorkSheet.get_Range(UnitIDRange).Cells[1, i].value);
                        config.ShipName = rFleet.Cells[1, i].Value();
                        config.ShipType = rFleet.Cells[2, i].Value();
                        config.ShipSubType = rFleet.Cells[3, i].Value();
                        config.PrimaryService = rFleet.Cells[4, i].Value();
                        config.Flag = rFleet.Cells[5, i].Value();
                        config.CapacityDWT = Convert.ToInt32(rFleet.Cells[6, i].Value);
                        config.CapacityCBM = Convert.ToInt32(rFleet.Cells[7, i].Value);
                        config.NumTanks = Convert.ToInt32(rFleet.Cells[8, i].Value);
                        config.TankType = rFleet.Cells[9, i].Value();
                        config.LGCGroups = rFleet.Cells[10, i].Value();
                        config.PressureMAX = Convert.ToInt32(rFleet.Cells[11, i].Value);
                        config.TempMIN = Convert.ToInt32(rFleet.Cells[12, i].Value);

                        lpg.Add(config);
                    }
                    else
                    {
                        break;
                    }
                }

                NumberShips = i - 1;
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetFleetInformation(" + i + ")";
            }

            return lpg;

        }

        public List<LPGShipping_UtilizationTrips> GetShipUtilizationTrips(LPGShipping_T1_FleetInformation s)
        {

            int i = 1;
            List<LPGShipping_UtilizationTrips> lpg = new List<LPGShipping_UtilizationTrips>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(s.ShipName + " " + Utilization);
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("Trips");

                int NumRows = rFleet.Rows.Count;

                for (i = 1; i <= NumRows; i++)
                {

                    LPGShipping_UtilizationTrips config = new LPGShipping_UtilizationTrips();
                    if (Convert.ToDecimal(rFleet.Cells[i, 1].Value()) != null)
                    {
                        config.UnitID = s.UnitID;
                        config.ShipName = s.ShipName;
                        config.TripNo = i;
                        config.VolumeCBM = Convert.ToDecimal(rFleet.Cells[i, 1].Value());
                        config.ActualVoyageHours = Convert.ToInt32(rFleet.Cells[i, 2].Value());
                        config.AuthorizedShippingCapacityCBMHR = Convert.ToDecimal(rFleet.Cells[i, 3].Value());
                        config.ActualVoyageHours = Convert.ToInt32(rFleet.Cells[i, 4].Value());
                        config.RealizedShippingCapacityCBMHR = Convert.ToDecimal(rFleet.Cells[i, 5].Value());
                        config.TripType = rFleet.Cells[i, 6].Value;
                        config.Comment = rFleet.Cells[i, 7].Value;


                        lpg.Add(config);
                    }
                }


            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetUtilizationTrips(" + i + ")";
            }

            return lpg;

        }


        public List<LPGShipping_T2_Engines> GetShipMainEngines(int ShipNo)
        {
            int i = 1;
            List<LPGShipping_T2_Engines> scfg = new List<LPGShipping_T2_Engines>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(FleetLevelInputSheet);
            try
            {

                int NumItems = Convert.ToInt32(xlWorkSheet.get_Range(NumEnginesRange).Cells[1, ShipNo].Value);



                for (i = 1; i <= NumItems; i++)
                {
                    Excel.Range rFleet = xlWorkSheet.get_Range("Engine" + i);


                    LPGShipping_T2_Engines config = new LPGShipping_T2_Engines();

                    config.UnitID = Convert.ToInt32(xlWorkSheet.get_Range(UnitIDRange).Cells[1, i].value);
                    config.EngineNo = i;
                    config.Horsepower = Convert.ToInt32(rFleet.Cells[1, 1].Value);
                    config.EngineType = rFleet.Cells[2, 1].Value;
                    config.Fuel = rFleet.Cells[3, 1].Value;
                    config.Manufacturer = rFleet.Cells[4, 1].Value();
                    config.Model = rFleet.Cells[5, 1].Value;
                    config.Auxillary = false;

                    scfg.Add(config);

                }
            }

            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetMainEngines(" + i + ")";
            }

            return scfg;


        }


        public List<LPGShipping_T2_Engines> GetShipAuxEngines(int ShipNo)
        {
            int i = 1;
            List<LPGShipping_T2_Engines> scfg = new List<LPGShipping_T2_Engines>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(FleetLevelInputSheet);
            try
            {



                int NumItems = Convert.ToInt32(xlWorkSheet.get_Range(NumAuxEnginesRange).Cells[1, ShipNo].Value);



                for (i = 1; i <= NumItems; i++)
                {
                    Excel.Range rFleet = xlWorkSheet.get_Range("AuxEngine" + i);


                    LPGShipping_T2_Engines config = new LPGShipping_T2_Engines();

                    config.UnitID = Convert.ToInt32(xlWorkSheet.get_Range(UnitIDRange).Cells[1, i].value);
                    config.EngineNo = i;
                    config.Horsepower = Convert.ToInt32(rFleet.Cells[1, 1].Value);
                    config.EngineType = rFleet.Cells[2, 1].Value;
                    config.Fuel = rFleet.Cells[3, 1].Value;
                    config.Manufacturer = rFleet.Cells[4, 1].Value;
                    config.Model = rFleet.Cells[5, 1].Value;
                    config.Auxillary = true;

                    scfg.Add(config);

                }
            }

            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipAuxEngines(" + i + ")";
            }

            return scfg;


        }



        public List<LPGShipping_T2_Generators> GetShipGenerators(int ShipNo)
        {
            int i = 1;
            List<LPGShipping_T2_Generators> scfg = new List<LPGShipping_T2_Generators>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(FleetLevelInputSheet);
            try
            {



                int NumItems = Convert.ToInt32(xlWorkSheet.get_Range(NumGeneratorsRange).Cells[1, ShipNo].Value);



                for (i = 1; i <= NumItems; i++)
                {
                    Excel.Range rFleet = xlWorkSheet.get_Range("MainGenerator" + i);


                    LPGShipping_T2_Generators config = new LPGShipping_T2_Generators();

                    config.UnitID = Convert.ToInt32(xlWorkSheet.get_Range(UnitIDRange).Cells[1, i].value);
                    config.GeneratorNo = i;
                    config.Kilowatts = Convert.ToInt32(rFleet.Cells[1, 1].Value);
                    config.Fuel = rFleet.Cells[2, 1].Value;
                    config.Additional = false;

                    scfg.Add(config);

                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipGenerators(" + i + ")";
            }

            return scfg;


        }


        public List<LPGShipping_T2_Generators> GetShipAdditionalGenerators(int ShipNo)
        {
            int i = 1;
            List<LPGShipping_T2_Generators> scfg = new List<LPGShipping_T2_Generators>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(FleetLevelInputSheet);
            try
            {



                int NumItems = Convert.ToInt32(xlWorkSheet.get_Range(NumAddGeneratorsRange).Cells[1, ShipNo].Value);



                for (i = 1; i <= NumItems; i++)
                {
                    Excel.Range rFleet = xlWorkSheet.get_Range("AddGenerator" + i);


                    LPGShipping_T2_Generators config = new LPGShipping_T2_Generators();
                    config.UnitID = Convert.ToInt32(xlWorkSheet.get_Range(UnitIDRange).Cells[1, i].value);
                    config.GeneratorNo = i;
                    config.Kilowatts = Convert.ToInt32(rFleet.Cells[1, 1].Value);
                    config.Fuel = rFleet.Cells[2, 1].Value;
                    config.Additional = true;

                    scfg.Add(config);

                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetAdditionalGenerators(" + i + ")";
            }

            return scfg;


        }


        public List<LPGShipping_T2_CargoPumps> GetCargoPumps(int ShipNo)
        {
            int i = 1;
            List<LPGShipping_T2_CargoPumps> scfg = new List<LPGShipping_T2_CargoPumps>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(FleetLevelInputSheet);
            try
            {



                int NumItems = Convert.ToInt32(xlWorkSheet.get_Range(NumCargoPumpsRange).Cells[1, ShipNo].Value);  //Cell L:132



                for (i = 1; i <= NumItems; i++)
                {
                    Excel.Range rFleet = xlWorkSheet.get_Range("CargoPump" + i);


                    LPGShipping_T2_CargoPumps config = new LPGShipping_T2_CargoPumps();
                    config.UnitID = Convert.ToInt32(xlWorkSheet.get_Range(UnitIDRange).Cells[1, i].value);
                    config.CargoPumpNo = i;
                    config.PumpType = rFleet.Cells[1, 1].Value;
                    config.FlowRate = Convert.ToInt32(rFleet.Cells[2, 1].Value);
                    config.MotorType = rFleet.Cells[3, 1].Value;
                    config.Horsepower = Convert.ToInt32(rFleet.Cells[4, 1].Value);


                    scfg.Add(config);

                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetCargoPumps(" + i + ")";
            }

            return scfg;


        }


        public List<LPGShipping_T2_Evaporators> GetShipEvaporators(int ShipNo)
        {
            int i = 1;
            List<LPGShipping_T2_Evaporators> scfg = new List<LPGShipping_T2_Evaporators>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(FleetLevelInputSheet);
            try
            {



                int NumItems = Convert.ToInt32(xlWorkSheet.get_Range(NumEvaporatorsRange).Cells[1, ShipNo].Value);  //Cell L:132



                for (i = 1; i <= NumItems; i++)
                {
                    Excel.Range rFleet = xlWorkSheet.get_Range("Evaporator" + i);


                    LPGShipping_T2_Evaporators config = new LPGShipping_T2_Evaporators();
                    config.UnitID = Convert.ToInt32(xlWorkSheet.get_Range(UnitIDRange).Cells[1, i].value);
                    config.EvaporatorNo = i;
                    config.CapacityGPM = Convert.ToInt32(rFleet.Cells[1, 1].Value);
                    config.EvaporatorType = rFleet.Cells[2, 1].Value;
                    config.OtherWaterProductionUnit = false;

                    scfg.Add(config);

                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipEvaporators(" + i + ")";
            }

            return scfg;


        }

        public List<LPGShipping_T2_Evaporators> GetShipWaterProdUnits(int ShipNo)
        {
            int i = 1;
            List<LPGShipping_T2_Evaporators> scfg = new List<LPGShipping_T2_Evaporators>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = (Excel.Worksheet)excelSheets.get_Item(FleetLevelInputSheet);
            try
            {



                int NumItems = Convert.ToInt32(xlWorkSheet.get_Range(NumWaterProdUnitsRange).Cells[1, ShipNo].Value); //Cell L:143



                for (i = 1; i <= NumItems; i++)
                {
                    Excel.Range rFleet = xlWorkSheet.get_Range("WaterProdUnit" + i);


                    LPGShipping_T2_Evaporators config = new LPGShipping_T2_Evaporators();
                    config.UnitID = Convert.ToInt32(xlWorkSheet.get_Range(UnitIDRange).Cells[1, i].value);
                    config.EvaporatorNo = i;
                    config.CapacityGPM = Convert.ToInt32(rFleet.Cells[2, 1].Value);
                    config.EvaporatorType = rFleet.Cells[1, 1].Value;
                    config.OtherWaterProductionUnit = true;

                    scfg.Add(config);

                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipWaterProdUnits(" + i + ")";
            }

            return scfg;


        }

        public int? Write(int? str)
        {
            if (str == null)

                return 0;
            else
                return Convert.ToInt32(str);
        }

        public string Write(string str)
        {
            if (str == null)

                return "";
            else
                return str.ToString();
        }

    }
    

}
