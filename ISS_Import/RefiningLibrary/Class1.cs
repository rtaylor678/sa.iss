﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Refining;



namespace Refining
{
    //TABLE 1
    public class Refining_CompanyInfo
    {

        public int CompanyID { get; set; }
        public string Company { get; set; }
        public string BusUnitName { get; set; }
        public string Location { get; set; }
        public string DCName { get; set; }
        public string DCTitle { get; set; }
        public string DCAdd1 { get; set; }
        public string DCAdd2 { get; set; }
        public string DCCity { get; set; }
        public string DCState { get; set; }
        public string DCPostalCode { get; set; }
        public string DCCountry { get; set; }
        public string DCPhone { get; set; }
        public string DCEmail { get; set; }
    }


    //TABLE 1
    public class Refining_Airport
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int AirportID { get; set; }
        public string Airport { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zip { get; set; }
        public string AirportFile { get; set; }
        public string OrgChartFile { get; set; }
        public int? JetFuelVol { get; set; }
        public int? AVGASVol { get; set; }
        public int? OtherVol { get; set; }
        public string JetFuelVolNote { get; set; }
        public string AVGASVolNote { get; set; }
        public string OtherVolNote { get; set; }
        public int? NoAprons { get; set; }
        public int? NoTanks { get; set; }
        public int? NoHydrants { get; set; }
        public int? NoFuelers { get; set; }
        public int? NoCarts { get; set; }
        public int? NoLoadStations { get; set; }
        public int? NoPumpStations { get; set; }
        public int? NoPumps { get; set; }

    }

    public class Refining_Apron
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int AirportID { get; set; }
        public string Information { get; set; }
        public string Name { get; set; }
        public string DeliverySystem { get; set; }
        public string FuelingServices { get; set; }
        public string AircraftTypes { get; set; }
        public string JetFuelClasses { get; set; }
        public string AviationGas { get; set; }
        public string DefuelingServices { get; set; }
        public string Note { get; set; }

    }

    public class Refining_Tank
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public string Information { get; set; }
        public string ID { get; set; }
        public string Product { get; set; }
        public string Type { get; set; }
        public int? Volume { get; set; }
        public string YearBuilt { get; set; }
        public string YearLastTA { get; set; }
        public int? LastInspectionCost { get; set; }
        public string CycleTime { get; set; }
        public string Note { get; set; }

    }

    public class Refining_Hydrant
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public string Information { get; set; }
        public string Name { get; set; }
        public string Product { get; set; }
        public int? Pressure { get; set; }
        public int? PitValves { get; set; }
        public int? Dispensers { get; set; }
        public int? NoApronsServed { get; set; }
        public string Note { get; set; }

    }


    public class Refining_Fueler
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public string ID { get; set; }
        public string Name { get; set; }
        public int? Capacity { get; set; }
        public string Note { get; set; }

    }

    public class Refining_FuelingCart
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public string Information { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }

    }


    public class Refining_LoadingStation
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public string Information { get; set; }
        public string Name { get; set; }
        public int? Capacity { get; set; }
        public string Note { get; set; }

    }


    public class Refining_PumpStation
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public string Information { get; set; }
        public string Name { get; set; }
        public int? HydrantPumps { get; set; }
        public int? TransferPumps { get; set; }
        public string Note { get; set; }

    }


    public class Refining_Pump
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public string Information { get; set; }
        public string Name { get; set; }
        public int? Capacity { get; set; }
        public string Service { get; set; }
        public string Note { get; set; }

    }

    public class Refining_MiscEquipment
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public string Information { get; set; }
        public int? Count { get; set; }
        public int? Capacity { get; set; }
        public string Service { get; set; }
        public string Note { get; set; }

    }


    //TABLE 2
    public class Refining_Maintenance
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public int? TotalCapitalExp { get; set; }
        public int? DiscExp { get; set; }
        public int? NonDiscExp { get; set; }
        public int? OverheadCapital { get; set; }
        public int? TotalExp { get; set; }
        public int? CoLaborCapitalExp { get; set; }
        public int? CoLaborMaintExp { get; set; }
        public int? TotalCompLaborMaint { get; set; }
        public int? CoMaterialsExp { get; set; }
        public int? CoMaterialsMaintExp { get; set; }
        public int? TotalCoMaterials { get; set; }
        public int? TotalMaintCapExp { get; set; }
        public int? TotalMaintExp { get; set; }
        public int? TotalMaint { get; set; }
        public int? IntMaintCapExp { get; set; }
        public int? IntMaintExp { get; set; }
        public int? TotalInt { get; set; }
        public int? GTMaintCapExp { get; set; }
        public int? GTMaintExp { get; set; }
        public int? GTMaint { get; set; }
    }

    //TABLE2
    public class Refining_Operations
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public int? JetFuelVolMo { get; set; }
        public int? JetFuelRecord { get; set; }
        public string OpJetFuelNote { get; set; }
        public int? AVGASVolMo { get; set; }
        public int? AVGASRecord { get; set; }
        public string OpAVGASNote { get; set; }
        public int? OtherVolMo { get; set; }
        public int? OtherRecord { get; set; }
        public string OpOtherNote { get; set; }
        public int? TotalFuelVolMo { get; set; }
        public int? TotalRecord { get; set; }

        public int? UnderwingCount { get; set; }
        public int? UnderwingRecord { get; set; }
        public string UnderwingNote { get; set; }
        public int? OverwingCount { get; set; }
        public int? OverwingRecord { get; set; }
        public string OverwingNote { get; set; }
        public int? RotaryWingCount { get; set; }
        public int? RotaryRecord { get; set; }
        public string RotaryWingNote { get; set; }
        public int? OtherCount { get; set; }
        public int? OtherAircraftRecord { get; set; }
        public string OtherAircraftNote { get; set; }
        public int? TotalAircraft { get; set; }
        public int? TotalAircraftRecord { get; set; }

        public int? CoOpCausesCount { get; set; }
        public int? CoOpHours { get; set; }
        public string CoOpNote { get; set; }
        public int? CoMechCausesCount { get; set; }
        public int? CoMechHours { get; set; }
        public string CoMechNote { get; set; }
        public int? NonCoCausesCount { get; set; }
        public int? NonCoHours { get; set; }
        public string NonCoNote { get; set; }
        public int? TotalEvents { get; set; }
        public int? TotalEventHours { get; set; }
        public int? SpillsCount { get; set; }
        public int? SpillsVol { get; set; }
        public string SpillsNote { get; set; }
        public int? UnknownCount { get; set; }
        public int? UnknownVol { get; set; }
        public string UnknownNote { get; set; }
        public int? IsMassBalance { get; set; }
        public string IsMassBalanceNote { get; set; }
        public string FreqMassBalance { get; set; }
        public string FreqMassBalanceNote { get; set; }
        public string IsVolOrMass { get; set; }
        public string IsVolOrMassNote { get; set; }
        public string IsDryOrWet { get; set; }
        public string IsDryOrWetNote { get; set; }
        public int? TotalReceipts { get; set; }
        public string TotalReceiptsNote { get; set; }
        public int? TotalDisposals { get; set; }
        public string TotalDisposalsNote { get; set; }
        public int? TotalICTanks { get; set; }
        public string TotalICTanksNote { get; set; }
        public int? TotalICFuelers { get; set; }
        public string TotalICFuelersNote { get; set; }
        public int? TotalDeliveries { get; set; }
        public string TotalDeliveriesNote { get; set; }
        public int? Loss { get; set; }
        public string LossNote { get; set; }
        public int? LossPercent { get; set; }
        public string LossPercentNote { get; set; }
    }


    //TABLE 2
    public class Refining_Personnel
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }

        public int? OpActEmpCount { get; set; }
        public int? OpActWorkHours { get; set; }
        public int? OpActSWB { get; set; }
        public int? OpActTotalExp { get; set; }
        public int? OpActContLabor { get; set; }
        public int? OpActAvgContRate { get; set; }
        public int? TotalContHours { get; set; }
        public int? GTOpPersCost { get; set; }
        public int? GTOpWorkHours { get; set; }
        public string OpActNote { get; set; }

        public int? MaintActEmpCount { get; set; }
        public int? MaintActWorkHours { get; set; }
        public int? MaintActSWB { get; set; }
        public int? MaintActTotalExp { get; set; }
        public int? MaintActContLabor { get; set; }
        public int? MaintActAvgContRate { get; set; }
        public int? TotalMaintContHours { get; set; }
        public int? GTMaintPersCost { get; set; }
        public int? GTMaintWorkHours { get; set; }
        public string MaintActNote { get; set; }

        public int? TechSuppEmpCount { get; set; }
        public int? TechSuppWorkHours { get; set; }
        public int? TechSuppSWB { get; set; }
        public int? TechSuppTotalExp { get; set; }
        public int? TechSuppContLabor { get; set; }
        public int? TechSuppAvgContRate { get; set; }
        public int? TotalTechSuppHours { get; set; }
        public int? GTTechSuppPersCost { get; set; }
        public int? GTTechSuppWorkHours { get; set; }
        public string TechSuppNote { get; set; }

        public int? AdminEmpCount { get; set; }
        public int? AdminWorkHours { get; set; }
        public int? AdminSWB { get; set; }
        public int? AdminTotalExp { get; set; }
        public int? AdminContLabor { get; set; }
        public int? AdminAvgContRate { get; set; }
        public int? TotalAdminHours { get; set; }
        public int? GTAdminPersCost { get; set; }
        public int? GTAdminWorkHours { get; set; }
        public string AdminNote { get; set; }


    }


    //TABLE 2
    public class Refining_Energy
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public int? ElectMW { get; set; }
        public int? ThermalGJ { get; set; }
        public int? DieselUSG { get; set; }
        public int? GasUSG { get; set; }
        public int? CNG { get; set; }
        public int? LPG { get; set; }
        public int? Propane { get; set; }
        public int? Ethanol { get; set; }
        public int? ElectricDriven { get; set; }
        public int? OtherFuels { get; set; }
        public int? TotalAFV { get; set; }
        public int? ElectUnitCost { get; set; }
        public int? ThermalEnergyUnitCost { get; set; }
        public int? DieselUnitCost { get; set; }
        public int? GasUnitCost { get; set; }
        public int? CNGUnitCost { get; set; }
        public int? LPGUnitCost { get; set; }
        public int? EthanolUnitCost { get; set; }
        public int? OtherFuelsUnitCost { get; set; }

        public string ElectMWNote { get; set; }
        public string ThermalGJNote { get; set; }
        public string DieselUSGNote { get; set; }
        public string GasUSGNote { get; set; }
        public string CNGNote { get; set; }
        public string LPGNote { get; set; }
        public string PropaneNote { get; set; }
        public string EthanolNote { get; set; }
        public string ElectricDrivenNote { get; set; }
        public string OtherFuelsNote { get; set; }
        public string TotalAFVNote { get; set; }
        public string ElectUnitCostNote { get; set; }
        public string ThermalEnergyUnitCostNote { get; set; }
        public string DieselUnitCostNote { get; set; }
        public string GasUnitCostNote { get; set; }
        public string CNGUnitCostNote { get; set; }
        public string LPGUnitCostNote { get; set; }
        public string EthanolUnitCostNote { get; set; }
        public string OtherFuelsUnitCostNote { get; set; }

    }

    //TABLE 2
    public class Refining_OtherCosts
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public int? VehicleExp { get; set; }
        public int? MiscOpExp { get; set; }
        public int? TotalNonPersExp { get; set; }
        public int? FeesExp { get; set; }
        public int? AltFuelExp { get; set; }
        public int? OtherVolRelatedExp { get; set; }
        public int? FireSafetyExp { get; set; }
        public int? FinesExp { get; set; }
        public int? SpillResponseExp { get; set; }
        public string AOGExp { get; set; }

        public string VehicleExpNote { get; set; }
        public string MiscOpExpNote { get; set; }
        public string TotalNonPersExpGNote { get; set; }
        public string FeesExpNote { get; set; }
        public string AltFuelExpNote { get; set; }
        public string OtherVolRelatedExpNote { get; set; }
        public string FireSafetyExpNote { get; set; }
        public string FinesExpNote { get; set; }
        public string SpillResponseExpNote { get; set; }
        public string AOGExpNote { get; set; }

    }


    public class Refining
    {
        //Open Excel Application
        Excel.Application excel;
        Excel.Workbook excelWorkbook;

        //Initialize Connection and Command
        SqlConnection cn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();


        string CONNECTSTR = Properties.Settings.Default.CONN;
        public string Error = null;
        string CurrentLogFile = null;
        int SubmissionID = 0;
        int CompanyID = 0;
        int AirportID = 0;

        public Refining(string cs)
        {
            CurrentLogFile = cs;
        }






        public string ImportRefining(string RefiningPath)
        {
            string rtn = "";
            cn.ConnectionString = CONNECTSTR;
            cn.Open();

            //Open Workbook
            excel = new Excel.Application();

            excelWorkbook = excel.Workbooks.Open(RefiningPath,
                0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "",
                true, false, 0, true, false, false);



            //Initialize the Objects
            Refining_CompanyInfo Contact = new Air.Refining_CompanyInfo();
            Refining_Airport Airport = new Refining_Airport();
            List<Refining_Apron> Aprons = new List<Refining_Apron>();
            Refining_Energy Energy = new Refining_Energy();
            List<Refining_Fueler> Fuelers = new List<Refining_Fueler>();
            List<Refining_FuelingCart> Carts = new List<Refining_FuelingCart>();
            List<Refining_Hydrant> Hydrants = new List<Refining_Hydrant>();
            List<Refining_LoadingStation> LoadStations = new List<Refining_LoadingStation>();
            Refining_Maintenance Maintenance = new Refining_Maintenance();
            List<Refining_MiscEquipment> MiscEquipment = new List<Refining_MiscEquipment>();
            Refining_Operations Operations = new Refining_Operations();
            Refining_OtherCosts OtherCosts = new Refining_OtherCosts();
            Refining_Personnel Personnel = new Refining_Personnel();
            List<Refining_Pump> Pumps = new List<Refining_Pump>();
            List<Refining_PumpStation> PumpStations = new List<Refining_PumpStation>();
            List<Refining_Tank> Tanks = new List<Refining_Tank>();

            Helper.WriteLog("--------------------------------------------------------------------------------------------------------------------------------------------", @"Refining.log");
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Refining Import Started", CurrentLogFile);
            //Read Data from Excel to Objects
            Contact = GetCompanyInfo();
            CompanyID = WriteCompanyInfo(Contact);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Contacts Read", CurrentLogFile);
            //Get Next SubmissionID
            SubmissionID = Helper.GetNextSubmissionID(CONNECTSTR, CompanyID);
            Helper.WriteSubmission(CONNECTSTR, SubmissionID, CompanyID);
            Airport = GetAirport();
            AirportID = Airport.AirportID;
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Airports Read", CurrentLogFile);
            Maintenance = GetMaintenance();
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Maintenance Read", CurrentLogFile);
            Operations = GetOperations();
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Operations Read", CurrentLogFile);
            Personnel = GetPersonnel();
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Personnel Read", CurrentLogFile);
            Energy = GetEnergy();
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Energy Read", CurrentLogFile);
            OtherCosts = GetOtherCosts();
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": OtherCosts Read", CurrentLogFile);
            //Write Objects to Database
            WriteCompanyInfo(Contact);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Contacts Written to Database", CurrentLogFile);
            WriteAirports(Airport);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Airports Written to Database", CurrentLogFile);
            WriteMaintenance(Maintenance);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Maintenance Written to Database", CurrentLogFile);
            WriteOperations(Operations);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Operations Written to Database", CurrentLogFile);
            WritePersonnel(Personnel);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Personnel Written to Database", CurrentLogFile);
            WriteEnergy(Energy);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Personnel Written to Database", CurrentLogFile);


            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Reading Aprons, Fuelers, Carts, Hyrdrants, LoadStations, Pumps, PumpStations and Tanks", CurrentLogFile);
            //Read & Write Engines, Generators, Evaporators and Cargo Pumps


            Aprons.Clear();
            Fuelers.Clear();
            Carts.Clear();
            Hydrants.Clear();
            LoadStations.Clear();
            Pumps.Clear();
            PumpStations.Clear();
            Tanks.Clear();

            Aprons = GetAprons();
            Fuelers = GetFuelers();
            Carts = GetCarts();
            Hydrants = GetHydrants();
            LoadStations = GetLoadStations();
            Pumps = GetPumps();
            PumpStations = GetPumpStations();
            Tanks = GetTanks();
            MiscEquipment = GetMiscEquipment();
            OtherCosts = GetOtherCosts();


            WriteAprons(Aprons);
            WriteFuelers(Fuelers);
            WriteFuelingCarts(Carts);
            WriteHydrants(Hydrants);
            WriteLoadingStations(LoadStations);
            WritePumps(Pumps);
            WritePumpStations(PumpStations);
            WriteTanks(Tanks);
            WriteMiscEquipment(MiscEquipment);
            WriteOtherCosts(OtherCosts);





            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Reading Aprons, Fuelers, Carts, Hyrdrants, LoadStations, Pumps, PumpStations and Tanks Written to Database", CurrentLogFile);

            rtn = "Complete";
            Helper.WriteLog("SubmissionID:" + SubmissionID + " - Refining Import Complete", CurrentLogFile);

            //Close Connection
            cn.Close();

            //Close and Destroy Excel Object
            excelWorkbook.Close(SaveChanges: false);
            excelWorkbook = null;

            excel = null;
            return rtn;
        }


        private Refining_Airport GetAirport()
        {
            Refining_Airport comp = new Refining_Airport();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {
                comp.CompanyID = CompanyID;
                comp.Airport = xlWorkSheet.get_Range("Airport").Value2;
                comp.City = xlWorkSheet.get_Range("AirportCity").Value2;
                comp.State = xlWorkSheet.get_Range("AirportState").Value2;
                comp.Country = xlWorkSheet.get_Range("AirportCountry").Value2;
                comp.AirportFile = xlWorkSheet.get_Range("AirportMapFile").Value2;
                comp.OrgChartFile = xlWorkSheet.get_Range("OrgChartFile").Value2;
                comp.JetFuelVol = xlWorkSheet.get_Range("JetFuelVol").Value2;
                comp.AVGASVol = xlWorkSheet.get_Range("AVGASVol").Value2;
                comp.OtherVol = xlWorkSheet.get_Range("OtherVol").Value2;
                comp.JetFuelVolNote = xlWorkSheet.get_Range("JetFuelNote").Value2;
                comp.AVGASVolNote = xlWorkSheet.get_Range("AVGASNote").Value2;
                comp.OtherVolNote = xlWorkSheet.get_Range("OtherNote").Value2;
                comp.NoAprons = Convert.ToInt32(xlWorkSheet.get_Range("NoAprons").Value2);
                comp.NoCarts = Convert.ToInt32(xlWorkSheet.get_Range("NoCarts").Value2);
                comp.NoFuelers = Convert.ToInt32(xlWorkSheet.get_Range("NoFuelers").Value2);
                comp.NoHydrants = Convert.ToInt32(xlWorkSheet.get_Range("NoHydrants").Value2);
                comp.NoLoadStations = Convert.ToInt32(xlWorkSheet.get_Range("NoLoadStations").Value2);
                comp.NoPumps = Convert.ToInt32(xlWorkSheet.get_Range("NoPumps").Value2);
                comp.NoPumpStations = Convert.ToInt32(xlWorkSheet.get_Range("NoPumpStations").Value2);
                comp.NoTanks = Convert.ToInt32(xlWorkSheet.get_Range("NoTanks").Value2);



            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetAirport";
            }

            return comp;

        }

        private void WriteAirports(Refining_Airport airport)
        {

            cmd.CommandText = "Refining_InsertAirport";
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@Airport", (airport.Airport == null) ? "" : airport.Airport));
            cmd.Parameters.Add(new SqlParameter("@AirportID", airport.AirportID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", airport.CompanyID));
            cmd.Parameters.Add(new SqlParameter("@City", (airport.City == null) ? "" : airport.City));
            cmd.Parameters.Add(new SqlParameter("@State", (airport.State == null) ? "" : airport.State));
            cmd.Parameters.Add(new SqlParameter("@Country", (airport.Country == null) ? "" : airport.Country));
            cmd.Parameters.Add(new SqlParameter("@AirportFile", (airport.AirportFile == null) ? "" : airport.AirportFile));
            cmd.Parameters.Add(new SqlParameter("@OrgChartFile", (airport.OrgChartFile == null) ? "" : airport.OrgChartFile));
            cmd.Parameters.Add(new SqlParameter("@JetFuelVol", (airport.JetFuelVol == null) ? 0 : airport.JetFuelVol));
            cmd.Parameters.Add(new SqlParameter("@AVGASVol", (airport.AVGASVol == null) ? 0 : airport.AVGASVol));
            cmd.Parameters.Add(new SqlParameter("@OtherVol", (airport.OtherVol == null) ? 0 : airport.OtherVol));
            cmd.Parameters.Add(new SqlParameter("@NoAprons", (airport.NoAprons == null) ? 0 : airport.NoAprons));
            cmd.Parameters.Add(new SqlParameter("@NoTanks", (airport.AirportFile == null) ? 0 : airport.NoTanks));
            cmd.Parameters.Add(new SqlParameter("@NoHydrants", (airport.NoHydrants == null) ? 0 : airport.NoHydrants));
            cmd.Parameters.Add(new SqlParameter("@NoFuelers", (airport.NoFuelers == null) ? 0 : airport.NoFuelers));
            cmd.Parameters.Add(new SqlParameter("@NoCarts", (airport.NoCarts == null) ? 0 : airport.NoCarts));
            cmd.Parameters.Add(new SqlParameter("@NoLoadStations", (airport.NoLoadStations == null) ? 0 : airport.NoLoadStations));
            cmd.Parameters.Add(new SqlParameter("@NoPumpStations", (airport.NoPumpStations == null) ? 0 : airport.NoPumpStations));
            cmd.Parameters.Add(new SqlParameter("@NoPumps", (airport.NoPumps == null) ? 0 : airport.NoPumps));
            cmd.Parameters.Add(new SqlParameter("@JetFuelVolNote", (airport.JetFuelVolNote == null) ? "" : airport.JetFuelVolNote));
            cmd.Parameters.Add(new SqlParameter("@AVGASVolNote", (airport.AVGASVolNote == null) ? "" : airport.AVGASVolNote));
            cmd.Parameters.Add(new SqlParameter("@OtherVolNote", (airport.OtherVolNote == null) ? "" : airport.OtherVolNote));
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                ////ErrorHandler("SubmissionID:" + SubmissionID + ": WriteShipData", ex.Message);
                return;
            }



        }

        private List<Refining_Apron> GetAprons()
        {

            int i = 1;
            List<Refining_Apron> aprons = new List<Refining_Apron>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("Aprons");

                int NumRows = rFleet.Rows.Count;



                for (i = 1; i <= NumRows; i++)
                {

                    if (rFleet.Cells[i, 1].Value.Length > 0)
                    {
                        Refining_Apron config = new Refining_Apron();

                        config.Information = rFleet.Cells[i, 1].Value;
                        config.Name = rFleet.Cells[i, 2].Value;
                        config.DeliverySystem = rFleet.Cells[i, 3].Value;
                        config.FuelingServices = rFleet.Cells[i, 4].Value;
                        config.AircraftTypes = rFleet.Cells[i, 5].Value;
                        config.JetFuelClasses = rFleet.Cells[i, 6].Value;
                        config.AviationGas = rFleet.Cells[i, 7].Value;
                        config.DefuelingServices = rFleet.Cells[i, 8].Value;
                        config.Note = rFleet.Cells[i, 9].Value;

                        aprons.Add(config);
                    }

                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipConfiguration(" + i + ")";

            }

            return aprons;


        }


        private void WriteAprons(List<Refining_Apron> aprons)
        {

            cmd.CommandText = "Refining_InsertAprons";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_Apron apron in aprons)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@Information", (apron.Information == null) ? "" : apron.Information));
                cmd.Parameters.Add(new SqlParameter("@Name", (apron.Name == null) ? "" : apron.Name));
                cmd.Parameters.Add(new SqlParameter("@DeliverySystem", (apron.DeliverySystem == null) ? "" : apron.DeliverySystem));
                cmd.Parameters.Add(new SqlParameter("@FuelingServices", (apron.FuelingServices == null) ? "" : apron.FuelingServices));
                cmd.Parameters.Add(new SqlParameter("@AircraftTypes", (apron.AircraftTypes == null) ? "" : apron.AircraftTypes));
                cmd.Parameters.Add(new SqlParameter("@JetFuelClasses", (apron.JetFuelClasses == null) ? "" : apron.JetFuelClasses));
                cmd.Parameters.Add(new SqlParameter("@AviationGas", (apron.AviationGas == null) ? "" : apron.AviationGas));
                cmd.Parameters.Add(new SqlParameter("@DefuelingServices", (apron.DefuelingServices == null) ? "" : apron.DefuelingServices));
                cmd.Parameters.Add(new SqlParameter("@Note", (apron.Note == null) ? "" : apron.Note));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    ////ErrorHandler("SubmissionID:" + SubmissionID + ": WriteShipData", ex.Message);
                    return;
                }



            }
        }

        private Refining_OtherCosts GetOtherCosts()
        {


            Refining_OtherCosts OC = new Refining_OtherCosts();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Other Costs"];
            try
            {


                Refining_OtherCosts config = new Refining_OtherCosts();


                config.VehicleExp = xlWorkSheet.get_Range("VehicleExp").Value2;
                config.MiscOpExp = xlWorkSheet.get_Range("MiscOpExp").Value2;
                config.TotalNonPersExp = xlWorkSheet.get_Range("TotalNonPersExp").Value2;
                config.FeesExp = xlWorkSheet.get_Range("FeesExp").Value2;
                config.AltFuelExp = xlWorkSheet.get_Range("AltFuelExp").Value2;
                config.OtherVolRelatedExp = xlWorkSheet.get_Range("OtherVolRelatedExp").Value2;
                config.FireSafetyExp = xlWorkSheet.get_Range("FireSafetyExp").Value2;
                config.FinesExp = xlWorkSheet.get_Range("FinesExp").Value2;
                config.SpillResponseExp = xlWorkSheet.get_Range("SpillResponseExp").Value2;
                config.AOGExp = xlWorkSheet.get_Range("AOGExp").Value2;

                config.VehicleExpNote = xlWorkSheet.get_Range("VehicleExpNote").Value2;
                config.MiscOpExpNote = xlWorkSheet.get_Range("MiscOpExpNote").Value2;
                config.TotalNonPersExpGNote = xlWorkSheet.get_Range("TotalNonPersExpNote").Value2;
                config.FeesExpNote = xlWorkSheet.get_Range("FeesExpNote").Value2;
                config.AltFuelExpNote = xlWorkSheet.get_Range("AltFuelExpNote").Value2;
                config.OtherVolRelatedExpNote = xlWorkSheet.get_Range("OtherVolRelatedExpNote").Value2;
                config.FireSafetyExpNote = xlWorkSheet.get_Range("FireSafetyExpNote").Value2;
                config.FinesExpNote = xlWorkSheet.get_Range("FinesExpNote").Value2;
                config.SpillResponseExpNote = xlWorkSheet.get_Range("SpillResponseExpNote").Value2;
                config.AOGExpNote = xlWorkSheet.get_Range("AOGExpNote").Value2;




            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetOtherCosts";

            }

            return OC;


        }



        private void WriteOtherCosts(Refining_OtherCosts oc)
        {

            cmd.CommandText = "Refining_InsertOtherCost";
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            cmd.Parameters.Add(new SqlParameter("@VehicleExp", (oc.VehicleExp == null) ? 0 : oc.VehicleExp));
            cmd.Parameters.Add(new SqlParameter("@MiscOpExp", (oc.MiscOpExp == null) ? 0 : oc.MiscOpExp));
            cmd.Parameters.Add(new SqlParameter("@TotalNonPersExp", (oc.TotalNonPersExp == null) ? 0 : oc.TotalNonPersExp));
            cmd.Parameters.Add(new SqlParameter("@FeesExp", (oc.FeesExp == null) ? 0 : oc.FeesExp));
            cmd.Parameters.Add(new SqlParameter("@AltFuelExp", (oc.AltFuelExp == null) ? 0 : oc.AltFuelExp));
            cmd.Parameters.Add(new SqlParameter("@OtherVolRelatedExp", (oc.OtherVolRelatedExp == null) ? 0 : oc.OtherVolRelatedExp));
            cmd.Parameters.Add(new SqlParameter("@FireSafetyExp", (oc.FireSafetyExp == null) ? 0 : oc.FireSafetyExp));
            cmd.Parameters.Add(new SqlParameter("@FinesExp", (oc.FinesExp == null) ? 0 : oc.FinesExp));
            cmd.Parameters.Add(new SqlParameter("@SpillResponseExp", (oc.SpillResponseExp == null) ? 0 : oc.SpillResponseExp));

            cmd.Parameters.Add(new SqlParameter("@AOGExp", (oc.AOGExp == null) ? "" : oc.AOGExp));
            cmd.Parameters.Add(new SqlParameter("@VehicleExpNote", (oc.VehicleExpNote == null) ? "" : oc.VehicleExpNote));
            cmd.Parameters.Add(new SqlParameter("@MiscOpExpNote", (oc.MiscOpExpNote == null) ? "" : oc.MiscOpExpNote));
            cmd.Parameters.Add(new SqlParameter("@TotalNonPersExpGNote", (oc.TotalNonPersExpGNote == null) ? "" : oc.TotalNonPersExpGNote));
            cmd.Parameters.Add(new SqlParameter("@FeesExpNote", (oc.FeesExpNote == null) ? "" : oc.FeesExpNote));
            cmd.Parameters.Add(new SqlParameter("@AltFuelExpNote", (oc.AltFuelExpNote == null) ? "" : oc.AltFuelExpNote));

            cmd.Parameters.Add(new SqlParameter("@OtherVolRelatedExpNote", (oc.OtherVolRelatedExpNote == null) ? "" : oc.OtherVolRelatedExpNote));
            cmd.Parameters.Add(new SqlParameter("@FireSafetyExpNote", (oc.FireSafetyExpNote == null) ? "" : oc.FireSafetyExpNote));
            cmd.Parameters.Add(new SqlParameter("@FinesExpNote", (oc.FinesExpNote == null) ? "" : oc.FinesExpNote));
            cmd.Parameters.Add(new SqlParameter("@SpillResponseExpNote", (oc.SpillResponseExpNote == null) ? "" : oc.SpillResponseExpNote));
            cmd.Parameters.Add(new SqlParameter("@AOGExpNote", (oc.AOGExpNote == null) ? "" : oc.AOGExpNote));
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                ////ErrorHandler("SubmissionID:" + SubmissionID + ": WriteShipData", ex.Message);
                return;
            }




        }

        private void WriteTanks(List<Refining_Tank> tanks)
        {

            cmd.CommandText = "Refining_InsertTanks";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_Tank tank in tanks)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@Information", (tank.Information == null) ? "" : tank.Information));
                cmd.Parameters.Add(new SqlParameter("@ID", (tank.ID == null) ? "" : tank.ID));
                cmd.Parameters.Add(new SqlParameter("@Product", (tank.Product == null) ? "" : tank.Product));
                cmd.Parameters.Add(new SqlParameter("@Type", (tank.Type == null) ? "" : tank.Type));
                cmd.Parameters.Add(new SqlParameter("@Volume", (tank.Volume == null) ? 0 : tank.Volume));
                cmd.Parameters.Add(new SqlParameter("@YearBuilt", (tank.YearBuilt == null) ? "" : tank.YearBuilt));
                cmd.Parameters.Add(new SqlParameter("@YearLastTA", (tank.YearLastTA == null) ? "" : tank.YearLastTA));
                cmd.Parameters.Add(new SqlParameter("@LastInspectionCost", (tank.LastInspectionCost == null) ? 0 : tank.LastInspectionCost));
                cmd.Parameters.Add(new SqlParameter("@CycleTime", (tank.CycleTime == null) ? "" : tank.CycleTime));
                cmd.Parameters.Add(new SqlParameter("@Note", (tank.Note == null) ? "" : tank.Note));

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    ////ErrorHandler("SubmissionID:" + SubmissionID + ": WriteShipData", ex.Message);
                    return;
                }



            }
        }

        private List<Refining_Tank> GetTanks()
        {

            int i = 1;
            List<Refining_Tank> tanks = new List<Refining_Tank>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("Tanks");

                int NumRows = rFleet.Rows.Count;



                for (i = 1; i <= NumRows; i++)
                {

                    if (rFleet.Cells[i, 1].Value.Length > 0)
                    {
                        Refining_Tank config = new Refining_Tank();

                        config.Information = rFleet.Cells[i, 1].Value;
                        config.ID = rFleet.Cells[i, 2].Value.ToString();
                        config.Product = rFleet.Cells[i, 3].Value;
                        config.Type = rFleet.Cells[i, 4].Value;
                        config.Volume = Convert.ToInt32(rFleet.Cells[i, 5].Value);
                        config.YearBuilt = rFleet.Cells[i, 6].Value.ToString();
                        config.YearLastTA = (rFleet.Cells[i, 7].Value == null) ? "" : rFleet.Cells[i, 7].Value.ToString();
                        config.LastInspectionCost = Convert.ToInt32(rFleet.Cells[i, 8].Value);
                        config.CycleTime = (rFleet.Cells[i, 9].Value == null) ? "" : rFleet.Cells[i, 9].Value.ToString();
                        config.Note = rFleet.Cells[i, 10].Value;

                        tanks.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipConfiguration(" + i + ")";

            }

            return tanks;


        }

        private int WriteCompanyInfo(Refining_CompanyInfo contact)
        {
            int rtn = 0;
            cmd.CommandText = "Refining_InsertCompanyInformation";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;


            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@Company", (contact.Company == null) ? "" : contact.Company));
            cmd.Parameters.Add(new SqlParameter("@Location", (contact.Location == null) ? "" : contact.Location));
            cmd.Parameters.Add(new SqlParameter("@BusUnitName", (contact.BusUnitName == null) ? "" : contact.BusUnitName));
            cmd.Parameters.Add(new SqlParameter("@DCName", (contact.DCName == null) ? "" : contact.DCName));
            cmd.Parameters.Add(new SqlParameter("@DCTitle", (contact.DCTitle == null) ? "" : contact.DCTitle));
            cmd.Parameters.Add(new SqlParameter("@DCAdd1", (contact.DCAdd1 == null) ? "" : contact.DCAdd1));
            cmd.Parameters.Add(new SqlParameter("@DCAdd2", (contact.DCAdd2 == null) ? "" : contact.DCAdd2));
            cmd.Parameters.Add(new SqlParameter("@DCCity", (contact.DCCity == null) ? "" : contact.DCCity));
            cmd.Parameters.Add(new SqlParameter("@DCState", (contact.DCState == null) ? "" : contact.DCState));
            cmd.Parameters.Add(new SqlParameter("@DCCountry", (contact.DCCountry == null) ? "" : contact.DCCountry));
            cmd.Parameters.Add(new SqlParameter("@DCPostalCode", (contact.DCPostalCode == null) ? "" : contact.DCPostalCode));
            cmd.Parameters.Add(new SqlParameter("@DCTelephone", (contact.DCPhone == null) ? "" : contact.DCPhone));
            cmd.Parameters.Add(new SqlParameter("@DCEmail", (contact.DCEmail == null) ? "" : contact.DCEmail));


            try
            {
                rtn = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                ////ErrorHandler("SubmissionID:" + SubmissionID + ": WriteShipData", ex.Message);

            }

            return rtn;

        }


        private Refining_CompanyInfo GetCompanyInfo()
        {


            Refining_CompanyInfo comp = new Refining_CompanyInfo();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Contact Info"];
            try
            {

                comp.Company = xlWorkSheet.get_Range("Company").Value2;
                comp.Location = xlWorkSheet.get_Range("Location").Value2;
                comp.DCName = xlWorkSheet.get_Range("DCName").Value2;
                comp.DCTitle = xlWorkSheet.get_Range("DCTitle").Value2;
                comp.DCAdd1 = xlWorkSheet.get_Range("DCAdd1").Value2;
                comp.DCAdd2 = xlWorkSheet.get_Range("DCAdd2").Value2;
                comp.DCCity = xlWorkSheet.get_Range("DCCity").Value2;
                comp.DCState = xlWorkSheet.get_Range("DCState").Value2;
                comp.DCPostalCode = xlWorkSheet.get_Range("DCPostalCode").Value2;
                comp.DCCountry = xlWorkSheet.get_Range("DCCountry").Value2;
                comp.DCPhone = xlWorkSheet.get_Range("DCTelephone").Value2;
                comp.DCEmail = xlWorkSheet.get_Range("DCEmail").Value2;



            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetCompanyInfo";
            }

            return comp;

        }

        private List<Refining_Hydrant> GetHydrants()
        {

            int i = 1;
            List<Refining_Hydrant> Hydrants = new List<Refining_Hydrant>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("Hydrants");

                int NumRows = rFleet.Rows.Count;



                for (i = 1; i <= NumRows; i++)
                {

                    if (rFleet.Cells[i, 1].Value.Length > 0)
                    {
                        Refining_Hydrant config = new Refining_Hydrant();

                        config.Information = rFleet.Cells[i, 1].Value;
                        config.Name = rFleet.Cells[i, 2].Value;
                        config.Product = rFleet.Cells[i, 3].Value;
                        config.Pressure = Convert.ToInt32(rFleet.Cells[i, 4].Value);
                        config.PitValves = Convert.ToInt32(rFleet.Cells[i, 5].Value);
                        config.Dispensers = Convert.ToInt32(rFleet.Cells[i, 6].Value);
                        config.NoApronsServed = Convert.ToInt32(rFleet.Cells[i, 7].Value);
                        config.Note = rFleet.Cells[i, 8].Value;

                        Hydrants.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipConfiguration(" + i + ")";

            }

            return Hydrants;


        }

        private void WriteHydrants(List<Refining_Hydrant> hydrants)
        {

            cmd.CommandText = "Refining_InsertHydrants";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_Hydrant hydrant in hydrants)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@Information", (hydrant.Information == null) ? "" : hydrant.Information));
                cmd.Parameters.Add(new SqlParameter("@Name", (hydrant.Name == null) ? "" : hydrant.Name));
                cmd.Parameters.Add(new SqlParameter("@Product", (hydrant.Product == null) ? "" : hydrant.Product));
                cmd.Parameters.Add(new SqlParameter("@Pressure", (hydrant.Pressure == null) ? 0 : hydrant.Pressure));
                cmd.Parameters.Add(new SqlParameter("@PitValves", (hydrant.PitValves == null) ? 0 : hydrant.PitValves));
                cmd.Parameters.Add(new SqlParameter("@Dispensers", (hydrant.Dispensers == null) ? 0 : hydrant.Dispensers));
                cmd.Parameters.Add(new SqlParameter("@NoApronsServed", (hydrant.NoApronsServed == null) ? 0 : hydrant.NoApronsServed));
                cmd.Parameters.Add(new SqlParameter("@Note", (hydrant.Note == null) ? "" : hydrant.Note));



                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    ////ErrorHandler("SubmissionID:" + SubmissionID + ": WriteShipData", ex.Message);
                    return;
                }

            }

        }


        private List<Refining_Fueler> GetFuelers()
        {

            int i = 1;
            List<Refining_Fueler> Fuelers = new List<Refining_Fueler>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("Fuelers");

                int NumRows = rFleet.Rows.Count;



                for (i = 1; i <= NumRows; i++)
                {

                    if (rFleet.Cells[i, 1].Value.Length > 0)
                    {
                        Refining_Fueler config = new Refining_Fueler();

                        config.ID = rFleet.Cells[i, 1].Value;
                        config.Name = rFleet.Cells[i, 2].Value;
                        config.Capacity = Convert.ToInt32(rFleet.Cells[i, 3].Value);
                        config.Note = rFleet.Cells[i, 4].Value;

                        Fuelers.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetShipConfiguration(" + i + ")";

            }

            return Fuelers;


        }

        private void WriteFuelers(List<Refining_Fueler> fuelers)
        {

            cmd.CommandText = "Refining_InsertFuelers";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_Fueler fueler in fuelers)
            {

                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@Name", (fueler.Name == null) ? "" : fueler.Name));
                cmd.Parameters.Add(new SqlParameter("@Capacity", (fueler.Capacity == null) ? 0 : fueler.Capacity));
                cmd.Parameters.Add(new SqlParameter("@Note", (fueler.Note == null) ? "" : fueler.Note));



                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteFuelers : " + ex.Message;
                    return;
                }

            }

        }

        private List<Refining_FuelingCart> GetCarts()
        {

            int i = 1;
            List<Refining_FuelingCart> Fuelers = new List<Refining_FuelingCart>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("Carts");

                int NumRows = rFleet.Rows.Count;



                for (i = 1; i <= NumRows; i++)
                {

                    if (rFleet.Cells[i, 1].Value.Length > 0)
                    {
                        Refining_FuelingCart config = new Refining_FuelingCart();

                        config.Information = rFleet.Cells[i, 1].Value;
                        config.Name = rFleet.Cells[i, 2].Value;
                        config.Note = rFleet.Cells[i, 3].Value;

                        Fuelers.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetFuelingCarts : " + ex.Message;

            }

            return Fuelers;


        }

        private void WriteFuelingCarts(List<Refining_FuelingCart> fuelers)
        {

            cmd.CommandText = "Refining_InsertFuelingCarts";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_FuelingCart fueler in fuelers)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@Information", (fueler.Information == null) ? "" : fueler.Information));
                cmd.Parameters.Add(new SqlParameter("@Name", (fueler.Name == null) ? "" : fueler.Name));
                cmd.Parameters.Add(new SqlParameter("@Note", (fueler.Note == null) ? "" : fueler.Note));



                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteFuelingCarts : " + ex.Message;
                    return;
                }

            }

        }


        private List<Refining_LoadingStation> GetLoadStations()
        {

            int i = 1;
            List<Refining_LoadingStation> LoadStation = new List<Refining_LoadingStation>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("LoadStations");

                int NumRows = rFleet.Rows.Count;



                for (i = 1; i <= NumRows; i++)
                {

                    if (rFleet.Cells[i, 1].Value.Length > 0)
                    {
                        Refining_LoadingStation config = new Refining_LoadingStation();

                        config.Information = rFleet.Cells[i, 1].Value;
                        config.Name = rFleet.Cells[i, 2].Value;
                        config.Capacity = Convert.ToInt32(rFleet.Cells[i, 3].Value);
                        config.Note = rFleet.Cells[i, 4].Value;

                        LoadStation.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetLoadingStations : " + ex.Message;

            }

            return LoadStation;


        }


        private void WriteLoadingStations(List<Refining_LoadingStation> stations)
        {

            cmd.CommandText = "Refining_InsertLoadingStations";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_LoadingStation fueler in stations)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@Information", (fueler.Information == null) ? "" : fueler.Information));
                cmd.Parameters.Add(new SqlParameter("@Name", (fueler.Name == null) ? "" : fueler.Name));
                cmd.Parameters.Add(new SqlParameter("@Capacity", (fueler.Capacity == null) ? 0 : fueler.Capacity));
                cmd.Parameters.Add(new SqlParameter("@Note", (fueler.Note == null) ? "" : fueler.Note));



                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteLoadingStations : " + ex.Message;
                    return;
                }

            }

        }


        private List<Refining_PumpStation> GetPumpStations()
        {

            int i = 1;
            List<Refining_PumpStation> PumpStation = new List<Refining_PumpStation>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("PumpStations");

                int NumRows = rFleet.Rows.Count;



                for (i = 1; i <= NumRows; i++)
                {

                    if (rFleet.Cells[i, 1].Value.Length > 0)
                    {
                        Refining_PumpStation config = new Refining_PumpStation();

                        config.Information = rFleet.Cells[i, 1].Value;
                        config.Name = rFleet.Cells[i, 2].Value;
                        config.HydrantPumps = Convert.ToInt32(rFleet.Cells[i, 3].Value);
                        config.TransferPumps = Convert.ToInt32(rFleet.Cells[i, 4].Value);
                        config.Note = rFleet.Cells[i, 5].Value;

                        PumpStation.Add(config);

                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetPumpStation : " + ex.Message;

            }

            return PumpStation;


        }

        private void WritePumpStations(List<Refining_PumpStation> stations)
        {

            cmd.CommandText = "Refining_InsertPumpStations";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_PumpStation fueler in stations)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@Information", (fueler.Information == null) ? "" : fueler.Information));
                cmd.Parameters.Add(new SqlParameter("@Name", (fueler.Name == null) ? "" : fueler.Name));
                cmd.Parameters.Add(new SqlParameter("@HydrantPumps", (fueler.HydrantPumps == null) ? 0 : fueler.HydrantPumps));
                cmd.Parameters.Add(new SqlParameter("@TransferPumps", (fueler.TransferPumps == null) ? 0 : fueler.TransferPumps));
                cmd.Parameters.Add(new SqlParameter("@Note", (fueler.Note == null) ? "" : fueler.Note));



                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WritePumpStation : " + ex.Message;
                    return;
                }
            }


        }


        private List<Refining_Pump> GetPumps()
        {

            int i = 1;
            List<Refining_Pump> Pumps = new List<Refining_Pump>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("Pumps");

                int NumRows = rFleet.Rows.Count;



                for (i = 1; i <= NumRows; i++)
                {

                    if (rFleet.Cells[i, 1].Value.Length > 0)
                    {
                        Refining_Pump config = new Refining_Pump();

                        config.Information = rFleet.Cells[i, 1].Value;
                        config.Name = rFleet.Cells[i, 2].Value;
                        config.Capacity = Convert.ToInt32(rFleet.Cells[i, 3].Value);
                        config.Service = rFleet.Cells[i, 4].Value;
                        config.Note = rFleet.Cells[i, 5].Value;

                        Pumps.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetPumps : " + ex.Message;

            }

            return Pumps;


        }

        private void WritePumps(List<Refining_Pump> pumps)
        {

            cmd.CommandText = "Refining_InsertPumps";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_Pump fueler in pumps)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@Information", (fueler.Information == null) ? "" : fueler.Information));
                cmd.Parameters.Add(new SqlParameter("@Name", (fueler.Name == null) ? "" : fueler.Name));
                cmd.Parameters.Add(new SqlParameter("@Capacity", (fueler.Capacity == null) ? 0 : fueler.Capacity));
                cmd.Parameters.Add(new SqlParameter("@Service", (fueler.Service == null) ? "" : fueler.Service));
                cmd.Parameters.Add(new SqlParameter("@Note", (fueler.Note == null) ? "" : fueler.Note));



                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WritePumps : " + ex.Message;
                    return;
                }

            }

        }


        private List<Refining_MiscEquipment> GetMiscEquipment()
        {

            int i = 1;
            List<Refining_MiscEquipment> MiscEquipment = new List<Refining_MiscEquipment>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Airport Level Inputs"];
            try
            {

                Excel.Range rFleet = xlWorkSheet.get_Range("MiscEquip");

                int NumRows = rFleet.Rows.Count;



                for (i = 1; i <= NumRows; i++)
                {

                    if (rFleet.Cells[i, 1].Value.Length > 0)
                    {
                        Refining_MiscEquipment config = new Refining_MiscEquipment();

                        config.Information = rFleet.Cells[i, 1].Value;
                        config.Count = Convert.ToInt32(rFleet.Cells[i, 2].Value);
                        config.Capacity = Convert.ToInt32(rFleet.Cells[i, 3].Value);
                        config.Service = rFleet.Cells[i, 4].Value;
                        config.Note = rFleet.Cells[i, 5].Value;

                        MiscEquipment.Add(config);
                    }
                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetMiscEquipment : " + ex.Message;

            }

            return MiscEquipment;


        }
        private void WriteMiscEquipment(List<Refining_MiscEquipment> equip)
        {

            cmd.CommandText = "Refining_InsertMiscEquipment";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_MiscEquipment e in equip)
            {

                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@Information", (e.Information == null) ? "" : e.Information));
                cmd.Parameters.Add(new SqlParameter("@Service", (e.Service == null) ? "" : e.Service));
                cmd.Parameters.Add(new SqlParameter("@Count", (e.Count == null) ? 0 : e.Count));
                cmd.Parameters.Add(new SqlParameter("@Capacity", (e.Capacity == null) ? 0 : e.Capacity));
                cmd.Parameters.Add(new SqlParameter("@Note", (e.Note == null) ? "" : e.Note));



                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteMiscEquipment : " + ex.Message;
                    return;
                }

            }

        }

        private Refining_Maintenance GetMaintenance()
        {
            Refining_Maintenance com = new Refining_Maintenance();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Maintenance"];
            try
            {

                com.AirportID = AirportID;
                com.TotalCapitalExp = Convert.ToInt32(xlWorkSheet.get_Range("TotalCapitalExp").Value2);
                com.DiscExp = Convert.ToInt32(xlWorkSheet.get_Range("DiscExp").Value2);
                com.NonDiscExp = Convert.ToInt32(xlWorkSheet.get_Range("NonDiscExp").Value2);
                com.OverheadCapital = Convert.ToInt32(xlWorkSheet.get_Range("OverheadCapital").Value2);
                com.TotalExp = Convert.ToInt32(xlWorkSheet.get_Range("TotalExp").Value2);
                com.CoLaborCapitalExp = Convert.ToInt32(xlWorkSheet.get_Range("CoLaborCapitalExp").Value2);
                com.CoLaborMaintExp = Convert.ToInt32(xlWorkSheet.get_Range("CoLaborMaintExp").Value2);
                com.TotalCompLaborMaint = Convert.ToInt32(xlWorkSheet.get_Range("TotalCompLaborMaint").Value2);
                com.CoMaterialsExp = Convert.ToInt32(xlWorkSheet.get_Range("CoMaterialsExp").Value2);
                com.CoMaterialsMaintExp = Convert.ToInt32(xlWorkSheet.get_Range("CoMaterialsMaintExp").Value2);
                com.TotalCoMaterials = Convert.ToInt32(xlWorkSheet.get_Range("TotalCoMaterials").Value2);
                com.TotalMaintCapExp = Convert.ToInt32(xlWorkSheet.get_Range("TotalMaintCapExp").Value2);
                com.TotalMaintExp = Convert.ToInt32(xlWorkSheet.get_Range("TotalMaintExp").Value2);
                com.TotalMaint = Convert.ToInt32(xlWorkSheet.get_Range("TotalMaint").Value2);
                com.IntMaintCapExp = Convert.ToInt32(xlWorkSheet.get_Range("IntMaintCapExp").Value2);
                com.IntMaintExp = Convert.ToInt32(xlWorkSheet.get_Range("IntMaintExp").Value2);
                com.TotalInt = Convert.ToInt32(xlWorkSheet.get_Range("TotalInt").Value2);
                com.GTMaintCapExp = Convert.ToInt32(xlWorkSheet.get_Range("GTMaintCapExp").Value2);
                com.GTMaintExp = Convert.ToInt32(xlWorkSheet.get_Range("GTMaintExp").Value2);
                com.GTMaint = Convert.ToInt32(xlWorkSheet.get_Range("GTMaint").Value2);



            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetMaintenance";
            }

            return com;

        }

        private void WriteMaintenance(Refining_Maintenance contact)
        {

            cmd.CommandText = "Refining_InsertMaintenance";
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            cmd.Parameters.Add(new SqlParameter("@TotalCapitalExp", (contact.TotalCapitalExp == null) ? 0 : contact.TotalCapitalExp));
            cmd.Parameters.Add(new SqlParameter("@DiscExp", (contact.DiscExp == null) ? 0 : contact.DiscExp));
            cmd.Parameters.Add(new SqlParameter("@NonDiscExp", (contact.NonDiscExp == null) ? 0 : contact.NonDiscExp));
            cmd.Parameters.Add(new SqlParameter("@OverheadCapital", (contact.OverheadCapital == null) ? 0 : contact.OverheadCapital));
            cmd.Parameters.Add(new SqlParameter("@TotalExp", (contact.TotalExp == null) ? 0 : contact.TotalExp));
            cmd.Parameters.Add(new SqlParameter("@CoLaborCapitalExp", (contact.CoLaborCapitalExp == null) ? 0 : contact.CoLaborCapitalExp));
            cmd.Parameters.Add(new SqlParameter("@CoLaborMaintExp", (contact.CoLaborMaintExp == null) ? 0 : contact.CoLaborMaintExp));
            cmd.Parameters.Add(new SqlParameter("@TotalCompLaborMaint", (contact.TotalCompLaborMaint == null) ? 0 : contact.TotalCompLaborMaint));
            cmd.Parameters.Add(new SqlParameter("@CoMaterialsExp", (contact.CoMaterialsExp == null) ? 0 : contact.CoMaterialsExp));
            cmd.Parameters.Add(new SqlParameter("@CoMaterialsMaintExp", (contact.CoMaterialsMaintExp == null) ? 0 : contact.CoMaterialsMaintExp));
            cmd.Parameters.Add(new SqlParameter("@TotalCoMaterials", (contact.TotalCoMaterials == null) ? 0 : contact.TotalCoMaterials));
            cmd.Parameters.Add(new SqlParameter("@TotalMaintCapExp", (contact.TotalMaintCapExp == null) ? 0 : contact.TotalMaintCapExp));
            cmd.Parameters.Add(new SqlParameter("@TotalMaintExp", (contact.TotalMaintExp == null) ? 0 : contact.TotalMaintExp));
            cmd.Parameters.Add(new SqlParameter("@TotalMaint", (contact.TotalMaint == null) ? 0 : contact.TotalMaint));
            cmd.Parameters.Add(new SqlParameter("@IntMaintCapExp", (contact.IntMaintCapExp == null) ? 0 : contact.IntMaintCapExp));
            cmd.Parameters.Add(new SqlParameter("@IntMaintExp", (contact.IntMaintExp == null) ? 0 : contact.IntMaintExp));
            cmd.Parameters.Add(new SqlParameter("@TotalInt", (contact.TotalInt == null) ? 0 : contact.TotalInt));
            cmd.Parameters.Add(new SqlParameter("@GTMaintCapExp", (contact.GTMaintCapExp == null) ? 0 : contact.GTMaintCapExp));
            cmd.Parameters.Add(new SqlParameter("@GTMaintExp", (contact.GTMaintExp == null) ? 0 : contact.GTMaintExp));
            cmd.Parameters.Add(new SqlParameter("@GTMaint", (contact.GTMaint == null) ? 0 : contact.GTMaint));


            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteMaintenance : " + ex.Message;
                return;
            }



        }

        private Refining_Operations GetOperations()
        {
            Refining_Operations comp = new Refining_Operations();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Operations-Reliability-HC Loss"];
            try
            {

                comp.JetFuelVolMo = Convert.ToInt32(xlWorkSheet.get_Range("JetFuelVolMo").Value2);
                comp.JetFuelRecord = Convert.ToInt32(xlWorkSheet.get_Range("JetFuelRecord").Value2);
                comp.OpJetFuelNote = xlWorkSheet.get_Range("OpJetFuelNote").Value2;
                comp.AVGASVolMo = Convert.ToInt32(xlWorkSheet.get_Range("AVGASVolMo").Value2);
                comp.AVGASRecord = Convert.ToInt32(xlWorkSheet.get_Range("AVGASRecord").Value2);
                comp.OpAVGASNote = xlWorkSheet.get_Range("OpAVGASNote").Value2;
                comp.OtherVolMo = Convert.ToInt32(xlWorkSheet.get_Range("OtherVolMo").Value2);
                comp.OtherRecord = Convert.ToInt32(xlWorkSheet.get_Range("OtherRecord").Value2);
                comp.OpOtherNote = xlWorkSheet.get_Range("OpOtherNote").Value2;
                comp.TotalFuelVolMo = Convert.ToInt32(xlWorkSheet.get_Range("TotalFuelVolMo").Value2);
                comp.TotalRecord = Convert.ToInt32(xlWorkSheet.get_Range("TotalRecord").Value2);
                comp.UnderwingCount = Convert.ToInt32(xlWorkSheet.get_Range("UnderwingCount").Value2);
                comp.UnderwingRecord = Convert.ToInt32(xlWorkSheet.get_Range("UnderwingRecord").Value2);
                comp.UnderwingNote = xlWorkSheet.get_Range("UnderwingNote").Value2;
                comp.OverwingCount = Convert.ToInt32(xlWorkSheet.get_Range("OverwingCount").Value2);
                comp.OverwingRecord = Convert.ToInt32(xlWorkSheet.get_Range("OverwingRecord").Value2);
                comp.OverwingNote = xlWorkSheet.get_Range("OverwingNote").Value2;
                comp.RotaryWingCount = Convert.ToInt32(xlWorkSheet.get_Range("RotaryWingCount").Value2);
                comp.RotaryRecord = Convert.ToInt32(xlWorkSheet.get_Range("RotaryRecord").Value2);
                comp.RotaryWingNote = xlWorkSheet.get_Range("RotaryWingNote").Value2;
                comp.OtherCount = Convert.ToInt32(xlWorkSheet.get_Range("OtherCount").Value2);
                comp.OtherAircraftRecord = Convert.ToInt32(xlWorkSheet.get_Range("OtherAircraftRecord").Value2);
                comp.OtherAircraftNote = xlWorkSheet.get_Range("OtherAircraftNote").Value2;
                comp.TotalAircraft = Convert.ToInt32(xlWorkSheet.get_Range("TotalAircraft").Value2);
                comp.TotalAircraftRecord = Convert.ToInt32(xlWorkSheet.get_Range("TotalAircraftRecord").Value2);
                comp.CoOpCausesCount = Convert.ToInt32(xlWorkSheet.get_Range("CoOpCausesCount").Value2);
                comp.CoOpHours = Convert.ToInt32(xlWorkSheet.get_Range("CoOpHours").Value2);
                comp.CoOpNote = xlWorkSheet.get_Range("CoOpNote").Value2;
                comp.CoMechCausesCount = Convert.ToInt32(xlWorkSheet.get_Range("CoMechCausesCount").Value2);
                comp.CoMechHours = Convert.ToInt32(xlWorkSheet.get_Range("CoMechHours").Value2);
                comp.CoMechNote = xlWorkSheet.get_Range("CoMechNote").Value2;
                comp.NonCoCausesCount = Convert.ToInt32(xlWorkSheet.get_Range("NonCoCausesCount").Value2);
                comp.NonCoHours = Convert.ToInt32(xlWorkSheet.get_Range("NonCoHours").Value2);
                comp.NonCoNote = xlWorkSheet.get_Range("NonCoNote").Value2;
                comp.TotalEvents = Convert.ToInt32(xlWorkSheet.get_Range("TotalEvents").Value2);
                comp.TotalEventHours = Convert.ToInt32(xlWorkSheet.get_Range("TotalEventHours").Value2);
                comp.SpillsCount = Convert.ToInt32(xlWorkSheet.get_Range("SpillsCount").Value2);
                comp.SpillsVol = Convert.ToInt32(xlWorkSheet.get_Range("SpillsVol").Value2);
                comp.SpillsNote = xlWorkSheet.get_Range("SpillsNote").Value2;
                comp.UnknownCount = Convert.ToInt32(xlWorkSheet.get_Range("UnknownCount").Value2);
                comp.UnknownVol = Convert.ToInt32(xlWorkSheet.get_Range("UnknownVol").Value2);
                comp.UnknownNote = xlWorkSheet.get_Range("UnknownNote").Value2;
                comp.IsMassBalance = (xlWorkSheet.get_Range("IsMassBalance").Value2.ToUpper() == "YES" ? 1 : 0);
                comp.IsMassBalanceNote = xlWorkSheet.get_Range("IsMassBalanceNote").Value2;
                comp.FreqMassBalance = xlWorkSheet.get_Range("FreqMassBalance").Value2;
                comp.FreqMassBalanceNote = xlWorkSheet.get_Range("FreqMassBalanceNote").Value2;
                comp.IsVolOrMass = xlWorkSheet.get_Range("IsVolOrMass").Value2;
                comp.IsVolOrMassNote = xlWorkSheet.get_Range("IsVolOrMassNote").Value2;
                comp.IsDryOrWet = xlWorkSheet.get_Range("IsDryOrWet").Value2;
                comp.IsDryOrWetNote = xlWorkSheet.get_Range("IsDryOrWetNote").Value2;
                comp.TotalReceipts = Convert.ToInt32(xlWorkSheet.get_Range("TotalReceipts").Value2);
                comp.TotalReceiptsNote = xlWorkSheet.get_Range("TotalReceiptsNote").Value2;
                comp.TotalDisposals = Convert.ToInt32(xlWorkSheet.get_Range("TotalDisposals").Value2);
                comp.TotalDisposalsNote = xlWorkSheet.get_Range("TotalDisposalsNote").Value2;
                comp.TotalICTanks = Convert.ToInt32(xlWorkSheet.get_Range("TotalICTanks").Value2);
                comp.TotalICTanksNote = xlWorkSheet.get_Range("TotalICTanksNote").Value2;
                comp.TotalICFuelers = Convert.ToInt32(xlWorkSheet.get_Range("TotalICFuelers").Value2);
                comp.TotalICFuelersNote = xlWorkSheet.get_Range("TotalICFuelersNote").Value2;
                comp.TotalDeliveries = Convert.ToInt32(xlWorkSheet.get_Range("TotalDeliveries").Value2);
                comp.TotalDeliveriesNote = xlWorkSheet.get_Range("TotalDeliveriesNote").Value2;
                comp.Loss = Convert.ToInt32(xlWorkSheet.get_Range("Loss").Value2);
                comp.LossPercent = Convert.ToInt32(xlWorkSheet.get_Range("LossPercent").Value2);
                comp.LossPercentNote = xlWorkSheet.get_Range("LossPercentNote").Value2;



            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetOperations : " + ex.Message;
            }

            return comp;

        }



        private void WriteOperations(Refining_Operations contact)
        {

            cmd.CommandText = "Refining_InsertOperations";
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
            cmd.Parameters.Add(new SqlParameter("@JetFuelVolMo", (contact.JetFuelVolMo == null) ? 0 : contact.JetFuelVolMo));
            cmd.Parameters.Add(new SqlParameter("@JetFuelRecord", (contact.JetFuelRecord == null) ? 0 : contact.JetFuelRecord));
            cmd.Parameters.Add(new SqlParameter("@OpJetFuelNote", (contact.OpJetFuelNote == null) ? "" : contact.OpJetFuelNote));
            cmd.Parameters.Add(new SqlParameter("@AVGASVolMo", (contact.AVGASVolMo == null) ? 0 : contact.AVGASVolMo));
            cmd.Parameters.Add(new SqlParameter("@AVGASRecord", (contact.AVGASRecord == null) ? 0 : contact.AVGASRecord));
            cmd.Parameters.Add(new SqlParameter("@OpAVGASNote", (contact.OpAVGASNote == null) ? "" : contact.OpAVGASNote));
            cmd.Parameters.Add(new SqlParameter("@OtherVolMo", (contact.OtherVolMo == null) ? 0 : contact.OtherVolMo));
            cmd.Parameters.Add(new SqlParameter("@OtherRecord", (contact.OtherRecord == null) ? 0 : contact.OtherRecord));
            cmd.Parameters.Add(new SqlParameter("@TotalFuelVolMo", (contact.TotalFuelVolMo == null) ? 0 : contact.TotalFuelVolMo));
            cmd.Parameters.Add(new SqlParameter("@OpOtherNote", (contact.OpOtherNote == null) ? "" : contact.OpOtherNote));
            cmd.Parameters.Add(new SqlParameter("@TotalRecord", (contact.TotalRecord == null) ? 0 : contact.TotalRecord));
            cmd.Parameters.Add(new SqlParameter("@UnderwingCount", (contact.UnderwingCount == null) ? 0 : contact.UnderwingCount));
            cmd.Parameters.Add(new SqlParameter("@UnderwingRecord", (contact.UnderwingRecord == null) ? 0 : contact.UnderwingRecord));
            cmd.Parameters.Add(new SqlParameter("@UnderwingNote", (contact.UnderwingNote == null) ? "" : contact.UnderwingNote));
            cmd.Parameters.Add(new SqlParameter("@OverwingCount", (contact.OverwingCount == null) ? 0 : contact.OverwingCount));
            cmd.Parameters.Add(new SqlParameter("@OverwingRecord", (contact.OverwingRecord == null) ? 0 : contact.OverwingRecord));
            cmd.Parameters.Add(new SqlParameter("@OverwingNote", (contact.OverwingNote == null) ? "" : contact.OverwingNote));
            cmd.Parameters.Add(new SqlParameter("@RotaryWingCount", (contact.RotaryWingCount == null) ? 0 : contact.RotaryWingCount));
            cmd.Parameters.Add(new SqlParameter("@RotaryRecord", (contact.RotaryRecord == null) ? 0 : contact.RotaryRecord));
            cmd.Parameters.Add(new SqlParameter("@RotaryWingNote", (contact.RotaryWingNote == null) ? "" : contact.RotaryWingNote));
            cmd.Parameters.Add(new SqlParameter("@OtherCount", (contact.OtherCount == null) ? 0 : contact.OtherCount));

            cmd.Parameters.Add(new SqlParameter("@OtherAircraftRecord", (contact.OtherAircraftRecord == null) ? 0 : contact.OtherAircraftRecord));
            cmd.Parameters.Add(new SqlParameter("@OtherAircraftNote", (contact.OtherAircraftNote == null) ? "" : contact.OtherAircraftNote));
            cmd.Parameters.Add(new SqlParameter("@TotalAircraft", (contact.TotalAircraft == null) ? 0 : contact.TotalAircraft));
            cmd.Parameters.Add(new SqlParameter("@TotalAircraftRecord", (contact.TotalAircraftRecord == null) ? 0 : contact.TotalAircraftRecord));
            cmd.Parameters.Add(new SqlParameter("@CoOpCausesCount", (contact.CoOpCausesCount == null) ? 0 : contact.CoOpCausesCount));
            cmd.Parameters.Add(new SqlParameter("@CoOpHours", (contact.CoOpHours == null) ? 0 : contact.CoOpHours));
            cmd.Parameters.Add(new SqlParameter("@CoOpNote", (contact.CoOpNote == null) ? "" : contact.CoOpNote));
            cmd.Parameters.Add(new SqlParameter("@CoMechCausesCount", (contact.CoMechCausesCount == null) ? 0 : contact.CoMechCausesCount));
            cmd.Parameters.Add(new SqlParameter("@CoMechHours", (contact.CoMechHours == null) ? 0 : contact.CoMechHours));
            cmd.Parameters.Add(new SqlParameter("@CoMechNote", (contact.CoMechNote == null) ? "" : contact.CoMechNote));
            cmd.Parameters.Add(new SqlParameter("@NonCoCausesCount", (contact.NonCoCausesCount == null) ? 0 : contact.NonCoCausesCount));
            cmd.Parameters.Add(new SqlParameter("@NonCoHours", (contact.NonCoHours == null) ? 0 : contact.NonCoHours));
            cmd.Parameters.Add(new SqlParameter("@NonCoNote", (contact.NonCoNote == null) ? "" : contact.NonCoNote));
            cmd.Parameters.Add(new SqlParameter("@TotalEvents", (contact.TotalEvents == null) ? 0 : contact.TotalEvents));
            cmd.Parameters.Add(new SqlParameter("@TotalEventHours", (contact.TotalEventHours == null) ? 0 : contact.TotalEventHours));
            cmd.Parameters.Add(new SqlParameter("@SpillsCount", (contact.SpillsCount == null) ? 0 : contact.SpillsCount));
            cmd.Parameters.Add(new SqlParameter("@SpillsVol", (contact.SpillsVol == null) ? 0 : contact.SpillsVol));
            cmd.Parameters.Add(new SqlParameter("@SpillsNote", (contact.SpillsNote == null) ? "" : contact.SpillsNote));
            cmd.Parameters.Add(new SqlParameter("@UnknownCount", (contact.UnknownCount == null) ? 0 : contact.UnknownCount));
            cmd.Parameters.Add(new SqlParameter("@UnknownVol", (contact.UnknownVol == null) ? 0 : contact.UnknownVol));
            cmd.Parameters.Add(new SqlParameter("@UnknownNote", (contact.UnknownNote == null) ? "" : contact.UnknownNote));

            cmd.Parameters.Add(new SqlParameter("@IsMassBalance", (contact.IsMassBalance == null) ? 0 : contact.IsMassBalance));
            cmd.Parameters.Add(new SqlParameter("@IsMassBalanceNote", (contact.IsMassBalanceNote == null) ? "" : contact.IsMassBalanceNote));
            cmd.Parameters.Add(new SqlParameter("@FreqMassBalance", (contact.FreqMassBalance == null) ? "" : contact.FreqMassBalance));
            cmd.Parameters.Add(new SqlParameter("@FreqMassBalanceNote", (contact.FreqMassBalanceNote == null) ? "" : contact.FreqMassBalanceNote));
            cmd.Parameters.Add(new SqlParameter("@IsVolOrMass", (contact.IsVolOrMass == null) ? "" : contact.IsVolOrMass));
            cmd.Parameters.Add(new SqlParameter("@IsVolOrMassNote", (contact.IsVolOrMassNote == null) ? "" : contact.IsVolOrMassNote));
            cmd.Parameters.Add(new SqlParameter("@IsDryOrWet", (contact.IsDryOrWet == null) ? "" : contact.IsDryOrWet));
            cmd.Parameters.Add(new SqlParameter("@IsDryOrWetNote", (contact.IsDryOrWetNote == null) ? "" : contact.IsDryOrWetNote));

            cmd.Parameters.Add(new SqlParameter("@TotalReceipts", (contact.TotalReceipts == null) ? 0 : contact.TotalReceipts));
            cmd.Parameters.Add(new SqlParameter("@TotalReceiptsNote", (contact.TotalReceiptsNote == null) ? "" : contact.TotalReceiptsNote));
            cmd.Parameters.Add(new SqlParameter("@TotalDisposals", (contact.TotalDisposals == null) ? 0 : contact.TotalDisposals));
            cmd.Parameters.Add(new SqlParameter("@TotalDisposalsNote", (contact.TotalDisposalsNote == null) ? "" : contact.TotalDisposalsNote));
            cmd.Parameters.Add(new SqlParameter("@TotalICTanks", (contact.TotalICTanks == null) ? 0 : contact.TotalICTanks));
            cmd.Parameters.Add(new SqlParameter("@TotalICTanksNote", (contact.TotalICTanksNote == null) ? "" : contact.TotalICTanksNote));
            cmd.Parameters.Add(new SqlParameter("@TotalICFuelers", (contact.TotalICFuelers == null) ? 0 : contact.TotalICFuelers));
            cmd.Parameters.Add(new SqlParameter("@TotalICFuelersNote", (contact.TotalICFuelersNote == null) ? "" : contact.TotalICFuelersNote));
            cmd.Parameters.Add(new SqlParameter("@TotalDeliveries", (contact.TotalDeliveries == null) ? 0 : contact.TotalDeliveries));
            cmd.Parameters.Add(new SqlParameter("@TotalDeliveriesNote", (contact.TotalDeliveriesNote == null) ? "" : contact.TotalDeliveriesNote));
            cmd.Parameters.Add(new SqlParameter("@Loss", (contact.Loss == null) ? 0 : contact.Loss));
            cmd.Parameters.Add(new SqlParameter("@LossNote", (contact.LossNote == null) ? "" : contact.LossNote));
            cmd.Parameters.Add(new SqlParameter("@LossPercent", (contact.LossPercent == null) ? 0 : contact.LossPercent));
            cmd.Parameters.Add(new SqlParameter("@LossPercentNote", (contact.LossPercentNote == null) ? "" : contact.LossPercentNote));



            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": InsertOperations : " + ex.Message;
                return;
            }



        }

        private Refining_Energy GetEnergy()
        {
            Refining_Energy comp = new Refining_Energy();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Energy"];
            try
            {


                comp.ElectMW = Convert.ToInt32(xlWorkSheet.get_Range("ElectMW").Value2);
                comp.ThermalGJ = Convert.ToInt32(xlWorkSheet.get_Range("ThermalGJ").Value2);
                comp.DieselUSG = Convert.ToInt32(xlWorkSheet.get_Range("DieselUSG").Value2);
                comp.GasUSG = Convert.ToInt32(xlWorkSheet.get_Range("GasUSG").Value2);
                comp.CNG = Convert.ToInt32(xlWorkSheet.get_Range("CNG").Value2);
                comp.LPG = Convert.ToInt32(xlWorkSheet.get_Range("LPG").Value2);
                comp.Propane = Convert.ToInt32(xlWorkSheet.get_Range("Propane").Value2);
                comp.Ethanol = Convert.ToInt32(xlWorkSheet.get_Range("Ethanol").Value2);
                comp.ElectricDriven = Convert.ToInt32(xlWorkSheet.get_Range("ElectricDriven").Value2);
                comp.OtherFuels = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuels").Value2);
                comp.TotalAFV = Convert.ToInt32(xlWorkSheet.get_Range("TotalAFV").Value2);
                comp.ElectUnitCost = Convert.ToInt32(xlWorkSheet.get_Range("ElectUnitCost").Value2);
                comp.ThermalEnergyUnitCost = Convert.ToInt32(xlWorkSheet.get_Range("ThermalEnergyUnitcost").Value2);
                comp.DieselUnitCost = Convert.ToInt32(xlWorkSheet.get_Range("DieselUnitCost").Value2);
                comp.GasUnitCost = Convert.ToInt32(xlWorkSheet.get_Range("GasUnitCost").Value2);
                comp.CNGUnitCost = Convert.ToInt32(xlWorkSheet.get_Range("CNGUnitCost").Value2);
                comp.LPGUnitCost = Convert.ToInt32(xlWorkSheet.get_Range("LPGUnitCost").Value2);
                comp.EthanolUnitCost = Convert.ToInt32(xlWorkSheet.get_Range("EthanolUnitCost").Value2);
                comp.OtherFuelsUnitCost = Convert.ToInt32(xlWorkSheet.get_Range("OtherFuelsUnitCost").Value2);

                comp.ElectMWNote = xlWorkSheet.get_Range("ElectMWNote").Value2;
                comp.ThermalGJNote = xlWorkSheet.get_Range("ThermalGJNote").Value2;
                comp.DieselUSGNote = xlWorkSheet.get_Range("DieselNote").Value2;
                comp.GasUSGNote = xlWorkSheet.get_Range("GasNote").Value2;
                comp.CNGNote = xlWorkSheet.get_Range("CNGNote").Value2;
                comp.LPGNote = xlWorkSheet.get_Range("LPGNote").Value2;
                comp.PropaneNote = xlWorkSheet.get_Range("PropaneNote").Value2;
                comp.EthanolNote = xlWorkSheet.get_Range("EthanolNote").Value2;
                comp.ElectricDrivenNote = xlWorkSheet.get_Range("ElectricDrivenNote").Value2;
                comp.OtherFuelsNote = xlWorkSheet.get_Range("OtherFuelsNote").Value2;
                comp.TotalAFVNote = xlWorkSheet.get_Range("TotalAFVNote").Value2;
                comp.ElectUnitCostNote = xlWorkSheet.get_Range("ElectUnitCostNote").Value2;
                comp.ThermalEnergyUnitCostNote = xlWorkSheet.get_Range("ThermalEnergyUnitCostNote").Value2;
                comp.DieselUnitCostNote = xlWorkSheet.get_Range("DieselUnitCostNote").Value2;
                comp.GasUnitCostNote = xlWorkSheet.get_Range("GasUnitCostNote").Value2;
                comp.CNGUnitCostNote = xlWorkSheet.get_Range("CNGUnitCostNote").Value2;
                comp.LPGUnitCostNote = xlWorkSheet.get_Range("LPGUnitCostNote").Value2;
                comp.EthanolUnitCostNote = xlWorkSheet.get_Range("EthanolUnitCostNote").Value2;
                comp.OtherFuelsUnitCostNote = xlWorkSheet.get_Range("OtherFuelsUnitCostNote").Value2;



            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetRefining_Energy";
            }

            return comp;

        }

        private void WriteEnergy(Refining_Energy energy)
        {

            cmd.CommandText = "Refining_InsertEnergy";
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@SubmissionID", SubmissionID);
            cmd.Parameters.AddWithValue("@AirportID", energy.AirportID);
            cmd.Parameters.AddWithValue("@CompanyID", CompanyID);
            cmd.Parameters.AddWithValue("@ElectMW", (energy.ElectMW == null) ? 0 : energy.ElectMW);
            cmd.Parameters.AddWithValue("@ThermalGJ", (energy.ThermalGJ == null) ? 0 : energy.ThermalGJ);
            cmd.Parameters.AddWithValue("@DieselUSG", (energy.DieselUSG == null) ? 0 : energy.DieselUSG);
            cmd.Parameters.AddWithValue("@GasUSG", (energy.GasUSG == null) ? 0 : energy.GasUSG);
            cmd.Parameters.AddWithValue("@CNG", (energy.CNG == null) ? 0 : energy.CNG);
            cmd.Parameters.AddWithValue("@LPG", (energy.LPG == null) ? 0 : energy.LPG);
            cmd.Parameters.AddWithValue("@Propane", (energy.Propane == null) ? 0 : energy.Propane);
            cmd.Parameters.AddWithValue("@Ethanol", (energy.Ethanol == null) ? 0 : energy.Ethanol);
            cmd.Parameters.AddWithValue("@ElectricDriven", (energy.ElectricDriven == null) ? 0 : energy.ElectricDriven);
            cmd.Parameters.AddWithValue("@OtherFuels", (energy.OtherFuels == null) ? 0 : energy.OtherFuels);
            cmd.Parameters.AddWithValue("@TotalAFV", (energy.TotalAFV == null) ? 0 : energy.TotalAFV);
            cmd.Parameters.AddWithValue("@ElectUnitCost", (energy.ElectUnitCost == null) ? 0 : energy.ElectUnitCost);
            cmd.Parameters.AddWithValue("@ThermalEnergyUnitcost", (energy.ThermalEnergyUnitCost == null) ? 0 : energy.ThermalEnergyUnitCost);
            cmd.Parameters.AddWithValue("@DieselUnitCost", (energy.DieselUnitCost == null) ? 0 : energy.DieselUnitCost);
            cmd.Parameters.AddWithValue("@GasUnitCost", (energy.GasUnitCost == null) ? 0 : energy.GasUnitCost);
            cmd.Parameters.AddWithValue("@CNGUnitCost", (energy.CNGUnitCost == null) ? 0 : energy.CNGUnitCost);
            cmd.Parameters.AddWithValue("@LPGUnitCost", (energy.LPGUnitCost == null) ? 0 : energy.LPGUnitCost);
            cmd.Parameters.AddWithValue("@EthanolUnitCost", (energy.EthanolUnitCost == null) ? 0 : energy.EthanolUnitCost);
            cmd.Parameters.AddWithValue("@OtherFuelsUnitCost", (energy.OtherFuelsUnitCost == null) ? 0 : energy.OtherFuelsUnitCost);
            cmd.Parameters.AddWithValue("@ElectMWNote", (energy.ElectMWNote == null) ? "" : energy.ElectMWNote);
            cmd.Parameters.AddWithValue("@ThermalGJNote", (energy.ThermalGJNote == null) ? "" : energy.ThermalGJNote);
            cmd.Parameters.AddWithValue("@DieselUSGNote", (energy.DieselUSGNote == null) ? "" : energy.DieselUSGNote);
            cmd.Parameters.AddWithValue("@GasUSGNote", (energy.GasUSGNote == null) ? "" : energy.GasUSGNote);
            cmd.Parameters.AddWithValue("@CNGNote", (energy.CNGNote == null) ? "" : energy.CNGNote);
            cmd.Parameters.AddWithValue("@LPGNote", (energy.LPGNote == null) ? "" : energy.LPGNote);
            cmd.Parameters.AddWithValue("@PropaneNote", (energy.PropaneNote == null) ? "" : energy.PropaneNote);
            cmd.Parameters.AddWithValue("@EthanolNote", (energy.EthanolNote == null) ? "" : energy.EthanolNote);
            cmd.Parameters.AddWithValue("@ElectricDrivenNote", (energy.ElectricDrivenNote == null) ? "" : energy.ElectricDrivenNote);
            cmd.Parameters.AddWithValue("@OtherFuelsNote", (energy.OtherFuelsNote == null) ? "" : energy.OtherFuelsNote);
            cmd.Parameters.AddWithValue("@TotalAFVNote", (energy.TotalAFVNote == null) ? "" : energy.TotalAFVNote);
            cmd.Parameters.AddWithValue("@ElectUnitCostNote", (energy.ElectUnitCostNote == null) ? "" : energy.ElectUnitCostNote);
            cmd.Parameters.AddWithValue("@ThermalEnergyUnitCostNote", (energy.ThermalEnergyUnitCostNote == null) ? "" : energy.ThermalEnergyUnitCostNote);
            cmd.Parameters.AddWithValue("@DieselUnitCostNote", (energy.DieselUnitCostNote == null) ? "" : energy.DieselUnitCostNote);
            cmd.Parameters.AddWithValue("@GasUnitCostNote", (energy.GasUnitCostNote == null) ? "" : energy.GasUnitCostNote);
            cmd.Parameters.AddWithValue("@CNGUnitCostNote", (energy.CNGUnitCostNote == null) ? "" : energy.CNGUnitCostNote);
            cmd.Parameters.AddWithValue("@LPGUnitCostNote", (energy.LPGUnitCostNote == null) ? "" : energy.LPGUnitCostNote);
            cmd.Parameters.AddWithValue("@EthanolUnitCostNote", (energy.EthanolUnitCostNote == null) ? "" : energy.EthanolUnitCostNote);
            cmd.Parameters.AddWithValue("@OtherFuelsUnitCostNote", (energy.OtherFuelsUnitCostNote == null) ? "" : energy.OtherFuelsUnitCostNote);



            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Error = "SubmissionID:" + SubmissionID + ": WriteEnergy : " + ex.Message;
                return;
            }



        }
        private Refining_Personnel GetPersonnel()
        {
            Refining_Personnel comp = new Refining_Personnel();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Personnel"];
            try
            {

                comp.OpActEmpCount = Convert.ToInt32(xlWorkSheet.get_Range("OpActEmpCount").Value2);
                comp.OpActWorkHours = Convert.ToInt32(xlWorkSheet.get_Range("OpActWorkHours").Value2);
                comp.OpActSWB = Convert.ToInt32(xlWorkSheet.get_Range("OpActSWB").Value2);
                comp.OpActTotalExp = Convert.ToInt32(xlWorkSheet.get_Range("OpActTotalExp").Value2);
                comp.OpActContLabor = Convert.ToInt32(xlWorkSheet.get_Range("OpActContLabor").Value2);
                comp.OpActAvgContRate = Convert.ToInt32(xlWorkSheet.get_Range("OpActAvgContRate").Value2);
                comp.TotalContHours = Convert.ToInt32(xlWorkSheet.get_Range("TotalContHours").Value2);
                comp.GTOpPersCost = Convert.ToInt32(xlWorkSheet.get_Range("GTOpPersCost").Value2);
                comp.GTOpWorkHours = Convert.ToInt32(xlWorkSheet.get_Range("GTOpWorkHours").Value2);
                comp.OpActNote = xlWorkSheet.get_Range("OpActNote").Value2;

                comp.MaintActEmpCount = Convert.ToInt32(xlWorkSheet.get_Range("MaintActEmpCount").Value2);
                comp.MaintActWorkHours = Convert.ToInt32(xlWorkSheet.get_Range("MaintActWorkHours").Value2);
                comp.MaintActSWB = Convert.ToInt32(xlWorkSheet.get_Range("MaintActSWB").Value2);
                comp.MaintActTotalExp = Convert.ToInt32(xlWorkSheet.get_Range("MaintActTotalExp").Value2);
                comp.MaintActContLabor = Convert.ToInt32(xlWorkSheet.get_Range("MaintActContLabor").Value2);
                comp.MaintActAvgContRate = Convert.ToInt32(xlWorkSheet.get_Range("MaintActAvgContRate").Value2);
                comp.TotalMaintContHours = Convert.ToInt32(xlWorkSheet.get_Range("TotalMaintContHours").Value2);
                comp.GTMaintPersCost = Convert.ToInt32(xlWorkSheet.get_Range("GTMaintPersCost").Value2);
                comp.GTMaintWorkHours = Convert.ToInt32(xlWorkSheet.get_Range("GTMaintWorkHours").Value2);
                comp.MaintActNote = xlWorkSheet.get_Range("MaintActNote").Value2;

                comp.TechSuppEmpCount = Convert.ToInt32(xlWorkSheet.get_Range("TechSuppEmpCount").Value2);
                comp.TechSuppWorkHours = Convert.ToInt32(xlWorkSheet.get_Range("TechSuppWorkHours").Value2);
                comp.TechSuppSWB = Convert.ToInt32(xlWorkSheet.get_Range("TechSuppSWB").Value2);
                comp.TechSuppTotalExp = Convert.ToInt32(xlWorkSheet.get_Range("TechSuppTotalExp").Value2);
                comp.TechSuppContLabor = Convert.ToInt32(xlWorkSheet.get_Range("TechSuppContLabor").Value2);
                comp.TechSuppAvgContRate = Convert.ToInt32(xlWorkSheet.get_Range("TechSuppAvgContRate").Value2);
                comp.TotalTechSuppHours = Convert.ToInt32(xlWorkSheet.get_Range("TotalTechSuppHours").Value2);
                comp.GTTechSuppPersCost = Convert.ToInt32(xlWorkSheet.get_Range("GTTechSuppPersCost").Value2);
                comp.GTTechSuppWorkHours = Convert.ToInt32(xlWorkSheet.get_Range("GTTechSuppWorkHours").Value2);
                comp.TechSuppNote = xlWorkSheet.get_Range("TechSuppNote").Value2;

                comp.AdminEmpCount = Convert.ToInt32(xlWorkSheet.get_Range("AdminEmpCount").Value2);
                comp.AdminWorkHours = Convert.ToInt32(xlWorkSheet.get_Range("AdminWorkHours").Value2);
                comp.AdminSWB = Convert.ToInt32(xlWorkSheet.get_Range("AdminSWB").Value2);
                comp.AdminTotalExp = Convert.ToInt32(xlWorkSheet.get_Range("AdminTotalExp").Value2);
                comp.AdminContLabor = Convert.ToInt32(xlWorkSheet.get_Range("AdminContLabor").Value2);
                comp.AdminAvgContRate = Convert.ToInt32(xlWorkSheet.get_Range("AdminAvgContRate").Value2);
                comp.TotalAdminHours = Convert.ToInt32(xlWorkSheet.get_Range("TotalAdminHours").Value2);
                comp.GTAdminPersCost = Convert.ToInt32(xlWorkSheet.get_Range("GTAdminPersCost").Value2);
                comp.GTAdminWorkHours = Convert.ToInt32(xlWorkSheet.get_Range("GTAdminWorkHours").Value2);
                comp.AdminNote = xlWorkSheet.get_Range("AdminNote").Value2;



            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetPersonnel : " + ex.Message;
            }

            return comp;

        }

        private void WritePersonnel(Refining_Personnel contact)
        {

            cmd.CommandText = "Refining_InsertPersonnel";
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
            cmd.Parameters.Add(new SqlParameter("@AirportID", AirportID));
            cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));

            cmd.Parameters.Add(new SqlParameter("@OpActEmpCount", (contact.OpActEmpCount == null) ? 0 : contact.OpActEmpCount));
            cmd.Parameters.Add(new SqlParameter("@OpActWorkHours", (contact.OpActWorkHours == null) ? 0 : contact.OpActWorkHours));
            cmd.Parameters.Add(new SqlParameter("@OpActSWB", (contact.OpActSWB == null) ? 0 : contact.OpActSWB));
            cmd.Parameters.Add(new SqlParameter("@OpActTotalExp", (contact.OpActTotalExp == null) ? 0 : contact.OpActTotalExp));
            cmd.Parameters.Add(new SqlParameter("@OpActContLabor", (contact.OpActContLabor == null) ? 0 : contact.OpActContLabor));
            cmd.Parameters.Add(new SqlParameter("@OpActAvgContRate", (contact.OpActAvgContRate == null) ? 0 : contact.OpActAvgContRate));
            cmd.Parameters.Add(new SqlParameter("@TotalContHours", (contact.TotalContHours == null) ? 0 : contact.TotalContHours));
            cmd.Parameters.Add(new SqlParameter("@GTOpPersCost", (contact.GTOpPersCost == null) ? 0 : contact.GTOpPersCost));
            cmd.Parameters.Add(new SqlParameter("@GTOpWorkHours", (contact.GTOpWorkHours == null) ? 0 : contact.GTOpWorkHours));
            cmd.Parameters.Add(new SqlParameter("@OpActNote", (contact.OpActNote == null) ? "" : contact.OpActNote));

            cmd.Parameters.Add(new SqlParameter("@MaintActEmpCount", (contact.MaintActEmpCount == null) ? 0 : contact.MaintActEmpCount));
            cmd.Parameters.Add(new SqlParameter("@MaintActWorkHours", (contact.MaintActWorkHours == null) ? 0 : contact.MaintActWorkHours));
            cmd.Parameters.Add(new SqlParameter("@MaintActSWB", (contact.MaintActSWB == null) ? 0 : contact.MaintActSWB));
            cmd.Parameters.Add(new SqlParameter("@MaintActTotalExp", (contact.MaintActTotalExp == null) ? 0 : contact.MaintActTotalExp));
            cmd.Parameters.Add(new SqlParameter("@MaintActContLabor", (contact.MaintActContLabor == null) ? 0 : contact.MaintActContLabor));
            cmd.Parameters.Add(new SqlParameter("@MaintActAvgContRate", (contact.MaintActAvgContRate == null) ? 0 : contact.MaintActAvgContRate));
            cmd.Parameters.Add(new SqlParameter("@TotalMaintContHours", (contact.TotalMaintContHours == null) ? 0 : contact.TotalMaintContHours));
            cmd.Parameters.Add(new SqlParameter("@GTMaintPersCost", (contact.GTMaintPersCost == null) ? 0 : contact.GTMaintPersCost));
            cmd.Parameters.Add(new SqlParameter("@GTMaintWorkHours", (contact.GTMaintWorkHours == null) ? 0 : contact.GTMaintWorkHours));
            cmd.Parameters.Add(new SqlParameter("@MaintActNote", (contact.MaintActNote == null) ? "" : contact.MaintActNote));


            cmd.Parameters.Add(new SqlParameter("@TechSuppEmpCount", (contact.TechSuppEmpCount == null) ? 0 : contact.TechSuppEmpCount));
            cmd.Parameters.Add(new SqlParameter("@TechSuppWorkHours", (contact.TechSuppWorkHours == null) ? 0 : contact.TechSuppWorkHours));
            cmd.Parameters.Add(new SqlParameter("@TechSuppSWB", (contact.TechSuppSWB == null) ? 0 : contact.TechSuppSWB));
            cmd.Parameters.Add(new SqlParameter("@TechSuppTotalExp", (contact.TechSuppTotalExp == null) ? 0 : contact.TechSuppTotalExp));
            cmd.Parameters.Add(new SqlParameter("@TechSuppContLabor", (contact.TechSuppContLabor == null) ? 0 : contact.TechSuppContLabor));
            cmd.Parameters.Add(new SqlParameter("@TechSuppAvgContRate", (contact.TechSuppAvgContRate == null) ? 0 : contact.TechSuppAvgContRate));
            cmd.Parameters.Add(new SqlParameter("@TotalTechSuppHours", (contact.TotalContHours == null) ? 0 : contact.TotalTechSuppHours));
            cmd.Parameters.Add(new SqlParameter("@GTTechSuppPersCost", (contact.GTTechSuppPersCost == null) ? 0 : contact.GTTechSuppPersCost));
            cmd.Parameters.Add(new SqlParameter("@GTTechSuppWorkHours", (contact.GTTechSuppWorkHours == null) ? 0 : contact.GTTechSuppWorkHours));
            cmd.Parameters.Add(new SqlParameter("@TechSuppNote", (contact.TechSuppNote == null) ? "" : contact.TechSuppNote));

            cmd.Parameters.Add(new SqlParameter("@AdminEmpCount", (contact.AdminEmpCount == null) ? 0 : contact.AdminEmpCount));
            cmd.Parameters.Add(new SqlParameter("@AdminWorkHours", (contact.AdminWorkHours == null) ? 0 : contact.AdminWorkHours));
            cmd.Parameters.Add(new SqlParameter("@AdminSWB", (contact.AdminSWB == null) ? 0 : contact.AdminSWB));
            cmd.Parameters.Add(new SqlParameter("@AdminTotalExp", (contact.AdminTotalExp == null) ? 0 : contact.AdminTotalExp));
            cmd.Parameters.Add(new SqlParameter("@AdminContLabor", (contact.AdminContLabor == null) ? 0 : contact.AdminContLabor));
            cmd.Parameters.Add(new SqlParameter("@AdminAvgContRate", (contact.AdminAvgContRate == null) ? 0 : contact.AdminAvgContRate));
            cmd.Parameters.Add(new SqlParameter("@TotalAdminHours", (contact.TotalAdminHours == null) ? 0 : contact.TotalAdminHours));
            cmd.Parameters.Add(new SqlParameter("@GTAdminPersCost", (contact.GTAdminPersCost == null) ? 0 : contact.GTAdminPersCost));
            cmd.Parameters.Add(new SqlParameter("@GTAdminWorkHours", (contact.GTAdminWorkHours == null) ? 0 : contact.GTAdminWorkHours));
            cmd.Parameters.Add(new SqlParameter("@AdminNote", (contact.AdminNote == null) ? "" : contact.AdminNote));
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": InsertPersonnel : " + ex.Message;
                return;
            }



        }
    }


}
