﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using RefiningLibrary;



namespace RefiningLibrary
{
    //TABLE 1
    public class Refining_CompanyInfo
    {

        public int CompanyID { get; set; }
        public string Company { get; set; }
        public string Location { get; set; }
        public string CoordName { get; set; }
        public string CoordTitle { get; set; }
        public string CoordPhone { get; set; }
        public string CoordEmail { get; set; }
    }


   
    public class Refining_Electricty
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? ElectricPur { get; set; }
        public int? ElectricProd { get; set; }
        public int? ElectricProdEff { get; set; }
        public int? ElectricTransIN { get; set; }
        public int? ElectricTransOUT { get; set; }
        public int? ElectricSOL { get; set; }
        public int? ElectricCost { get; set; }
        
    }

    public class Refining_HydroCarbonLoss
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? TotalHCLoss { get; set; }
        public int? TotalFlareLoss { get; set; }
        

    }

    public class Refining_NonTAMaintenance
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public int? TotalHrsDownReg { get; set; }
        public int? TotalHrsDownMech { get; set; }
        

    }

    public class Refining_Notes
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public string Note { get; set; }

    }


    public class Refining_ProcessData
    {
        public int SubmissionID { get; set; }
        public int AirportID { get; set; }
        public int CompanyID { get; set; }
        public int? FeedDensity {get; set;}
	    public int? FlashZoneTemp {get; set;}
	    public int? FlashZonePress {get; set;}
	    public int? AtmVacPercFeed {get; set;}
	    public int? ConCarbon {get; set;}
	    public int? CokeYield {get; set;}
	    public int? ConversionPerc {get; set;}
	    public int? ASTM10 {get; set;}
	    public int? ASTM90 {get; set;}
	    public int? Aniline {get; set;}
	    public int? CrackedHGO {get; set;}
	    public int? VGO {get; set;}
	    public int? DeasphaltedOil {get; set;}
	    public int? LubeExtracts {get; set;}
	    public int? LtHCNaphtha {get; set;}
	    public int? HvyHCNaphtha {get; set;}
	    public int? Diesel {get; set;}
	    public int? HGO {get; set;}
	    public int? BalProds {get; set;}
	    public int? ReactorPress {get; set;}
	    public int? H2Consumption {get; set;}
	    public int? Naphthenes {get; set;}
	    public int? Aromatics {get; set;}
	    public int? AvgRefQuality {get; set;}
	    public int? ReactorTempDiffs {get; set;}
	    public int? H2ContFeed {get; set;}
	    public int? AvgGasFeedRate {get; set;}
	    public int? CrackedStock {get; set;}

    }

    public class Refining_ProductYield
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public string Category { get; set; }
        public string MaterialID { get; set; }
        public string MaterialName { get; set; }
        public int? BBL {get; set;}
	    public int? Density {get; set;}
	    public int? Tonne {get; set;}
	   

    }


    public class Refining_RawMaterials
    {
       public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public string Category { get; set; }
        public string MaterialID { get; set; }
        public string MaterialName { get; set; }
        public int? BBL {get; set;}
	    public int? Density {get; set;}
	    public int? Tonne {get; set;}
	   

    }


    public class Refining_ReceiptsShipments
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? RSCRUDE_BargeBerthBBL {get; set;}
	    public int? RSCRUDE_OffshoreBuoyBBL {get; set;}
	    public int? RSCRUDE_RailcarBBL {get; set;}
	    public int? RSCRUDE_TankerBerthBBL {get; set;}
	    public int? RSPROD_BargeBerthBBL {get; set;}
	    public int? RSPROD_OffshoreBuoyBBL {get; set;}
	    public int? RSPROD_RailcarBBL {get; set;}
	    public int? RSPROD_TankerBerthBBL {get; set;}
	    public int? RSPROD_TankTruckBBL {get; set;}

    }


   public class Refining_TAMaintHistory
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? UnitID {get; set;}
	    public string TADate {get; set;}
	    public string PrevTADate {get; set;}
	    public int? TotalHrsDown {get; set;}
	    public int? TotalCost {get; set;}
	    public int? EmpHours {get; set;}
	    public int? ContHours {get; set;}
	    

    }

    public class Refining_ThermalEnergy
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? EnergyPur { get; set; }
        public int? EnergyProd { get; set; }
        public int? EnergyTransIN { get; set; }
        public int? EnergyTransOUT { get; set; }
        public int? EnergySOL { get; set; }
        public int? ThermalCost { get; set; }
        
    }


    
    public class Refining_Units
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int UnitID { get; set; }
        public string UnitName { get; set; }
        public string ProcessID { get; set; }
        public string ProcessType { get; set; }
        public string ProcessSubType { get; set; }
        public int? Capacity { get; set; }
        public int? CapacityUtil { get; set; }

    }

    public class Refining_WorkCosts
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? OCCSal{ get; set; }
        public int? RoutCostLocal { get; set; }
        public int? EmpSWB { get; set; }
        public int? MaintContLabor { get; set; }
        public int? MiscContServices { get; set; }
        public int? GACosts { get; set; }
        
    }

    public class Refining_WorkHours
    {
        public int SubmissionID { get; set; }
        public int CompanyID { get; set; }
        public int? STH{ get; set; }
        public int? Contract { get; set; }
        public int? GA { get; set; }
        
    }

    


    public class Refinery
    {
        //Open Excel Application
        Excel.Application excel;
        Excel.Workbook excelWorkbook;

        //Initialize Connection and Command
        SqlConnection cn = new SqlConnection();
        SqlCommand cmd = new SqlCommand();


        string CONNECTSTR = Properties.Settings.Default.CONN;
        public string Error = null;
        string CurrentLogFile = null;
        int SubmissionID = 0;
        int CompanyID = 0;
        int AirportID = 0;

        public Refinery(string cs)
        {
            CurrentLogFile = cs;
        }






        public string ImportRefining(string RefiningPath)
        {
            string rtn = "Refining Import Complete....";
            cn.ConnectionString = CONNECTSTR;
            cn.Open();

            //Open Workbook
            excel = new Excel.Application();

            excelWorkbook = excel.Workbooks.Open(RefiningPath,
                0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "",
                true, false, 0, true, false, false);



            //Initialize the Objects
            Refining_CompanyInfo Contact = new Refining_CompanyInfo();
            Refining_Electricty Electricity = new Refining_Electricty();
            List<Refining_ProductYield> ProductYield = new List<Refining_ProductYield>();
            Refining_HydroCarbonLoss HyrdoCarbonLoss = new Refining_HydroCarbonLoss();
            List<Refining_RawMaterials> RawMaterials = new List<Refining_RawMaterials>();
            List<Refining_Units> Units = new List<Refining_Units>();
            List<Refining_WorkCosts> WorkCosts = new List<Refining_WorkCosts>();
            List<Refining_WorkHours> WorkHours = new List<Refining_WorkHours>();
            List<Refining_ProcessData> ProcessData = new List<Refining_ProcessData>();
            Refining_NonTAMaintenance NonTAMaint = new Refining_NonTAMaintenance();
            Refining_TAMaintHistory TAMaintHisory = new Refining_TAMaintHistory();
            Refining_Notes Notes = new Refining_Notes();
            Refining_ReceiptsShipments RecShip = new Refining_ReceiptsShipments();
            Refining_ThermalEnergy ThermalEnergy = new Refining_ThermalEnergy();

            Helper.WriteLog("--------------------------------------------------------------------------------------------------------------------------------------------", @"Refining.log");
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Refining Import Started", CurrentLogFile);
            //Read Data from Excel to Objects
            Contact = GetCompanyInfo();
            if (Contact == null)
            {
                rtn = "MISSING COMPANY INFORMATION";
                return rtn;
            }
                
            CompanyID = WriteCompanyInfo(Contact);
            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Contacts Read", CurrentLogFile);
            //Get Next SubmissionID
            SubmissionID = Helper.GetNextSubmissionID(CONNECTSTR, CompanyID);
            Helper.WriteSubmission(CONNECTSTR, SubmissionID, CompanyID);
            
            Units = GetUnits();
            WriteUnits(Units);







            Helper.WriteLog("SubmissionID:" + SubmissionID + ": Reading Aprons, Fuelers, Carts, Hyrdrants, LoadStations, Pumps, PumpStations and Tanks Written to Database", CurrentLogFile);

            rtn = "Complete";
            Helper.WriteLog("SubmissionID:" + SubmissionID + " - Refining Import Complete", CurrentLogFile);

            //Close Connection
            cn.Close();

            //Close and Destroy Excel Object
            excelWorkbook.Close(SaveChanges: false);
            excelWorkbook = null;

            excel = null;
            return rtn;
        }


        private Refining_CompanyInfo GetCompanyInfo()
        {


            Refining_CompanyInfo comp = new Refining_CompanyInfo();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Refinery-Level Input"];
            try
            {
                if (xlWorkSheet.get_Range("Company").Value2 != null)
                {
                    comp.Company = xlWorkSheet.get_Range("Company").Value2;
                    comp.Location = xlWorkSheet.get_Range("Location").Value2;
                    comp.CoordName = xlWorkSheet.get_Range("CoordName").Value2;
                    comp.CoordTitle = xlWorkSheet.get_Range("CoordTitle").Value2;
                    comp.CoordPhone = xlWorkSheet.get_Range("CoordPhone").Value2;
                    comp.CoordEmail = xlWorkSheet.get_Range("CoordEmail").Value2;
                }
                else
                    return null;


            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetCompanyInfo";
            }

            return comp;

        }


        private int WriteCompanyInfo(Refining_CompanyInfo contact)
        {
            int rtn = 0;
            cmd.CommandText = "Refining_InsertCompanyInformation";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = cn;

            cmd.Parameters.Clear();
            cmd.Parameters.Add(new SqlParameter("@Company", Write(contact.Company)));
            cmd.Parameters.Add(new SqlParameter("@Location", Write(contact.Location)));
            cmd.Parameters.Add(new SqlParameter("@CoordName", Write(contact.CoordName)));
            cmd.Parameters.Add(new SqlParameter("@CoordTitle", Write(contact.CoordTitle)));
            cmd.Parameters.Add(new SqlParameter("@CoordPhone", Write(contact.CoordPhone)));
            cmd.Parameters.Add(new SqlParameter("@CoordEmail", Write(contact.CoordEmail)));


            try
            {
                rtn = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (SqlException ex)
            {
                Error = "SubmissionID:" + SubmissionID + ": WriteCompanyInfo" + ": ex.Message";

            }

            return rtn;

        }

        private List<Refining_Units> GetUnits()
        {

            int i = 1;
            List<Refining_Units> units = new List<Refining_Units>();


            Excel.Sheets excelSheets = excelWorkbook.Worksheets;
            Excel.Worksheet xlWorkSheet = excelWorkbook.Worksheets["Process-Level Input"];
            try
            {

                Excel.Range rUnits = xlWorkSheet.get_Range("Units");

                int NumCols = rUnits.Columns.Count;



                for (i = 1; i <= NumCols; i++)
                {
                    
                        Refining_Units config = new Refining_Units();

                        config.UnitID = Convert.ToInt32(rUnits.Cells[2, i].Value);
                        config.UnitName = rUnits.Cells[3, i].Value;
                        config.ProcessID = rUnits.Cells[4, i].Value;
                        config.ProcessType = rUnits.Cells[5, i].Value;
                        config.ProcessSubType = rUnits.Cells[6, i].Value;
                        config.Capacity = Convert.ToInt32(rUnits.Cells[7, i].Value);
                        config.CapacityUtil = Convert.ToInt32(rUnits.Cells[8, i].Value);
                        
                        if (config.UnitName.Length > 1) units.Add(config);
                   

                }
            }
            catch (Exception ex)
            {
                Error = ex.Message + ": SubmissionID= " + SubmissionID + ": GetUnits(" + i + ")";

            }

            return units;


        }


        private void WriteUnits(List<Refining_Units> units)
        {

            cmd.CommandText = "Refining_InsertUnits";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (Refining_Units unit in units)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.Add(new SqlParameter("@SubmissionID", SubmissionID));
                cmd.Parameters.Add(new SqlParameter("@UnitID", unit.UnitID));
                cmd.Parameters.Add(new SqlParameter("@CompanyID", CompanyID));
                cmd.Parameters.Add(new SqlParameter("@UnitName", Write(unit.UnitName)));
                cmd.Parameters.Add(new SqlParameter("@ProcessID", Write(unit.ProcessID)));
                cmd.Parameters.Add(new SqlParameter("@ProcessType", Write(unit.ProcessType)));
                cmd.Parameters.Add(new SqlParameter("@ProcessSubType", Write(unit.ProcessSubType)));
                cmd.Parameters.Add(new SqlParameter("@Capacity", Write(unit.Capacity)));
                cmd.Parameters.Add(new SqlParameter("@CapacityUtil", Write(unit.CapacityUtil)));
                

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException ex)
                {
                    Error = ex.Message + ": SubmissionID= " + SubmissionID + ": WriteUnits";
                    return;
                }



            }
        }

        

        public int? Write(int? str)
        {
            if (str == null)

                return 0;
            else
                return Convert.ToInt32(str);
        }

        public string Write(string str)
        {
            if (str == null)

                return "";
            else
                return str.ToString();
        }
    }


}
