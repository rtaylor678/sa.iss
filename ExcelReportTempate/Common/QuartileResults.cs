﻿using System;
namespace Common
{
    public struct QuartileResults
    {
        public Single top2 { get; set; }
        public Single tile1Break { get; set; }
        public Single tile2Break { get; set; }
        public Single tile3Break { get; set; }
        public Single bottom2 { get; set; }

        public Single currentMonth { get; set; }

        public Single RollingAvg { get; set; }
        public string KpiName { get; set; }
    }

    public struct QuartileDisplayResults
    {
        public Single Invisible { get; set; }
        public Single Q1Bar { get; set; }
        public Single Q2Bar { get; set; }
        public Single Q3Bar { get; set; }
        public Single Q4Bar { get; set; }
        public Single CurrentMonth { get; set; }
        public Single RollingAvg { get; set; }
    }

  
}