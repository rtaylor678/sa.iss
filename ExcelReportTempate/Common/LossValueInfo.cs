﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public struct LossValueInfo
    {
        public Single RefineryLossActualLoss { get; set; }
        public Single RefineryLossTargetLoss { get; set; }
        public Single RefineryLossValueOfLossPerBblFeed { get; set; }
        public Single RefineryLossValueOfLossForTheMonthVersusTargetLoss { get; set; }
        public Single RefineryLossAnnualizedValueOfLossVersusTarget { get; set; }

        //"LOAVT" is short for "Lost Operational Availability Versus Target"
        public Single LOAVTTargetOA { get; set; }
        public Single LOAVTActualOA { get; set; }
        public Single LOAVTValueOfLostOAPerBbl { get; set; }
        public Single LOAVTLostFeedEquivalentversusTargetOA { get; set; }
        public Single LOAVTValueOfLostOAForTheMonth { get; set; }
        public Single LOAVTAnnualizedValueOfLostOA { get; set; }
        public DateTime MonthAndYear { get; set; }
    }
}
