﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class InjuryInfo
    {
        public float TotalIncididentsForMonth { get; set; }
        public float TotalIncididentsYTD { get; set; }
        public float TotalPersonnelWorkhoursForMonth { get; set; }
        public float TotalPersonnelWorkhoursYTD { get; set; }
        public float LostTimeInjuryRateForMonth { get; set; }
        public float LostTimeInjuryRateYTD { get; set; }
        public int MonthDataIsFor { get; set; }
        public int YearDataIsFor { get; set; }


    }
}
