﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Excel = Microsoft.Office.Interop.Excel;

namespace ExcelManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("If you have loaded the mPEI (MaintPEI_Target) and nmPEI (NonMaintPEI_Target) targets, then enter 'Y'");
            /*
            Declare @SubmissionID int ;
            Set @SubmissionID = 10138; --fuels SEP 2017
            UPDate
            --select MaintPEI_Target, NonMaintPEI_Target  from
            ProfileFuels12.dbo.GenSum
            set MaintPEI_Target = 162
            --fuels and lubes: and UOM = 'MET'and Currency = 'USD'
            where SubmissionID = @SubmissionID  and factorset = 2012  and scenario = 'CLIENT' and UOM = 'MET'and Currency = 'USD'
            UPDate
            --select MaintPEI_Target, NonMaintPEI_Target  from
            ProfileFuels12.dbo.GenSum
            set NonMaintPEI_Target = 331
            --fuels and lubes: and UOM = 'MET'and Currency = 'USD'
            where SubmissionID = @SubmissionID  and factorset = 2012  and scenario = 'CLIENT'
            and UOM = 'MET'and Currency = 'USD'

            DO LUBES TOO
            Set @SubmissionID = 10655; --lubes NOV 2017
           UPDate
            --select MaintPEI_Target, NonMaintPEI_Target  from
            ProfileFuels12.dbo.GenSum
            set MaintPEI_Target = 234
            --fuels and lubes: and UOM = 'MET'and Currency = 'USD'
            where SubmissionID = @SubmissionID  and factorset = 2012  and scenario = 'CLIENT' and UOM = 'MET'and Currency = 'USD'
            UPDate
            --select MaintPEI_Target, NonMaintPEI_Target  from
            ProfileFuels12.dbo.GenSum
            set NonMaintPEI_Target =  230
            --fuels and lubes: and UOM = 'MET'and Currency = 'USD'
            where SubmissionID = @SubmissionID  and factorset = 2012  and scenario = 'CLIENT'
            and UOM = 'MET'and Currency = 'USD'


           */
            string trgs = Console.ReadLine();
            if (trgs.Trim().ToUpper() != "Y")
                return;
            Console.WriteLine("Enter ordinal of month to use");
            string inputMonth = Console.ReadLine();
            int month = Int32.Parse(inputMonth);
            Console.WriteLine("Enter year to use");
            string inputYear = Console.ReadLine();
            int year=Int32.Parse(inputYear);
            DateTime date = new DateTime(year, month, 1);
            
            Console.WriteLine("Enter F for Fuels, or L for Lubes");
            string refineryType = Console.ReadLine();
            switch (refineryType.Trim().ToUpper())
            {
                case "F":
                    BusinessManager.Fuels fuels = new BusinessManager.Fuels(date);
                    fuels.Run();
                    fuels = null;
                    break;
                case "L":
                    BusinessManager.Lubes lubes = new BusinessManager.Lubes(date);
                    lubes.Run();
                    lubes = null;
                    break;
                default:
                    break;
            }

        }
    }
}
