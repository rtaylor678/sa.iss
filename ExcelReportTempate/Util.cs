﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Data;
using Common;
using System.Configuration;

namespace BusinessManager
{
    public static class Util
    {
        public static List<string> Get24MonthNames(int currentMonth)
        {
            List<string> results = new List<string>();
            
            int startMonth = currentMonth + 1;
            if (currentMonth == 0)
                currentMonth = 12;
            DateTime startDate = new DateTime(DateTime.Now.Year, startMonth, 1);
            for (int i = 0; i < 24; i++)
            {
                results.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(startDate.Month));
                startDate = startDate.AddMonths(1);
            }
            return results;
        }

        public static bool WriteList(ref ExcelService.Refinery _xlRefinery, ref Excel.Worksheet sheet, List<string> lst, ref int xlrow)
        {
            try
            {
                if (lst != null)
                {
                    int c = 2;
                    for (int lrow = 0; lrow < lst.Count; lrow++)
                    {
                        _xlRefinery.Write(ref sheet, xlrow, c + lrow, lst[lrow]);
                    }
                }
                xlrow++;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool WriteUserDefinedByHeader(ref Excel.Worksheet sheet, string header, 
            ref int startXlsRowNumber, string conxString,
            DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {
                int count = 0;
                DBHelper dbHelper = new DBHelper(conxString);
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);

                string sql = queries.UserDefinedThisMonth(header);
                DataTable dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber, 1, header + "CurrentMonth");
                
                WriteRows(ref sheet, dTable, startXlsRowNumber);
                count++;

                sql = queries.UserDefinedCurrentYear(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "CurrentYear");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                sql = queries.UserDefinedLastCalendarYear(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "LastCalendarYear");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count, 12);
                count++;

                sql = queries.UserDefinedLast12Months(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "Last12Months");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count, 12);
                count++;

                sql = queries.UserDefinedLast24Months(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "Last24Months");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count, 24);
                count++;

                sql = queries.UserDefinedTargets(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "Target");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                sql = queries.UserDefinedCurrentYear(header);
                dTable = dbHelper.GetDataTable(sql);
                List<float> lst = AvgFromRows(dTable, "RptValue");
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "Avg");
                int rowSoFar = startXlsRowNumber + count;
                WriteListToRow(ref sheet, lst, ref rowSoFar);
                count++;

                /*
                YTD is just a set of charts that start in THIS YEAR and end THIS YEAR, it is not a sum of the values so far.
                sql = _routines.UserDefinedCurrentYear(header);
                dTable = _dbHelper.GetDataTable(sql);
                lst = YtdFromRows(dTable, "RptValue");
                _xlLubes.Write(ref sheet, startXlsRowNumber+ count, 1,header+"Ytd");
                WriteList(ref sheet, lst, startXlsRowNumber + count);
                count++;
                */
                startXlsRowNumber = startXlsRowNumber + count;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static bool WriteLossValueInfoByHeader(ref Excel.Worksheet sheet, string header, ref int startXlsRowNumber,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {                
                List<float> last24MonthsOfSomething = new List<float>();
                List<float> targets = new List<float>();
                DBHelper dbHelper = new DBHelper(conxString);                
                // get longest oldest set of data
                //Last24Months
                DateTime monthToUse = DateTime.Now.AddMonths(-24);
                for (int i = 0; i < 24; i++)
                {
                    Common.LossValueInfo temp = Util.GetLossValue(conxString, monthToUse, refNum, dataSet,
                    factorSet, scenario, currency, uOM);
                    last24MonthsOfSomething.Add(temp.RefineryLossAnnualizedValueOfLossVersusTarget);
                    /*
                    //get target - From UserDefined
                    DateTime tempDate = new DateTime(monthToUse.Year, monthToUse.Month, 1);
                    Queries routines = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                    string sql = routines.UserDefinedTarget("LossValueTarget", monthToUse);
                    DataRow dRow = dbHelper.GetFirstRow(sql);
                    WriteValue(ref sheet, startXlsRowNumber, 2, float.Parse(dRow[0].ToString()));
                    */

                    //get avg 
                    //throw new Exception("Need to calculate avearge. Try AvgFromRows()");
                    monthToUse = monthToUse.AddMonths(1);
                }
                List<float> valuesToRow = new List<float>();
                //CurrentMonth
                WriteString(ref sheet, startXlsRowNumber, 1, header + "CurrentMonth");
                valuesToRow.Add(last24MonthsOfSomething[last24MonthsOfSomething.Count - 1]);
                WriteValue(ref sheet, startXlsRowNumber, 2, valuesToRow[0]);
                startXlsRowNumber++;

                //CurrentYear
                WriteString(ref sheet, startXlsRowNumber, 1, header + "CurrentYear");
                //find the last January figure and add all after it to the list
                valuesToRow = LastJanuaryAndForward(last24MonthsOfSomething, startDate);

                List<float> currentYear = valuesToRow;

                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //LastCalendarYear: prior year from Jan to Dec
                WriteString(ref sheet, startXlsRowNumber, 1, header + "LastCalendarYear");
                valuesToRow = LastCalendarYear(last24MonthsOfSomething, startDate);
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //Last12Months
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Last12Months");
                valuesToRow = Last12Months(last24MonthsOfSomething, startDate);
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //Last24Months
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Last24Months");
                valuesToRow = Last24Months(last24MonthsOfSomething, startDate);
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //_YTD
                WriteString(ref sheet, startXlsRowNumber, 1, header + "YTD");
                valuesToRow= new List<float>();
                for (int i = 1; i <= startDate.Month; i++)
                    valuesToRow.Add(0); //might need this later so keep its place in the xls file . . .
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //_Target
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Target");
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                string sql = queries.UserDefinedTargets(header + "Tgt");
                DataTable  dTable= dbHelper.GetDataTable(sql);
                int tgtCol = 2;
                foreach(DataRow row in dTable.Rows)
                {
                    WriteValue(ref sheet, startXlsRowNumber, tgtCol, float.Parse(row[0].ToString()));
                    tgtCol++;
                }
                startXlsRowNumber++;
                //_Avg
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Average");

                //send in a list with all the values containing the data you need
                List<float> avgs = AvgFromList(currentYear);
                WriteListToRow(ref sheet, avgs, ref startXlsRowNumber);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static bool Write24MonthTargets(ref Excel.Worksheet sheet, ref int xlrow,
            string tableName, string fieldName,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
                {
            try
            {
                List<float> last24Months = GetLast24TargetsFromTable(tableName,fieldName,conxString,
                    startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                WriteString(ref sheet, xlrow, 1, fieldName + "Last24Targets");
                WriteListToRow(ref sheet, last24Months, ref xlrow);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Write24MonthUserDefinedTargets(ref Excel.Worksheet sheet, ref int xlrow, 
            string headerText,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {
                List<float> last24Months = GetLast24TargetsFromUserDefined(headerText, conxString, startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                WriteString(ref sheet, xlrow, 1, headerText + "Last24Targets");
                WriteListToRow(ref sheet, last24Months, ref xlrow);
                return true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static List<float> GetLast24TargetsFromTable(string tableName, string fieldName,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            List<float> last24Months = new List<float>();
            List<float> targets = new List<float>();
            DBHelper dbHelper = new DBHelper(conxString);
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            string sql = queries.GetLast23TargetsFromTable(tableName, fieldName);
            DataTable dTable = dbHelper.GetDataTable(sql);
            if (dTable != null && dTable.Rows.Count > 0)
            {
                int missingRows = 24 - dTable.Rows.Count;
                while (missingRows > 0)
                {
                    //pad with zeroes
                    last24Months.Add(0);
                    missingRows--;
                }
                foreach (DataRow row in dTable.Rows)
                {
                    float valu = 0;
                    try
                    {
                        valu = float.Parse(row[0].ToString());
                    }
                    catch
                    {
                        valu = 0;
                    }
                    last24Months.Add(valu);
                }
            }
            return last24Months;
        }
        public static List<float> GetLast24TargetsFromUserDefined(string headerText,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            List<float> last24Months = new List<float>();
            List<float> targets = new List<float>();
            DBHelper dbHelper = new DBHelper(conxString);
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            string sql = queries.UserDefinedLast23Targets(headerText);
            DataTable dTable = dbHelper.GetDataTable(sql);
            if (dTable != null && dTable.Rows.Count > 0)
            {
                int missingRows = 24 - dTable.Rows.Count;
                while (missingRows>0)
                {
                    //pad with zeroes
                    last24Months.Add(0);
                    missingRows--;
                }
                foreach (DataRow row in dTable.Rows)
                {
                    float valu = 0;
                    try
                    {
                        valu = float.Parse(row[0].ToString());
                    }
                    catch
                    {
                        valu = 0;
                    }
                    last24Months.Add(valu);
                }
            }
            return last24Months;
        }

        private static List<Single> AvgFromRows(DataTable dtable, string fieldName)
        {//use where have multiple values on diff datarows
            List<Single> results = new List<Single>();
            Single sum = 0;
            try
            {
                if (dtable != null && dtable.Rows.Count > 0)
                {
                    foreach (DataRow row in dtable.Rows)
                    {
                        if (row[fieldName] != DBNull.Value)
                        {
                            Single val = Single.Parse(row[fieldName].ToString());
                            //Single val = (Single)row[fieldName];
                            sum += val;
                            int denominator = results.Count + 1;
                            Single avg = sum / denominator;
                            if (avg.ToString() == "NaN")
                                avg = 0;
                            results.Add(avg);
                        }
                        else
                        {
                            Single val = 0;
                            sum += val;
                            Single avg = sum / results.Count;
                            results.Add(avg);
                        }
                    }
                }
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<float> AvgFromList(List<float> listIn)
        {
            List<float> results = new List<float>();
            float sum = 0;
            try
            {
                if (listIn != null && listIn.Count > 0)
                {
                    foreach (float valu in listIn)
                    {
                        sum += valu;
                        int denominator = results.Count + 1;
                        Single avg = sum / denominator;
                        if (avg.ToString() == "NaN")
                            avg = 0;
                        results.Add(avg);
                    }
                }
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<float> LastJanuaryAndForward(List<float> list, DateTime submissionMonthAndYear)
        {
            List<float> result = new List<float>();
            //if submission month is January, then only return that.
            if(submissionMonthAndYear.Month==1)
            {
                result.Add(list[list.Count - 1]);
                return list;
            }
            int i = 0;
            for(i=0;i>-12;i--)
            {
                DateTime temp = submissionMonthAndYear.AddMonths(i);
                if (temp.Month == 1)
                    break;
            }

            for(int j=list.Count-1 + i;j< list.Count; j++)
            {
                result.Add(list[j]);
            }
            return result;
        }

        public static List<float> LastCalendarYear(List<float> list, DateTime submissionMonthAndYear)
        {
            List<float> result = new List<float>();
            DateTime twoJanuariesAgo = new DateTime(submissionMonthAndYear.Year - 1, 1, 1); //The January of the month for current year's numbers
            DateTime lastDecember = new DateTime(twoJanuariesAgo.Year, 12, 1);
            int i = 0;
            for (i = 0; i < 24; i--)
            {
                DateTime temp = submissionMonthAndYear.AddMonths(i);
                if (temp.Month == twoJanuariesAgo.Month && temp.Year== twoJanuariesAgo.Year)
                    break;
            }
            int monthCount = 1;
            for (int j = list.Count -1  + i; j <= list.Count; j++)
            {
                if (monthCount > 12)
                    break;
                result.Add(list[j]);
                monthCount++;
            }
            return result; // { 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111 };
        }

        public static List<float> Last12Months(List<float> list, DateTime submissionMonthAndYear)
        {
            List<float> result = new List<float>();
            for (int i = list.Count - 11-1; i <= list.Count-1; i++)
            {
                result.Add(list[i]);
            }
            return result; // { 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111 };
        }

        public static List<float> Last24Months(List<float> list, DateTime submissionMonthAndYear)
        {
            List<float> result = new List<float>();
            for (int i = list.Count - 23 - 1; i <= list.Count - 1; i++)
            {
                result.Add(list[i]);
            }
            return result; // { 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111 };
        }

        public static bool WriteListToRow(ref Excel.Worksheet sheet, List<float> lst, ref int xlrow)
        {
            try
            {
                if (lst != null)
                {
                    int c = 2;
                    for (int lrow = 0; lrow < lst.Count; lrow++)
                    {
                        WriteValue(ref sheet, xlrow, (c + lrow), lst[lrow]);
                    }
                }
                xlrow++;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static bool WriteRows(ref Excel.Worksheet sheet, DataTable dTable, int xlrow, int minRows = 0)
        {   //use when your query returns multipoe rows that you want to write horizontally

            //use minRows to pad missing data with 0s

            try
            {
                int pad = 0;
                int c = 2;
                if (minRows > 0)
                {
                    if (dTable == null)
                    {
                        pad = minRows;
                    }
                    else
                    {
                        pad = minRows - dTable.Rows.Count;
                    }
                }
                while (pad > 0)
                {
                    WriteString(ref sheet, xlrow, c, "0");
                    pad -= 1;
                    c += 1;
                }

                if (dTable != null)
                {
                    for (int drow = 0; drow < dTable.Rows.Count; drow++)
                    {
                        WriteString(ref sheet, xlrow, c + drow, dTable.Rows[drow][0].ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static bool WriteString(ref Excel.Worksheet sheet, int row, int col, string val)
        {
            try
            {
                sheet.Cells[row, col].Value = val;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }

        private static bool WriteValue(ref Excel.Worksheet sheet, int row, int col, float val)
        {
            try
            {
                sheet.Cells[row, col].Value = val;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }

        public static InjuryInfo GetLostTime(DBHelper dbHelper, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            InjuryInfo info = new InjuryInfo();
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            String sql = queries.GetTotalPersonnelWorkoursForMonthSql();
            info.TotalPersonnelWorkhoursForMonth = GetFloatDbValue(dbHelper,sql);

            sql = queries.GetTotalPersonnelWorkoursYtdSql();
            info.TotalPersonnelWorkhoursYTD = GetFloatDbValue(dbHelper, sql);

            //GetTotalLostTimeInjuriesForMonth
            sql = queries.CurrentMonthLostTimeInjuriesSql(); 
            info.TotalIncididentsForMonth = GetFloatDbValue(dbHelper, sql);

            ////GetTotalLostTimeInjuriesYTD
            sql = queries.CurrentYearLostTimeInjuriesSql();
            DataTable dTable = dbHelper.GetDataTable(sql);
            if(dTable!=null)
            {
                float temp = 0;
                foreach(DataRow row in dTable.Rows)
                {
                    temp += float.Parse(row[0].ToString());
                }

                info.TotalIncididentsYTD = temp; // GetFloatDbValue(dbHelper, sql);
            }
            info.LostTimeInjuryRateForMonth = info.TotalIncididentsForMonth / info.TotalPersonnelWorkhoursForMonth * 200000;
            info.LostTimeInjuryRateYTD = info.TotalIncididentsYTD / info.TotalPersonnelWorkhoursYTD;
            return info;
        }

        public static EnviroInfo GetEnviroInfo(DBHelper dbHelper, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            EnviroInfo info = new EnviroInfo();
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            String sql = queries.GetTotalPersonnelWorkoursForMonthSql();
            info.TotalPersonnelWorkhoursForMonth = GetFloatDbValue(dbHelper, sql);

            sql = queries.GetTotalPersonnelWorkoursYtdSql();
            info.TotalPersonnelWorkhoursYTD = GetFloatDbValue(dbHelper, sql);

            sql = queries.CurrentMonthEnviroEventsSql();
            info.TotalEnviroEventsForMonth = GetFloatDbValue(dbHelper, sql);

            //sql = queries.CurrentYearEnviroEventsSql();
            //info.TotalEnviroEventsYTD = GetLongDbValue(dbHelper, sql);
            sql = queries.CurrentYearEnviroEventsSql();
            DataTable dTable = dbHelper.GetDataTable(sql);
            if (dTable != null)
            {
                float temp = 0;
                foreach (DataRow row in dTable.Rows)
                {
                    temp += float.Parse(row[0].ToString());
                }

                info.TotalEnviroEventsYTD = temp; // GetFloatDbValue(dbHelper, sql);
            }

            info.EnviroIncidentRateForMonth = info.TotalEnviroEventsForMonth / info.TotalPersonnelWorkhoursForMonth * 200000;
            info.EnviroIncidentRateYTD = info.TotalEnviroEventsYTD / info.TotalPersonnelWorkhoursYTD;
            return info;
        }

        public static LossValueInfo GetLossValue(string conxString, DateTime startDate, string refNum, string dataSet, 
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                int daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);
                int daysInYear = 0;
                for (int i = 1; i < 13; i++)
                {
                    daysInYear += DateTime.DaysInMonth(startDate.Year, i);
                }
                Single avgRawMatCost = GetLongDbValue(conxString, queries.UserDefinedRptValue("AvgRawMatCost")); ; //input form RefLevelInput G57
                Single avgVariableCashOpEx = GetLongDbValue(conxString, queries.UserDefinedRptValue("AvgVariableCashOpEx")); //input form RefLevelInput G58
                Single avgVariableNetCashMargin = GetLongDbValue(conxString, queries.UserDefinedRptValue("AvgVariableNetCashMargin")); //input form RefLevelInput G59

                Single tgtHydrocarbonLossPcntOfFeed = Single.Parse(ConfigurationManager.AppSettings["FuelsLostOpportunityTarget"].ToString());
                Single tgtOperationalAvailability = Single.Parse(ConfigurationManager.AppSettings["FuelsOperationalAvailabilityTarget"].ToString());

                long rawMatInputBblPerMoTot = GetLongDbValue(conxString, queries.UserDefinedRptValue("RawMatInputBblPerMoTot"));
                long crudeChargeDetailsBblPerMo = GetLongDbValue(conxString, queries.UserDefinedRptValue("CrudeChargeDetailsBblPerMo"));
                long refineryNetMaterialInput = rawMatInputBblPerMoTot + crudeChargeDetailsBblPerMo;

                Single utilization = GetDbValue(conxString, queries.GensumUtilPcnt());
                Single operationalAvailability = GetDbValue(conxString, queries.GensumOpAvail());
                Single lossMassPctnOfFeed = GetDbValue(conxString, queries.LossMassPctnOfFeed());
                LossValueInfo result = LossValueInfoCalc(daysInYear, daysInMonth, avgRawMatCost, avgVariableCashOpEx, avgVariableNetCashMargin, tgtHydrocarbonLossPcntOfFeed, tgtOperationalAvailability,
                    refineryNetMaterialInput, utilization, operationalAvailability, lossMassPctnOfFeed);

                //Single currentMonthoperationalAvailability    now a param
                //Single currentMonthLossMassPctnOfFeed  now a param
                //Single currentMonthValuesTonne = 0;  doesn't seem to be used
                //Single currentMonthValuesUtilization = 0;  now a param
                
                return result;
                
            }
            catch (Exception ex)
            { throw ex; }
        }

        private static Single GetDbValue(string conxString, string sql)
        {
            try
            {
                Single result = 0;
                DBHelper _dbHelper = new DBHelper(conxString);
                DataTable dTable = _dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count > 0)
                    result = Single.Parse(dTable.Rows[0][0].ToString());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static long GetLongDbValue(string conxString, string sql)
        {
            try
            {
                long result = 0;
                DBHelper _dbHelper = new DBHelper(conxString);
                DataTable dTable = _dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count > 0)
                    result = long.Parse(dTable.Rows[0][0].ToString());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static long GetLongDbValue(DBHelper dbHelper, string sql)
        {
            try
            {
                long result = 0;
                DataTable dTable = dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count > 0)
                    result = long.Parse(dTable.Rows[0][0].ToString());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static float GetFloatDbValue(DBHelper dbHelper, string sql)
        {
            try
            {
                float result = 0;
                DataTable dTable = dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count > 0)
                    result = float.Parse(dTable.Rows[0][0].ToString());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static Common.LossValueInfo LossValueInfoCalc(int daysInYear, int daysInMonth, Single avgRawMatCost,
            Single avgVariableCashOpEx, Single avgVariableNetCashMargin, Single tgtHydrocarbonLossPcntOfFeed,
            Single tgtOperationalAvailability, long refineryNetMaterialInput, Single utilization,
            Single operationalAvailability, Single lossMassPctnOfFeed)
        {

            //Single currentMonthoperationalAvailability    now a param
            //Single currentMonthLossMassPctnOfFeed  now a param
            //Single currentMonthValuesTonne = 0;  doesn't seem to be used
            //Single currentMonthValuesUtilization = 0;  now a param
            //Single currentMonthValuesValueCalculations = 0;  doesn't seem to be used

            Common.LossValueInfo results = new Common.LossValueInfo();
            results.RefineryLossActualLoss = lossMassPctnOfFeed / 100*refineryNetMaterialInput;
            results.RefineryLossTargetLoss = tgtHydrocarbonLossPcntOfFeed * refineryNetMaterialInput;
            results.RefineryLossValueOfLossPerBblFeed = avgRawMatCost + avgVariableCashOpEx;
            results.RefineryLossValueOfLossForTheMonthVersusTargetLoss = results.RefineryLossTargetLoss * results.RefineryLossValueOfLossPerBblFeed / 1000;
            results.RefineryLossAnnualizedValueOfLossVersusTarget = results.RefineryLossValueOfLossForTheMonthVersusTargetLoss * daysInYear / daysInMonth;

            results.LOAVTTargetOA = tgtOperationalAvailability * 100;
            results.LOAVTActualOA = operationalAvailability;
            results.LOAVTValueOfLostOAPerBbl = avgVariableNetCashMargin;
            results.LOAVTLostFeedEquivalentversusTargetOA = (results.LOAVTTargetOA - results.LOAVTActualOA) /100 * 
                (refineryNetMaterialInput/(utilization/100));
            results.LOAVTValueOfLostOAForTheMonth = (results.LOAVTTargetOA - results.LOAVTActualOA) /100 * 
                (refineryNetMaterialInput/(utilization/100)) * results.LOAVTValueOfLostOAPerBbl / 1000;
            results.LOAVTAnnualizedValueOfLostOA = results.LOAVTValueOfLostOAForTheMonth * daysInYear / daysInMonth;
            return results;
        }


        public static bool VerifyOutput(ref Excel.Worksheet sheet)
        {
            //ensure things didn't get out of place
            ValidOutput output = new ValidOutput();
            int row = 1;
            foreach (string expectedValue in output.OutputFields)
            {
                string foundValue = sheet.Cells[row, 1].Value ?? "";
                if (foundValue.Trim() != expectedValue.Trim())
                    return false;
                row++;
            }
            return true;
        }
    }
}
