﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;
using ExcelService;
using System.Data;
using Common;

namespace BusinessManager
{
    public class Queries
    {
        string _conxString = System.Configuration.ConfigurationManager.ConnectionStrings["ProfileFuelsConnectionString"].ToString();
        System.Data.SqlClient.SqlConnection _conx = null;
        Excel.Application _xl = null;
        Excel.Workbook _wb = null;
        Excel.Worksheet _ws = null;
        DateTime _startDate = DateTime.Today;
        DBHelper _dbHelper = null;
        ExcelService.Refinery _xlFuels = null;
        private string _refNum;
        private string _dataSet;
        private int _factorSet;
        private string _scenario;
        private string _currency;
        private string _uOM;
        public int SubmissionID {get; set; }

        public Queries(DateTime startDate, string refNum, string dataSet, int factorSet, string scenario, string currency, string uOM)
        {
            _startDate = startDate;
            _refNum = refNum;
            _dataSet = dataSet;
            _factorSet = factorSet;
            _scenario = scenario;
            _currency = currency;
            _uOM = uOM;
        }

        public string GetSubmissionID()
        {
            return "SELECT SubmissionID from dbo.SUbmissions where RefineryID = '" + _refNum
                + "' and periodStart = '" + _startDate + "';";
        }
        public string GenSumThisMonth(string fieldName, int factorSet, string scenario, string currency, string uOM)
        {
            DateTime nextMonth = _startDate.AddMonths(1); // new DateTime( _startDate.Year, _startDate.Month + 1, 1); 
            string sql = "Select " + fieldName + " from dbo.GenSum g inner join dbo.Submissions s on s.SubmissionID = g.SubmissionID ";
            sql += " where g.FactorSet = " + factorSet + " and g.Scenario = '" + scenario + "' and g.Currency = '" + currency + "' and g.UOM  = '" + uOM + "'";
            sql += " and g.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' ) ";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string GenSumCurrentYear(string fieldNames, int factorSet, string scenario, string currency, string uom) //string refnum, DateTime currentMonth)
        {
            DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
            string sql = "Select " + fieldNames + " from dbo.GenSum g inner join dbo.Submissions s on s.SubmissionID = g.SubmissionID ";
            sql += " where g.FactorSet = " + factorSet + " and g.Scenario = '" + scenario + "' and g.Currency = '" + currency + "' and g.UOM  = '" + uom + "'";
            sql += " and  g.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                mostRecentJanuary.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string GenSumLastCalendarYear(string fieldNames, int factorSet, string scenario, string currency, string uom) //string refnum, DateTime currentMonth)
        {
            DateTime twoJanuariesAgo = new DateTime(_startDate.Year - 1, 1, 1); //The January of the month for current year's numbers
            DateTime lastDecember = new DateTime(twoJanuariesAgo.Year, 12, 1);
            //DateTime elevenMonthsAgo = _startDate.AddMonths(-12);
            string sql = "Select " + fieldNames + " from dbo.GenSum g inner join dbo.Submissions s on s.SubmissionID = g.SubmissionID ";
            sql += " where g.FactorSet = " + factorSet + " and g.Scenario = '" + scenario + "' and g.Currency = '" + currency + "' and g.UOM  = '" + uom + "'";
            sql += " and  g.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                twoJanuariesAgo.ToString() + "' and PeriodStart <= '" + lastDecember.ToString() + "' ) ";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }
        public string GenSumLast12Months(string fieldNames, int factorSet, string scenario, string currency, string uom) //string refnum, DateTime currentMonth)
        {
            //DateTime twoJanuariesAgo = new DateTime(_startDate.Year - 1, 1, 1); //The January of the month for current year's numbers
            //DateTime lastDecember = new DateTime(twoJanuariesAgo.Year, 12, 1);
            DateTime elevenMonthsAgo = _startDate.AddMonths(-12);
            string sql = "Select " + fieldNames + " from dbo.GenSum g inner join dbo.Submissions s on s.SubmissionID = g.SubmissionID ";
            sql += " where g.FactorSet = " + factorSet + " and g.Scenario = '" + scenario + "' and g.Currency = '" + currency + "' and g.UOM  = '" + uom + "'";
            sql += " and  g.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart > '" +
                elevenMonthsAgo.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string GenSumLast24Months(string fieldNames, int factorSet, string scenario, string currency, string uom) //string refnum, DateTime currentMonth)
        {
            //DateTime twoYearsAgo = new DateTime(_startDate.Year - 2, _startDate.Month, 1);
            DateTime twentyThreeMonthsAgo = _startDate.AddMonths(-24);
            string sql = "Select " + fieldNames + " from dbo.GenSum g inner join dbo.Submissions s on s.SubmissionID = g.SubmissionID ";
            sql += " where g.FactorSet = " + factorSet + " and g.Scenario = '" + scenario + "' and g.Currency = '" + currency + "' and g.UOM  = '" + uom + "'";
            sql += " and  g.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart > '" +
                twentyThreeMonthsAgo.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string UserDefinedThisMonth(string headerText)
        {
            DateTime nextMonth = _startDate.AddMonths(1); // new DateTime( _startDate.Year, _startDate.Month + 1, 1); 
            string sql = "Select u.RptValue from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string UserDefinedCurrentYear(string headerText) //string refnum, DateTime currentMonth)
        {
            //DateTime januaryOfYearOfCurrentMonth = new DateTime(currentMonth.Year, 1, 1); //The January of the month for current year's numbers
            DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
            string sql = "Select u.RptValue from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                mostRecentJanuary.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        
        public string UserDefinedLastCalendarYear(string headerText) //string refnum, DateTime currentMonth)
        {

            DateTime twoJanuariesAgo = new DateTime(_startDate.Year-1, 1, 1); //The January of the month for current year's numbers
            DateTime lastDecember = new DateTime(twoJanuariesAgo.Year , 12, 1);
            //DateTime elevenMonthsAgo = _startDate.AddMonths(-12);
            string sql = "Select u.RptValue from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                twoJanuariesAgo.ToString() + "' and PeriodStart <= '" + lastDecember.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }
        public string UserDefinedLast12Months(string headerText) //string refnum, DateTime currentMonth)
        {

            //DateTime twoJanuariesAgo = new DateTime(_startDate.Year-1, 1, 1); //The January of the month for current year's numbers
            //DateTime lastDecember = new DateTime(twoJanuariesAgo.Year , 12, 1);
            DateTime elevenMonthsAgo = _startDate.AddMonths(-12);
            string sql = "Select u.RptValue from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart > '" +
                elevenMonthsAgo.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string UserDefinedLast24Months(string headerText) //string refnum, DateTime currentMonth)
        {
            //DateTime twoYearsAgo = new DateTime(_startDate.Year - 2, _startDate.Month, 1);
            //DateTime twoJanuariesAgo = new DateTime(twoYearsAgo.Year, 1, 1); //The January of the month for current year's numbers
            DateTime twentythreeMonthsAgo = _startDate.AddMonths(-24);
            string sql = "Select u.RptValue from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart > '" +
                twentythreeMonthsAgo.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string UserDefinedTargets(string headerText) //string refnum, DateTime currentMonth)
        {
            DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
            string sql = "Select u.RptValue_Target from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                mostRecentJanuary.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string UserDefinedLast23Targets(string headerText) //string refnum, DateTime currentMonth)
        { //use 23 becuuse including current month target
            DateTime twentyThreeMonthsAgo = _startDate.AddMonths(-23);
            string sql = "Select u.RptValue_Target from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                twentyThreeMonthsAgo.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string UserDefinedTarget(string headerText,DateTime monthAndYear) 
        {
            DateTime nextMonth = monthAndYear.AddMonths(1); // new DateTime( _startDate.Year, _startDate.Month + 1, 1); 
            string sql = "Select u.RptValue_Target from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                monthAndYear.ToString() + "' and PeriodStart <= '" + nextMonth.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string UserDefinedTargetForMonth(string headerText) //string refnum, DateTime currentMonth)
        {
            DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
            string sql = "Select u.RptValue_Target from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                mostRecentJanuary.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string UserDefinedRptValue(string headerText) 
        {
            DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
            string sql = "Select u.RptValue from dbo.UserDefined u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart > '" +
                mostRecentJanuary.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart desc";
            return sql;
        }

        public string InsertUserDefined(int submissionId, string header, string desc, float value, 
            int decPlaces, float target, float avg, float ytd)
        {
            string sql = "INSERT INTO ProfileFuels12.dbo.UserDefined values (" + submissionId.ToString() +
                ", '" + header + "', " +
                "'" + desc + "', " +
                value.ToString() + ", " +
                decPlaces.ToString() + ", " +
                target.ToString() + ", " +
                avg.ToString() + ", " +
                ytd.ToString() + "); ";
            return sql;

        }
        /*
        public string CurrentMonthTotalPersonnelWorkhoursSql() //int factorSet, string scenario, string currency, string uOM)
        {
            DateTime nextMonth = _startDate.AddMonths(1); // new DateTime( _startDate.Year, _startDate.Month + 1, 1); 
            string sql = "SELECT SUM(COALESCE(rptValue,0)) FROM [ProfileFuels12].[dbo].[UserDefined] ";
            sql += " where SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' ) ";
            sql += " and HeaderText in('ContractMaintHours','ContractNonMaintHours','GAMaintHours','GANonMaintHours','STHMaintHours','STHNonMaintHours');";
            return sql;
        }

        public string CurrentYearTotalPersonnelWorkhoursSql() 
        {
                DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
                string sql = "SELECT COALESCE(rptValue,0) FROM [ProfileFuels12].[dbo].[UserDefined] ";
                sql += " where SubmissionID in ";
                sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                    mostRecentJanuary.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
                sql += " and HeaderText in('ContractMaintHours','ContractNonMaintHours','GAMaintHours','GANonMaintHours','STHMaintHours','STHNonMaintHours');";
                return sql;
        }
        */
        public string CurrentYearUserDefinedSql(string headerText)
        {
            DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
            string sql = "SELECT COALESCE(rptValue,0) FROM [ProfileFuels12].[dbo].[UserDefined] ";
            sql += " where SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                mostRecentJanuary.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and HeaderText ='" + headerText + "';";
            return sql;
        }

        public string CurrentMonthUserDefinedSql(string headerText)
        { //SafetyLostTimeIncidents
            DateTime nextMonth = _startDate.AddMonths(1); // new DateTime( _startDate.Year, _startDate.Month + 1, 1); 
            string sql = "SELECT SUM(COALESCE(rptValue,0)) FROM [ProfileFuels12].[dbo].[UserDefined] ";
            sql += " where SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' ) ";
            sql += " and HeaderText in('" + headerText + "');";
            return sql;
        }

        public string CurrentYearEnviroEventsSql()
        {
            DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
            string sql = "SELECT COALESCE(rptValue,0) FROM [ProfileFuels12].[dbo].[UserDefined] ";
            sql += " where SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                mostRecentJanuary.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and HeaderText ='Reportable Environmental Incidents';";
            return sql;
        }

        public string CurrentMonthEnviroEventsSql()
        { //SafetyLostTimeIncidents
            DateTime nextMonth = _startDate.AddMonths(1); // new DateTime( _startDate.Year, _startDate.Month + 1, 1); 
            string sql = "SELECT SUM(COALESCE(rptValue,0)) FROM [ProfileFuels12].[dbo].[UserDefined] ";
            sql += " where SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' ) ";
            sql += " and HeaderText in('Reportable Environmental Incidents');";
            return sql;
        }

        public string GensumUtilPcnt()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "SELECT COALESCE(UtilPcnt,0) FROM [ProfileFuels12].[dbo].[GenSum] ";
            sql += " where SubmissionID = ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "') ";
            sql += " and factorset = " + _factorSet;
            sql += " and scenario = '" + _scenario + "'";
            sql += " and currency = '" + _currency + "'";
            sql += " and UOM = '" + _uOM + "';";
            return sql;
        }

        public string GensumOpAvail()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "SELECT COALESCE(OpAvail,0) FROM [ProfileFuels12].[dbo].[GenSum] ";
            sql += " where SubmissionID = ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "') ";
            sql += " and factorset = " + _factorSet;
            sql += " and scenario = '" + _scenario + "'";
            sql += " and currency = '" + _currency + "'";
            sql += " and UOM = '" + _uOM + "';";
            return sql;
        }

        public string LossMassPctnOfFeed()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "select (u.RptValue / gs.DaysInPeriod /(CASE WHEN gs.NetInputBPD < 1 THEN .001  ELSE gs.NetInputBPD END * 1000 * .137)) * 100 AS LossPcnt ";
            sql += " from dbo.UserDefined u inner join dbo.GenSum gs on u.SubmissionID = gs.SubmissionID ";
            sql += " where gs.SubmissionID = ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "') ";
            sql += " and gs.FactorSet = '" + _factorSet + "' AND gs.Scenario ='" + _scenario + "' AND UOM = '" + _uOM + "' and HeaderText = 'LOSS'";
            return sql;
        }

        public string GetLast23TargetsFromTable(string tableName, string fieldName) //string refnum, DateTime currentMonth)
        { //use 23 becuuse including current month target
            DateTime twentyThreeMonthsAgo = _startDate.AddMonths(-23);
            string sql = "Select u." + fieldName + " from ProfileFuels12." + tableName + " u inner join dbo.Submissions s on s.SubmissionID = u.SubmissionID ";
            sql += " where u.SubmissionID in ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                twentyThreeMonthsAgo.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' "
                + " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ) ";
            //sql += " and HeaderText = '" + headerText + "'";
            sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string Quartile(string dbName, string fieldName, string refListname) //string refnum, DateTime currentMonth)
        {
            string sql = string.Empty;
            //if (DateTime.Today == new DateTime(2018, 6, 24)) //dev database is down today, and data no longer exists in Prod
            //{
            //    sql = "SElect 0 as Top2, 0 as Tile1Break, 0 as Tile2Break, 0 as Tile3Break, 0 as Btm2 ;";
            //}
            //else
            //{
                //sql="SELECT Top2,Tile1Break,Tile2Break,Tile3Break,Btm2 FROM " + dbName + ".dbo.RankSummary WHERE RankVariable = '" + fieldName + "' ";
                sql = "SELECT Top2, Ptile25 as Tile1Break, Ptile50 as Tile2Break, Ptile75 as Tile3Break,  Btm2 FROM " + dbName + ".Ranking.Summary WHERE RankVariable = '" + fieldName + "' ";
            sql += " and GroupID = '" + refListname + "'";
            //sql += " and RankBreak = 'TotalList' AND reflistname = '" + refListname + "'";
            //}

            return sql;
        }
        public string Quartiles(string dbName, List<string> fieldNames, string refListname) //string refnum, DateTime currentMonth)
        {
            DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
            string sql = "SELECT Top2,Tile1Break,Tile2Break,Tile3Break,Btm2 FROM " + dbName + ".dbo.RankSummary WHERE RankVariable IN ";
            foreach (string fieldName in fieldNames)
            {
                sql += "'" + fieldName + "',";
            }
            sql = sql.Remove(sql.Length - 1); //trim trailing comma
            sql += " and GroupID = '" + refListname + "'";
            //sql += " and RankBreak = 'TotalList' AND reflistname = '" + refListname + "'";
            return sql;
        }

        public QuartileResults GetQuartilesForKpi(Single top2, Single tile1Break, Single tile2Break,
            Single tile3Break, Single bottom2, Single currentMonth, string kpiName, List<Single> kpiPriorValues)
        {
            Single curMo =100- System.Math.Min(System.Math.Max((currentMonth - bottom2) / (top2 - bottom2) * 100, 0), 97) ;
            QuartileResults results = new QuartileResults();
            results.top2 = 0;
            results.currentMonth =curMo;
            Single q1 = 100 - System.Math.Min(System.Math.Max((tile1Break - bottom2) / (top2 - bottom2) * 100, 0), 100);
            results.tile1Break = q1;
            Single q2 = 100 - System.Math.Min(System.Math.Max((tile2Break - bottom2) / (top2 - bottom2) * 100, 0), 100);
            results.tile2Break = q2;
            Single q3 = 100 - System.Math.Min(System.Math.Max((tile3Break - bottom2) / (top2 - bottom2) * 100, 0), 100);
            results.tile3Break = q3;
            results.bottom2 = 100;

            Single sumOfTrends = 0;
            int counter = 0;
            foreach (Single month in kpiPriorValues)
            {
                counter++;
                sumOfTrends += month;
            }
            Single avg = sumOfTrends / counter;
            Single rollingAvg = 100 - System.Math.Min(System.Math.Max((avg - bottom2) / (top2 - bottom2) * 100, 0), 97);
            results.RollingAvg = rollingAvg;
            results.KpiName = kpiName;
            return results;
        }

        public QuartileResults GetQuartilesForYTD(Single top2, Single tile1Break, Single tile2Break,
            Single tile3Break, Single bottom2, Single currentMonth, string kpiName, List<Single> kpiPriorValues)
        {
            Single curMo = System.Math.Min(System.Math.Max((currentMonth - bottom2) / (top2 - bottom2) * 100, 0), 97);
            QuartileResults results = new QuartileResults();
            results.top2 = 0;
            results.currentMonth = curMo;
            Single q1 = System.Math.Min(System.Math.Max((tile1Break - bottom2) / (top2 - bottom2) * 100, 0), 100);
            results.tile1Break = q1;
            Single q2 = System.Math.Min(System.Math.Max((tile2Break - bottom2) / (top2 - bottom2) * 100, 0), 100);
            results.tile2Break = q2;
            Single q3 = System.Math.Min(System.Math.Max((tile3Break - bottom2) / (top2 - bottom2) * 100, 0), 100);
            results.tile3Break = q3;
            results.bottom2 = 100;

            Single sumOfTrends = 0;
            int counter = 0;
            foreach (Single month in kpiPriorValues)
            {
                counter++;
                sumOfTrends += month;
            }
            Single avg = sumOfTrends / counter;
            Single rollingAvg = System.Math.Min(System.Math.Max((avg - bottom2) / (top2 - bottom2) * 100, 0), 97);
            results.RollingAvg = rollingAvg;
            results.KpiName = kpiName;
            return results;
        }

        public List<QuartileDisplayResults> AdjustQuartilesForDisplay(List<QuartileResults> list) //Single top2, Single tile1Break, Single tile2Break, Single tile3Break, Single bottom2, Single currentMonth)
        {
            List<QuartileDisplayResults> results = new List<QuartileDisplayResults>();
            //First need to find all the 2to3 breaks for ###ALL### the KPIs
            //Try to do this in GetQuartilesForKpi()
            Single largest2to3Break = 0;
            foreach(QuartileResults q in list)
            {
                if(q.tile2Break>largest2to3Break)
                {
                    largest2to3Break = q.tile2Break;
                }
            }
            foreach (QuartileResults q in list)
            {
                QuartileDisplayResults temp = new QuartileDisplayResults();
                Single offset = largest2to3Break - q.tile2Break;
                temp.Invisible = offset;
                //temp.top2 = 
                temp.Q1Bar = q.tile1Break -0;
                temp.Q2Bar = q.tile2Break - q.tile1Break;
                temp.Q3Bar = q.tile3Break - q.tile2Break;
                temp.Q4Bar = q.bottom2 - q.tile3Break;
                temp.CurrentMonth = q.currentMonth + offset;
                temp.RollingAvg = q.RollingAvg + offset;
                results.Add(temp);
            }
                return results;
        }

        public bool PopulateQuartiles(DataRow row, ref Single top2, ref Single q1, ref Single q2, ref Single q3, ref Single bottom2)
        {
            //reset
            top2 = 0;
            q1 = 0;
            q2 = 0;
            q3 = 0;
            bottom2 = 0;
            try
            { // Top2,Tile1Break,Tile2Break,Tile3Break,Btm2
                top2 = Single.Parse(row["Top2"].ToString());
                q1 = Single.Parse(row["Tile1Break"].ToString());
                q2 = Single.Parse(row["Tile2Break"].ToString());
                q3 = Single.Parse(row["Tile3Break"].ToString());
                bottom2 = Single.Parse(row["Btm2"].ToString());
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetTotalPersonnelWorkoursForMonthSql()
        {
            DateTime nextMonth = _startDate.AddMonths(1); 
            string sql = "select SUM(COALESCE(STH,0) + COALESCE(Contract,0) + COALESCE(GA,0)) ";
            sql += " from dbo.Pers where SubmissionID IN ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ) ";
            sql += " and PersID in('OCCMA','OCCPO');";
            //sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string GetTotalPersonnelWorkoursYtdSql()
        {
            DateTime mostRecentJanuary = new DateTime(_startDate.Year, 1, 1); //The January of the month for current year's numbers
            string sql = "select SUM(COALESCE(STH,0) + COALESCE(Contract,0) + COALESCE(GA,0)) ";
            sql += " from dbo.Pers where SubmissionID IN ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                mostRecentJanuary.ToString() + "' and PeriodStart <= '" + _startDate.ToString() + "' ) ";
            sql += " and PersID in('OCCMA','OCCPO');";
            //sql += " ORDER BY s.PeriodStart asc";
            return sql;
        }

        public string GetLossValueTotalNetRawMaterialInputSql()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "select Sum(BBL) from [ProfileFuels12].[dbo].[Yield] where (Category = 'RMI' and MaterialID = 'CRD') or (Category in( 'OTHRM', 'RLUBE')) AND SubmissionID IN ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ); ";
            return sql;
        }

        public string GetLossValueFinishedProdAdditSql()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "select BBL from [ProfileFuels12].[dbo].[Yield]	where SubmissionID = 12089 and Category = 'OTHRM' and MaterialID = 'ADD' AND SubmissionID IN ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ); ";
            return sql;
        }
        public string GetLossValueSubtotalVolumeRelatedExpenseSql()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "select STVol from OpExAll where scenario = 'CLIENT' AND SubmissionID IN ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ); ";
            return sql;
        }
        public string GetLossValueRawMaterialCostSql()
        {
            //throw new Exception("Comes from INput worksheet 'Refinery - Level Input', cell G57");
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "select RptValue from dbo.UserDefined where HeaderText = 'AvgRawMatCost' and SubmissionID = ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ) ";
            //sql += " AND factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' ";
            return sql;
        }
        public string GetLossValueSql()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "select RptValue from dbo.UserDefined where HeaderText = 'Loss' and SubmissionID = ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ) ";
            //sql += " AND factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' ";
            return sql;
        }
        public string GetLossValueBarrelsPerDay()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "select NetInputBPD from dbo.GenSum where SubmissionID = ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ) ";
            sql += " AND factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and UOM = '" + _uOM + "' ";
            return sql;
            //select  TOP 1 NetInputBPD from dbo.GenSum where SubmissionID 
        }
        public string GetLossValueRawMaterialInputkBPDSql()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "select g.NetInputBPD from dbo.GenSum g where SubmissionID = ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ) ";
            sql += " AND factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and UOM = '" + _uOM + "' ";
            return sql;
        }
        //
        public string GetLossValueTargetSql()
        {
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "select RptValue_Target from dbo.UserDefined where HeaderText = 'LossValueTgt' and SubmissionID = ";
            sql += " (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ) ";
            return sql;
        }

        public string GetGpv2016Sql()
        {
            //GLC sugested use 2016 because  2014 numbers won't look right
            DateTime nextMonth = _startDate.AddMonths(1);
            string sql = "SELECT GPV = ProdValue*1e6/mt.NetInputBbl FROM MaterialTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID INNER JOIN MaterialTot mt ON mt.SubmissionID = mtc.SubmissionID WHERE s.RefineryID = '" + _refNum + "'  AND mtc.Currency = '" + _currency + "' AND Scenario ='2016' ";
            //and s.submissionid = 12089
            sql += " and s.submissionid = (select SubmissionID from dbo.Submissions where RefineryID = '" + _refNum + "' and PeriodStart >= '" +
                _startDate.ToString() + "' and PeriodStart < '" + nextMonth.ToString() + "' "  //+ " and factorset = " + _factorSet + " and scenario = '" + _scenario + "' and currency = '" + _currency + "' and u.UOM = '" + _uOM + "' "
                + " ) ";
            return sql;
        }
    }
}
