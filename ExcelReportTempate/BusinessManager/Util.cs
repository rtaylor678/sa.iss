﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Data;
using Common;
using System.Configuration;

namespace BusinessManager
{
    public static class Util
    {
        public static List<string> Get24MonthNames(int currentMonth)
        {
            List<string> results = new List<string>();
            int startYear = DateTime.Now.Year;
            int startMonth = currentMonth + 1;
            if (currentMonth == 0)
                currentMonth = 12;
            if (startMonth > 12)
            {
                startMonth = 1;
                startYear += 1;
            }
            DateTime startDate = new DateTime(startYear, startMonth, 1);
            for (int i = 0; i < 24; i++)
            {
                results.Add(CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(startDate.Month));
                startDate = startDate.AddMonths(1);
            }
            return results;
        }

        public static bool WriteList(ref ExcelService.Refinery _xlRefinery, ref Excel.Worksheet sheet, List<string> lst, ref int xlrow)
        {
            try
            {
                if (lst != null)
                {
                    int c = 2;
                    for (int lrow = 0; lrow < lst.Count; lrow++)
                    {
                        _xlRefinery.Write(ref sheet, xlrow, c + lrow, lst[lrow]);
                    }
                }
                xlrow++;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool WriteUserDefinedByHeader(ref Excel.Worksheet sheet, string header,
            ref int startXlsRowNumber, string conxString,
            DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {
                int count = 0;
                DBHelper dbHelper = new DBHelper(conxString);
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);

                string sql = queries.UserDefinedThisMonth(header);
                DataTable dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber, 1, header + "CurrentMonth");

                WriteRows(ref sheet, dTable, startXlsRowNumber);
                count++;

                sql = queries.UserDefinedCurrentYear(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "CurrentYear");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                sql = queries.UserDefinedLastCalendarYear(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "LastCalendarYear");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count, 12);
                count++;

                sql = queries.UserDefinedLast12Months(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "Last12Months");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count, 12);
                count++;

                sql = queries.UserDefinedLast24Months(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "Last24Months");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count, 24);
                count++;

                sql = queries.UserDefinedTargets(header);
                dTable = dbHelper.GetDataTable(sql);
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "Target");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                sql = queries.UserDefinedCurrentYear(header);
                dTable = dbHelper.GetDataTable(sql);
                List<float> lst = AvgFromRows(dTable, "RptValue");
                WriteString(ref sheet, startXlsRowNumber + count, 1, header + "Avg");
                int rowSoFar = startXlsRowNumber + count;
                WriteListToRow(ref sheet, lst, ref rowSoFar);
                count++;

                /*
                YTD is just a set of charts that start in THIS YEAR and end THIS YEAR, it is not a sum of the values so far.
                sql = _routines.UserDefinedCurrentYear(header);
                dTable = _dbHelper.GetDataTable(sql);
                lst = YtdFromRows(dTable, "RptValue");
                _xlLubes.Write(ref sheet, startXlsRowNumber+ count, 1,header+"Ytd");
                WriteList(ref sheet, lst, startXlsRowNumber + count);
                count++;
                */
                startXlsRowNumber = startXlsRowNumber + count;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static bool WriteLossValueInfoByHeader(ref Excel.Worksheet sheet, string header, ref int startXlsRowNumber,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM, ref List<Single> currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss,
            ref List<Single> currentYearLOAVTValueOfLostOAForTheMonth, bool isFuels)
        {
            try
            {
                //throw new Exception("change from daily to monthly basis??");
                List<float> last24MonthsOfSomething = new List<float>();
                List<float> targets = new List<float>();
                //List<float> lossValueTargets = new List<float>();
                DBHelper dbHelper = new DBHelper(conxString);
                // get longest oldest set of data
                //Last24Months
                DateTime monthToUse = startDate.AddMonths(-23);
                List<float> last24MonthsOfRefineryLossValueOfLossForTheMonthVersusTargetLoss = new List<float>();
                List<float> last24MonthsOfLOAVTValueOfLostOAForTheMonth = new List<float>();
                for (int i = 0; i < 24; i++)
                {
                    Queries routines = new Queries(monthToUse, refNum, dataSet, factorSet, scenario, currency, uOM);
                    //Single lossValueTargetForThisMonth = GetDbValue(conxString, routines.GetLossValueTargetSql());
                    //

                    /*
                    Common.LossValueInfo temp = Util.GetLossValue2(conxString, monthToUse, refNum, dataSet,
                    factorSet, scenario, currency, uOM, isFuels);
                    */

                    /*
                     *  //Probably want to use  Util.LossValueVsTargetKDollars__2018_08_24() instead.
                    Single temp = Util.GetLossValueVsTargetkDollars(conxString, monthToUse, refNum, dataSet,
                    factorSet, scenario, currency, uOM, isFuels, lossValueTargetForThisMonth);
                    last24MonthsOfSomething.Add(temp);
                    */
                    int yearToUse = startDate.Year;
                    float opAvail = GetDbValue(conxString, routines.GensumOpAvail());
                    float netInputBPD = GetDbValue(conxString, routines.GenSumThisMonth("NetInputBPD", factorSet, scenario, currency, uOM));
                    float utilPcnt =  GetDbValue(conxString, routines.GenSumThisMonth("UtilPcnt", factorSet, scenario, currency, uOM));

                    //need to repopulate these each time because year will differ from startDate arg:
                    string yearForConfig = monthToUse.Year.ToString();
                    if (yearForConfig == "2016" || yearForConfig == "2017")
                        yearForConfig = "20162017";
                    string refineryTypeForConfig = "Fuels";
                    if(!isFuels)
                        refineryTypeForConfig = "Lubes";
                    float rawMaterialCostUsdPerBblNetInput = Util.ReadConfig(refineryTypeForConfig + "rawMaterialCostUsdPerBblNetInput" + yearForConfig);
                    float grossMarginUsdPerBblNetInput = Util.ReadConfig(refineryTypeForConfig + "grossMarginUsdPerBblNetInput" + yearForConfig);
                    float lossTarget = Util.ReadConfig(refineryTypeForConfig + "lossTarget" + yearForConfig);
                    targets.Add(lossTarget);
                    float variableCashOpex = Util.ReadConfig(refineryTypeForConfig + "variableCashOpex" + yearForConfig);
                    float hydrocarbonLossValue = variableCashOpex + rawMaterialCostUsdPerBblNetInput;// Util.ReadConfig(refineryTypeForConfig + "hydrocarbonLossValue" + yearForConfig);
                    float hcLossWtPcnt = GetDbValue(conxString, routines.LossMassPctnOfFeed());

                    Single temp = Util.LossValueVsTargetKDollars__2018_08_27(yearToUse, hcLossWtPcnt, opAvail, netInputBPD, utilPcnt,
                        lossTarget, rawMaterialCostUsdPerBblNetInput, grossMarginUsdPerBblNetInput, variableCashOpex);

                    //temp.MonthAndYear = monthToUse;
                    last24MonthsOfSomething.Add(temp); //.RefineryLossAnnualizedValueOfLossVersusTarget);
                     /*
                     last24MonthsOfRefineryLossValueOfLossForTheMonthVersusTargetLoss.Add(temp.RefineryLossValueOfLossForTheMonthVersusTargetLoss);
                     last24MonthsOfLOAVTValueOfLostOAForTheMonth.Add(temp.LOAVTValueOfLostOAForTheMonth);
                     */
                     /*
                     //get target - From UserDefined
                     DateTime tempDate = new DateTime(monthToUse.Year, monthToUse.Month, 1);
                     Queries routines = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                     string sql = routines.UserDefinedTarget("LossValueTarget", monthToUse);
                     DataRow dRow = dbHelper.GetFirstRow(sql);
                     WriteValue(ref sheet, startXlsRowNumber, 2, float.Parse(dRow[0].ToString()));
                     */

                     //get avg 
                     //throw new Exception("Need to calculate avearge. Try AvgFromRows()");
                     monthToUse = monthToUse.AddMonths(1);
                }
                List<float> valuesToRow = new List<float>();
                //CurrentMonth
                WriteString(ref sheet, startXlsRowNumber, 1, header + "CurrentMonth");
                valuesToRow.Add(last24MonthsOfSomething[last24MonthsOfSomething.Count - 1]);
                WriteValue(ref sheet, startXlsRowNumber, 2, valuesToRow[0]);
                startXlsRowNumber++;

                //CurrentYear
                WriteString(ref sheet, startXlsRowNumber, 1, header + "CurrentYear");
                //find the last January figure and add all after it to the list
                valuesToRow = LastJanuaryAndForward(last24MonthsOfSomething, startDate);
                /*
                currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss = LastJanuaryAndForward(last24MonthsOfRefineryLossValueOfLossForTheMonthVersusTargetLoss, new DateTime(startDate.Year, startDate.Month, 1));
                currentYearLOAVTValueOfLostOAForTheMonth = LastJanuaryAndForward(last24MonthsOfLOAVTValueOfLostOAForTheMonth, new DateTime(startDate.Year, startDate.Month, 1));
                */
                List <float> currentYear = valuesToRow;
                
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //LastCalendarYear: prior year from Jan to Dec
                WriteString(ref sheet, startXlsRowNumber, 1, header + "LastCalendarYear");
                valuesToRow = LastCalendarYear(last24MonthsOfSomething, startDate);
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //Last12Months
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Last12Months");
                valuesToRow = Last12Months(last24MonthsOfSomething, startDate);
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //Last24Months
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Last24Months");
                valuesToRow = Last24Months(last24MonthsOfSomething, startDate);
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //_YTD

                //Use last january and forward. 
                //for each month's value, multiply it by the # days in that month.
                //then sum all these.
                //Then divide by sum of count of all days in each month used.

                List<float> yTDs = new List<float>();
                float sumLossValueTimesDays = 0;
                int sumDays = 0;
                int monthCount = 1;
                //find the most recent January in last24MonthsOfSomething
                for (int count = 23 - startDate.Month + 1; count < 24; count++)
                {
                    DateTime tempDate = new DateTime(startDate.Year, monthCount, 1);
                    int numDays = numberOfDaysInThisMonth(monthCount, startDate.Year);
                    //multiply it by # days in that month
                    float temp = last24MonthsOfSomething[count] * numDays;
                    //add it to sumLossValueTimesDays
                    sumLossValueTimesDays += temp;
                    //add # days in that month to sumDays
                    sumDays += numDays;
                    //divide sumLossValueTimesDays by sumDays, store it for January YTD in yTDs.
                    yTDs.Add(sumLossValueTimesDays / sumDays);
                    //repeat above for Feb, etc.
                    monthCount++;
                }                
                WriteString(ref sheet, startXlsRowNumber, 1, header + "YTD");
                WriteListToRow(ref sheet, yTDs, ref startXlsRowNumber);
                
                //_Target
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Target");
                 //read all 12 from config instead - Use YTD from lossValueTargets
                int ordinal = startDate.Month;
                int col = 2;
                for(int month = targets.Count - ordinal; month < targets.Count;month++)
                {
                    WriteString(ref sheet, startXlsRowNumber, col, targets[month].ToString());
                    col++;
                }
                startXlsRowNumber++;
                //_Avg
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Average");

                //send in a list with all the values containing the data you need
                List<float> avgs = AvgFromList(currentYear);
                WriteListToRow(ref sheet, avgs, ref startXlsRowNumber);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool WriteOperationalAvailInfoByHeader(ref Excel.Worksheet sheet, string header, ref int startXlsRowNumber,
           string conxString, DateTime startDate, string refNum, string dataSet,
           int factorSet, string scenario, string currency, string uOM, ref List<Single> currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss,
           ref List<Single> currentYearLOAVTValueOfLostOAForTheMonth, bool isFuels)
        {
            try
            {
                //throw new Exception("change from daily to monthly basis??");
                List<float> last24MonthsOfSomething = new List<float>();
                DBHelper dbHelper = new DBHelper(conxString);
                // get longest oldest set of data
                //Last24Months
                DateTime monthToUse = startDate.AddMonths(-23);
                List<float> last24MonthsOfRefineryLossValueOfLossForTheMonthVersusTargetLoss = new List<float>();
                List<float> last24MonthsOfLOAVTValueOfLostOAForTheMonth = new List<float>();
                List<float> targets = new List<float>();
                for (int i = 0; i < 24; i++)
                {
                    Queries routines = new Queries(monthToUse, refNum, dataSet, factorSet, scenario, currency, uOM);
                    Single lossValueTargetForThisMonth = GetDbValue(conxString, routines.GetLossValueTargetSql());
                    //

                    /*
                    Common.LossValueInfo temp = Util.GetLossValue2(conxString, monthToUse, refNum, dataSet,
                    factorSet, scenario, currency, uOM, isFuels);
                    */

                    /*probably want to use  oaLostOpportunityValue__2018_08_24() instead 
                    Single temp = Util.GetOperationalAvailLostOpportunityValue(conxString, monthToUse, refNum, dataSet,
                    factorSet, scenario, currency, uOM, isFuels, lossValueTargetForThisMonth);
                    */

                    int yearToUse = startDate.Year;
                    float opAvail = GetDbValue(conxString, routines.GensumOpAvail());
                    float netInputBPD = GetDbValue(conxString, routines.GenSumThisMonth("NetInputBPD", factorSet, scenario, currency, uOM));
                    float utilPcnt = GetDbValue(conxString, routines.GenSumThisMonth("UtilPcnt", factorSet, scenario, currency, uOM));
                    
                    //need to repopulate these each time because year will differ from startDate arg:
                    string yearForConfig = monthToUse.Year.ToString();
                    if (yearForConfig == "2016" || yearForConfig == "2017")
                        yearForConfig = "20162017";
                    string refineryTypeForConfig = "Fuels";
                    if (!isFuels)
                        refineryTypeForConfig = "Lubes";
                    float rawMaterialCostUsdPerBblNetInput = Util.ReadConfig(refineryTypeForConfig + "rawMaterialCostUsdPerBblNetInput" + yearForConfig);
                    float grossMarginUsdPerBblNetInput = Util.ReadConfig(refineryTypeForConfig + "grossMarginUsdPerBblNetInput" + yearForConfig);
                    float oaTarget = Util.ReadConfig(refineryTypeForConfig + "oaTarget" + yearForConfig);
                    //float hydrocarbonLossValue = Util.ReadConfig(refineryTypeForConfig + "hydrocarbonLossValue" + yearForConfig);
                    float variableCashOpex = Util.ReadConfig(refineryTypeForConfig + "variableCashOpex" + yearForConfig);
                    float hcLossWtPcnt = GetDbValue(conxString, routines.LossMassPctnOfFeed());
                    targets.Add(oaTarget);

                    Single temp = Util.OALostOpportunityValue__2018_08_27(yearToUse, hcLossWtPcnt, opAvail, netInputBPD, utilPcnt, oaTarget,
                        rawMaterialCostUsdPerBblNetInput, grossMarginUsdPerBblNetInput, oaTarget, variableCashOpex);

                    last24MonthsOfSomething.Add(temp);
                    /*
                    temp.MonthAndYear = monthToUse;
                    last24MonthsOfSomething.Add(temp.RefineryLossAnnualizedValueOfLossVersusTarget);
                    last24MonthsOfRefineryLossValueOfLossForTheMonthVersusTargetLoss.Add(temp.RefineryLossValueOfLossForTheMonthVersusTargetLoss);
                    last24MonthsOfLOAVTValueOfLostOAForTheMonth.Add(temp.LOAVTValueOfLostOAForTheMonth);
                    */
                    /*
                    //get target - From UserDefined
                    DateTime tempDate = new DateTime(monthToUse.Year, monthToUse.Month, 1);
                    Queries routines = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                    string sql = routines.UserDefinedTarget("LossValueTarget", monthToUse);
                    DataRow dRow = dbHelper.GetFirstRow(sql);
                    WriteValue(ref sheet, startXlsRowNumber, 2, float.Parse(dRow[0].ToString()));
                    */

                    //get avg 
                    //throw new Exception("Need to calculate avearge. Try AvgFromRows()");
                    monthToUse = monthToUse.AddMonths(1);
                }
                List<float> valuesToRow = new List<float>();
                //CurrentMonth
                WriteString(ref sheet, startXlsRowNumber, 1, header + "CurrentMonth");
                valuesToRow.Add(last24MonthsOfSomething[last24MonthsOfSomething.Count - 1]);
                WriteValue(ref sheet, startXlsRowNumber, 2, valuesToRow[0]);
                startXlsRowNumber++;

                //CurrentYear
                WriteString(ref sheet, startXlsRowNumber, 1, header + "CurrentYear");
                //find the last January figure and add all after it to the list
                valuesToRow = LastJanuaryAndForward(last24MonthsOfSomething, startDate);
                /*
                currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss = LastJanuaryAndForward(last24MonthsOfRefineryLossValueOfLossForTheMonthVersusTargetLoss, new DateTime(startDate.Year, startDate.Month, 1));
                currentYearLOAVTValueOfLostOAForTheMonth = LastJanuaryAndForward(last24MonthsOfLOAVTValueOfLostOAForTheMonth, new DateTime(startDate.Year, startDate.Month, 1));
                */
                List<float> currentYear = valuesToRow;
                
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //LastCalendarYear: prior year from Jan to Dec
                WriteString(ref sheet, startXlsRowNumber, 1, header + "LastCalendarYear");
                valuesToRow = LastCalendarYear(last24MonthsOfSomething, startDate);
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //Last12Months
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Last12Months");
                valuesToRow = Last12Months(last24MonthsOfSomething, startDate);
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //Last24Months
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Last24Months");
                valuesToRow = Last24Months(last24MonthsOfSomething, startDate);
                WriteListToRow(ref sheet, valuesToRow, ref startXlsRowNumber);

                //_YTD

                //Use last january and forward. 
                //for each month's value, multiply it by the # days in that month.
                //then sum all these.
                //Then divide by sum of count of all days in each month used.

                List<float> yTDs = new List<float>();
                float sumOATimesDays = 0;
                int sumDays = 0;
                int monthCount = 1;
                //find the most recent January in last24MonthsOfSomething
                for (int count = 23 - startDate.Month + 1; count < 24; count++)
                {
                    DateTime tempDate = new DateTime(startDate.Year, monthCount, 1);
                    int numDays = numberOfDaysInThisMonth(monthCount, startDate.Year);
                    //multiply it by # days in that month
                    float temp = last24MonthsOfSomething[count] * numDays;
                    //add it to sumOATimesDays
                    sumOATimesDays += temp;
                    //add # days in that month to sumDays
                    sumDays += numDays;
                    //divide sumOATimesDays by sumDays, store it for January YTD in yTDs.
                    yTDs.Add(sumOATimesDays / sumDays);
                    //repeat above for Feb, etc.
                    monthCount++;
                }
                WriteString(ref sheet, startXlsRowNumber, 1, header + "YTD");
                WriteListToRow(ref sheet, yTDs, ref startXlsRowNumber);


                //_Target
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Target");
                //read all 12 from config instead - Use YTD from lossValueTargets
                int ordinal = startDate.Month;
                int col = 2;
                for (int month = targets.Count - ordinal; month < targets.Count; month++)
                {
                    WriteString(ref sheet, startXlsRowNumber, col, targets[month].ToString());
                    col++;
                }
                startXlsRowNumber++;

                //_Avg
                WriteString(ref sheet, startXlsRowNumber, 1, header + "Average");

                //send in a list with all the values containing the data you need
                List<float> avgs = AvgFromList(currentYear);
                WriteListToRow(ref sheet, avgs, ref startXlsRowNumber);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Write24MonthTargets(ref Excel.Worksheet sheet, ref int xlrow,
            string tableName, string fieldName,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {
                List<float> last24Months = GetLast24TargetsFromTable(tableName, fieldName, conxString,
                    startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                WriteString(ref sheet, xlrow, 1, fieldName + "Last24Targets");
                WriteListToRow(ref sheet, last24Months, ref xlrow);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool Write24MonthUserDefinedTargets(ref Excel.Worksheet sheet, ref int xlrow,
            string headerText,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {
                List<float> last24Months = GetLast24TargetsFromUserDefined(headerText, conxString, startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                WriteString(ref sheet, xlrow, 1, headerText + "Last24Targets");
                WriteListToRow(ref sheet, last24Months, ref xlrow);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<float> GetLast24TargetsFromTable(string tableName, string fieldName,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            List<float> last24Months = new List<float>();
            List<float> targets = new List<float>();
            DBHelper dbHelper = new DBHelper(conxString);
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            string sql = queries.GetLast23TargetsFromTable(tableName, fieldName);
            DataTable dTable = dbHelper.GetDataTable(sql);
            if (dTable != null && dTable.Rows.Count > 0)
            {
                int missingRows = 24 - dTable.Rows.Count;
                while (missingRows > 0)
                {
                    //pad with zeroes
                    last24Months.Add(0);
                    missingRows--;
                }
                foreach (DataRow row in dTable.Rows)
                {
                    float valu = 0;
                    try
                    {
                        valu = float.Parse(row[0].ToString());
                    }
                    catch
                    {
                        valu = 0;
                    }
                    last24Months.Add(valu);
                }
            }
            return last24Months;
        }
        public static List<float> GetLast24TargetsFromUserDefined(string headerText,
            string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            List<float> last24Months = new List<float>();
            List<float> targets = new List<float>();
            DBHelper dbHelper = new DBHelper(conxString);
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            string sql = queries.UserDefinedLast23Targets(headerText);
            DataTable dTable = dbHelper.GetDataTable(sql);

            if (dTable != null)
            {            
                if (dTable.Rows.Count > 0)
                {
                    int missingRows = 24 - dTable.Rows.Count;
                    while (missingRows > 0)
                    {
                        //pad with zeroes
                        last24Months.Add(0);
                        missingRows--;
                    }
                    foreach (DataRow row in dTable.Rows)
                    {
                        float valu = 0;
                        try
                        {
                            valu = float.Parse(row[0].ToString());
                        }
                        catch
                        {
                            valu = 0;
                        }
                        last24Months.Add(valu);
                    }
                }
            }
            else
            {
                last24Months = new List<float>() { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            }
          
            return last24Months;
        }

        private static List<Single> AvgFromRows(DataTable dtable, string fieldName)
        {//use where have multiple values on diff datarows
            List<Single> results = new List<Single>();
            Single sum = 0;
            try
            {
                if (dtable != null && dtable.Rows.Count > 0)
                {
                    foreach (DataRow row in dtable.Rows)
                    {
                        if (row[fieldName] != DBNull.Value)
                        {
                            Single val = Single.Parse(row[fieldName].ToString());
                            //Single val = (Single)row[fieldName];
                            sum += val;
                            int denominator = results.Count + 1;
                            Single avg = sum / denominator;
                            if (avg.ToString() == "NaN")
                                avg = 0;
                            results.Add(avg);
                        }
                        else
                        {
                            Single val = 0;
                            sum += val;
                            Single avg = sum / results.Count;
                            results.Add(avg);
                        }
                    }
                }
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<float> AvgFromList(List<float> listIn)
        {
            List<float> results = new List<float>();
            float sum = 0;
            try
            {
                if (listIn != null && listIn.Count > 0)
                {
                    foreach (float valu in listIn)
                    {
                        sum += valu;
                        int denominator = results.Count + 1;
                        Single avg = sum / denominator;
                        if (avg.ToString() == "NaN")
                            avg = 0;
                        results.Add(avg);
                    }
                }
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<float> AvgFromList_20180608(List<float> listIn)
        {
            //Do it this way per Bruce 2018-06-08
            /*
            It needs to be weighted by the number of days in the period.  
            the value reported each month is annualized (scaled by Days in Year/Days in Month).  
            so a YTD average has to be 
            Sumproduct(Annualized Loss Value for Month, Days in Month)/sum(Days in Month), summing over the months to date in the year.
            */

            //SumProduct = 
            List<float> results = new List<float>();
            float sum = 0;
            try
            {

                //if (listIn != null && listIn.Count > 0)
                //{
                //    foreach (float valu in listIn)
                //    {
                //        sum += valu;
                //        int denominator = results.Count + 1;
                //        Single avg = sum / denominator;
                //        if (avg.ToString() == "NaN")
                //            avg = 0;
                //        results.Add(avg);
                //    }
                //}
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<float> LastJanuaryAndForward(List<float> list, DateTime submissionMonthAndYear)
        {
            List<float> result = new List<float>();
            //if submission month is January, then only return that.
            if (submissionMonthAndYear.Month == 1)
            {
                result.Add(list[list.Count - 1]);
                return list;
            }
            int i = 0;
            for (i = 0; i > -12; i--)
            {
                DateTime temp = submissionMonthAndYear.AddMonths(i);
                if (temp.Month == 1)
                    break;
            }

            for (int j = list.Count - 1 + i; j < list.Count; j++)
            {
                result.Add(list[j]);
            }
            return result;
        }

        public static List<float> LastCalendarYear(List<float> list, DateTime submissionMonthAndYear)
        {
            List<float> result = new List<float>();
            DateTime twoJanuariesAgo = new DateTime(submissionMonthAndYear.Year - 1, 1, 1); //The January of the month for current year's numbers
            DateTime lastDecember = new DateTime(twoJanuariesAgo.Year, 12, 1);
            int i = 0;
            for (i = 0; i < 24; i--)
            {
                DateTime temp = submissionMonthAndYear.AddMonths(i);
                if (temp.Month == twoJanuariesAgo.Month && temp.Year == twoJanuariesAgo.Year)
                    break;
            }
            int monthCount = 1;
            for (int j = list.Count - 1 + i; j <= list.Count; j++)
            {
                if (monthCount > 12)
                    break;
                result.Add(list[j]);
                monthCount++;
            }
            return result; // { 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111 };
        }

        public static List<float> Last12Months(List<float> list, DateTime submissionMonthAndYear)
        {
            List<float> result = new List<float>();
            for (int i = list.Count - 11 - 1; i <= list.Count - 1; i++)
            {
                result.Add(list[i]);
            }
            return result; // { 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111 };
        }

        public static List<float> Last24Months(List<float> list, DateTime submissionMonthAndYear)
        {
            List<float> result = new List<float>();
            for (int i = list.Count - 23 - 1; i <= list.Count - 1; i++)
            {
                result.Add(list[i]);
            }
            return result; // { 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111 };
        }

        public static bool WriteListToRow(ref Excel.Worksheet sheet, List<float> lst, ref int xlrow)
        {
            try
            {
                if (lst != null)
                {
                    int c = 2;
                    for (int lrow = 0; lrow < lst.Count; lrow++)
                    {
                        WriteValue(ref sheet, xlrow, (c + lrow), lst[lrow]);
                    }
                }
                xlrow++;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static bool WriteRows(ref Excel.Worksheet sheet, DataTable dTable, int xlrow, int minRows = 0)
        {   //use when your query returns multipoe rows that you want to write horizontally

            //use minRows to pad missing data with 0s

            try
            {
                int pad = 0;
                int c = 2;
                if (minRows > 0)
                {
                    if (dTable == null)
                    {
                        pad = minRows;
                    }
                    else
                    {
                        pad = minRows - dTable.Rows.Count;
                    }
                }
                while (pad > 0)
                {
                    WriteString(ref sheet, xlrow, c, "0");
                    pad -= 1;
                    c += 1;
                }

                if (dTable != null)
                {
                    for (int drow = 0; drow < dTable.Rows.Count; drow++)
                    {
                        WriteString(ref sheet, xlrow, c + drow, dTable.Rows[drow][0].ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static bool WriteString(ref Excel.Worksheet sheet, int row, int col, string val)
        {
            try
            {
                sheet.Cells[row, col].Value = val;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }

        private static bool WriteValue(ref Excel.Worksheet sheet, int row, int col, float val)
        {
            try
            {
                sheet.Cells[row, col].Value = val;
                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Write(): " + ex.Message);
            }
        }

        public static bool WriteLostTimeLastCalendarYear(ref Excel.Worksheet net, ref int xlrow,
            DBHelper dbHelper, string userDefinedHeader, DateTime startDate,
            string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {
                string label = string.Empty;
                List<InjuryInfo> results = new List<InjuryInfo>();
                DateTime twoJanuariesAgo = new DateTime(startDate.Year, 1, 1).AddMonths(-12);

                for (int i = 0; i < 12; i++)
                {
                    DateTime tempMonth = twoJanuariesAgo.AddMonths(i);
                    //this get the YTD for the requested month.
                    List<InjuryInfo> tempInfo = GetLostTimeThisMonth(dbHelper, userDefinedHeader, tempMonth, refNum, dataSet, factorSet, scenario, currency, uOM);
                    tempInfo[0].MonthDataIsFor = tempMonth.Month;
                    tempInfo[0].YearDataIsFor = tempMonth.Year;
                    results.Add(tempInfo[0]);
                }
                //===================================

                //LastCalendarYear
                int col = 1;
                xlrow++;
                DateTime mostRecentJanuary = new DateTime(startDate.Year, 1, 1);
                label = "LostTimeInjuryRateLastCalendarYear";
                WriteString(ref net, xlrow, col, label);
                col++;

                for (int i = 0; i < 12; i++)
                {
                    WriteValue(ref net, xlrow, col, results[i].LostTimeInjuryRateForMonth);
                    col++;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool WriteLostTime(ref Excel.Worksheet net, ref int xlrow,
            DBHelper dbHelper, string userDefinedHeader, DateTime startDate,
            string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {
                string label = string.Empty;
                List<InjuryInfo> results = new List<InjuryInfo>();
                //Queries queries = new Queries(startDate, refNum,dataSet, factorSet, scenario, currency, uOM);

                //##########    RUN GetLostTimeThisMonth() for each of 24 months, add all to List,
                //########### and then go thru the list to populate the 
                //              current yr , last yr, last 12, last 24, YTD etc.
                //              so the sql needs to include the periodStart too.
                DateTime twentyThreeMonthsAgo = startDate.AddMonths(-23); //23 instead of 24 so include current month

                for (int i = 0; i <= 23; i++)
                {
                    DateTime tempMonth = twentyThreeMonthsAgo.AddMonths(i);
                    //this get the YTD for the requested month.
                    List<InjuryInfo> tempInfo = GetLostTimeThisMonth(dbHelper, userDefinedHeader, tempMonth, refNum, dataSet, factorSet, scenario, currency, uOM);
                    tempInfo[0].MonthDataIsFor = tempMonth.Month;
                    tempInfo[0].YearDataIsFor = tempMonth.Year;
                    results.Add(tempInfo[0]);
                }

                //===================================
                //Current Month Only
                //results = GetLostTimeThisMonth(dbHelper, userDefinedHeader, startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                WriteString(ref net, xlrow, 1, "LostTimeInjuryRateForMonthCurrentMonth");
                WriteValue(ref net, xlrow, 2, results[23].LostTimeInjuryRateForMonth);
                xlrow++;
                WriteString(ref net, xlrow, 1, "LostTimeInjuryRateForMonthYTD");
                WriteValue(ref net, xlrow, 2, results[23].LostTimeInjuryRateYTD);
                xlrow++;

                //CurrentYear
                int col = 1;
                DateTime mostRecentJanuary = new DateTime(startDate.Year, 1, 1);
                label = "LostTimeInjuryRateForCurrentYear";
                WriteString(ref net, xlrow, col, label);
                col++;
                //string sql = queries.UserDefinedCurrentYear(userDefinedHeader);
                //results = GetLostTime(dbHelper,userDefinedHeader,sql,sqlYTD, mostRecentJanuary, startDate, refNum,
                //    dataSet, factorSet, scenario, currency, uOM);
                for (int i = 0; i <= 23; i++) //foreach (InjuryInfo info in results)
                {
                    DateTime compareDate = new DateTime(results[i].YearDataIsFor, results[i].MonthDataIsFor, 1);
                    if (compareDate >= mostRecentJanuary)
                    {
                        WriteValue(ref net, xlrow, col, results[i].LostTimeInjuryRateForMonth);
                        col++;
                    }
                }

                //CuurrentYearYTD
                col = 1;
                xlrow++;
                label = "LostTimeInjuryRateForCurrentYearYTD";
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    DateTime compareDate = new DateTime(results[i].YearDataIsFor, results[i].MonthDataIsFor, 1);
                    if (compareDate >= mostRecentJanuary)
                    {
                        WriteValue(ref net, xlrow, col, results[i].LostTimeInjuryRateYTD);
                        col++;
                    }
                }

                //Last12Months
                col = 1;
                xlrow++;
                label = "LostTimeInjuryRateLast12Months";
                DateTime twelveMonthsAgo = startDate.AddMonths(-11); //11 instead of 12 so include current month
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    DateTime compareDate = new DateTime(results[i].YearDataIsFor, results[i].MonthDataIsFor, 1);
                    if (compareDate >= twelveMonthsAgo)
                    {
                        WriteValue(ref net, xlrow, col, results[i].LostTimeInjuryRateForMonth);
                        col++;
                    }
                }

                //Last12MonthsYTD
                col = 1;
                xlrow++;
                label = "LostTimeInjuryRateLast12MonthsYTD";
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    DateTime compareDate = new DateTime(results[i].YearDataIsFor, results[i].MonthDataIsFor, 1);
                    if (compareDate >= twelveMonthsAgo)
                    {
                        WriteValue(ref net, xlrow, col, results[i].LostTimeInjuryRateYTD);
                        col++;
                    }
                }

                //Last24Months
                col = 1;
                xlrow++;
                label = "LostTimeInjuryRateLast24Months";
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    WriteValue(ref net, xlrow, col, results[i].LostTimeInjuryRateForMonth);
                    col++;
                }

                //Last24MonthsYTD
                col = 1;
                xlrow++;
                label = "LostTimeInjuryRateLast24MonthsYTD";
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    WriteValue(ref net, xlrow, col, results[i].LostTimeInjuryRateYTD);
                    col++;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool WriteEnviroIncidents(ref Excel.Worksheet net, ref int xlrow,
            DBHelper dbHelper, string userDefinedHeader, DateTime startDate,
            string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            try
            {
                string label = string.Empty;
                List<EnviroInfo> results = new List<EnviroInfo>();
                //Queries queries = new Queries(startDate, refNum,dataSet, factorSet, scenario, currency, uOM);

                //##########    Get for each of 24 months, add all to List,
                //########### and then go thru the list to populate the 
                //              current yr , last yr, last 12, last 24, YTD etc.
                //              so the sql needs to include the periodStart too.
                DateTime twentyThreeMonthsAgo = startDate.AddMonths(-23); //23 instead of 24 so include current month

                for (int i = 0; i <= 23; i++)
                {
                    DateTime tempMonth = twentyThreeMonthsAgo.AddMonths(i);
                    //this get the YTD for the requested month.
                    List<EnviroInfo> tempInfo = GetEnviroInfoThisMonth(dbHelper, userDefinedHeader, tempMonth, refNum, dataSet, factorSet, scenario, currency, uOM);
                    tempInfo[0].MonthDataIsFor = tempMonth.Month;
                    tempInfo[0].YearDataIsFor = tempMonth.Year;
                    results.Add(tempInfo[0]);
                }

                //===================================
                //Current Month Only
                //results = GetLostTimeThisMonth(dbHelper, userDefinedHeader, startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                WriteString(ref net, xlrow, 1, "EnviroIncidentRateForMonthCurrentMonth");
                WriteValue(ref net, xlrow, 2, results[23].EnviroIncidentRateForMonth);
                xlrow++;
                WriteString(ref net, xlrow, 1, "EnviroIncidentRateForMonthYTD");
                WriteValue(ref net, xlrow, 2, results[23].EnviroIncidentRateYTD);
                xlrow++;

                //CurrentYear
                int col = 1;
                DateTime mostRecentJanuary = new DateTime(startDate.Year, 1, 1);
                label = "EnviroIncidentRateForCurrentYear";
                WriteString(ref net, xlrow, col, label);
                col++;
                //string sql = queries.UserDefinedCurrentYear(userDefinedHeader);
                //results = GetLostTime(dbHelper,userDefinedHeader,sql,sqlYTD, mostRecentJanuary, startDate, refNum,
                //    dataSet, factorSet, scenario, currency, uOM);
                for (int i = 0; i <= 23; i++) //foreach (InjuryInfo info in results)
                {
                    DateTime compareDate = new DateTime(results[i].YearDataIsFor, results[i].MonthDataIsFor, 1);
                    if (compareDate >= mostRecentJanuary)
                    {
                        WriteValue(ref net, xlrow, col, results[i].EnviroIncidentRateForMonth);
                        col++;
                    }
                }

                //CuurrentYearYTD
                col = 1;
                xlrow++;
                label = "EnviroIncidentRateForCurrentYearYTD";
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    DateTime compareDate = new DateTime(results[i].YearDataIsFor, results[i].MonthDataIsFor, 1);
                    if (compareDate >= mostRecentJanuary)
                    {
                        WriteValue(ref net, xlrow, col, results[i].EnviroIncidentRateYTD);
                        col++;
                    }
                }

                //Last12Months
                col = 1;
                xlrow++;
                label = "EnviroIncidentRateLast12Months";
                DateTime twelveMonthsAgo = startDate.AddMonths(-11); //11 instead of 12 so include current month
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    DateTime compareDate = new DateTime(results[i].YearDataIsFor, results[i].MonthDataIsFor, 1);
                    if (compareDate >= twelveMonthsAgo)
                    {
                        WriteValue(ref net, xlrow, col, results[i].EnviroIncidentRateForMonth);
                        col++;
                    }
                }

                //Last12MonthsYTD
                col = 1;
                xlrow++;
                label = "EnviroIncidentRateLast12MonthsYTD";
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    DateTime compareDate = new DateTime(results[i].YearDataIsFor, results[i].MonthDataIsFor, 1);
                    if (compareDate >= twelveMonthsAgo)
                    {
                        WriteValue(ref net, xlrow, col, results[i].EnviroIncidentRateYTD);
                        col++;
                    }
                }

                //Last24Months
                col = 1;
                xlrow++;
                label = "EnviroIncidentRateLast24Months";
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    WriteValue(ref net, xlrow, col, results[i].EnviroIncidentRateForMonth);
                    col++;
                }

                //Last24MonthsYTD
                col = 1;
                xlrow++;
                label = "EnviroIncidentRateLast24MonthsYTD";
                WriteString(ref net, xlrow, col, label);
                col++;
                for (int i = 0; i <= 23; i++)
                {
                    WriteValue(ref net, xlrow, col, results[i].EnviroIncidentRateYTD);
                    col++;
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static List<InjuryInfo> GetLostTimeThisMonth(DBHelper dbHelper,
            string userDefinedHeader, DateTime startDate,
            string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            List<InjuryInfo> results = new List<InjuryInfo>();
            try
            {
                DateTime dateBeingProcessed = startDate;

                string sql = "";
                InjuryInfo temp = null;

                //CurrentMonth
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                sql = queries.UserDefinedThisMonth(userDefinedHeader);
                temp = GetLostTimeForMonth(dbHelper, dateBeingProcessed, refNum, dataSet, factorSet, scenario, currency, uOM);
                temp.MonthDataIsFor = dateBeingProcessed.Month;
                temp.YearDataIsFor = dateBeingProcessed.Year;
                results.Add(temp);
                return results;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static List<EnviroInfo> GetEnviroInfoThisMonth(DBHelper dbHelper,
            string userDefinedHeader, DateTime startDate,
            string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            List<EnviroInfo> results = new List<EnviroInfo>();
            try
            {
                DateTime dateBeingProcessed = startDate;

                string sql = "";
                EnviroInfo temp = null;

                //CurrentMonth
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                sql = queries.UserDefinedThisMonth(userDefinedHeader);
                temp = GetEnviroIncidentsForMonth(dbHelper, dateBeingProcessed, refNum, dataSet, factorSet, scenario, currency, uOM);
                temp.MonthDataIsFor = dateBeingProcessed.Month;
                temp.YearDataIsFor = dateBeingProcessed.Year;
                results.Add(temp);
                return results;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }


        /*
        private static List<InjuryInfo> GetLostTimeForMonths(DBHelper dbHelper,
            string userDefinedHeader, string sql, string sqlYTD,  DateTime startDate, 
            DateTime endDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            List<InjuryInfo> results = new List<InjuryInfo>();
            try
            {
                DataTable dTable = dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count > 0)
                {
                    foreach (DataRow row in dTable.Rows)
                    {
                        InjuryInfo info = new InjuryInfo();
                        Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                        sql = queries.GetTotalPersonnelWorkoursForMonthSql();
                        info.TotalPersonnelWorkhoursForMonth = GetFloatDbValue(dbHelper, sql);

                        sql = queries.GetTotalPersonnelWorkoursYtdSql();
                        info.TotalPersonnelWorkhoursYTD = GetFloatDbValue(dbHelper, sql);

                        //GetTotalLostTimeInjuriesForMonth
                        sql = sqlForMonth;// queries.CurrentMonthLostTimeInjuriesSql(); 
                        info.TotalIncididentsForMonth = GetFloatDbValue(dbHelper, sql);

                        ////GetTotalLostTimeInjuriesYTD
                        sql = sqlForYtd;// queries.CurrentYearLostTimeInjuriesSql();
                        DataTable dTable = dbHelper.GetDataTable(sql);
                        if (dTable != null)
                        {
                            float temp = 0;
                            foreach (DataRow row in dTable.Rows)
                            {
                                temp += float.Parse(row[0].ToString());
                            }

                            info.TotalIncididentsYTD = temp; // GetFloatDbValue(dbHelper, sql);
                        }
                        info.LostTimeInjuryRateForMonth = info.TotalIncididentsForMonth / info.TotalPersonnelWorkhoursForMonth * 200000;
                        info.LostTimeInjuryRateYTD = info.TotalIncididentsYTD / info.TotalPersonnelWorkhoursYTD;
                        //temp.MonthDataIsFor = dateBeingProcessed.Month;
                        //temp.YearDataIsFor = dateBeingProcessed.Year;
                        results.Add(info);
                    }
                }
                
                //DateTime dateBeingProcessed = startDate;
                //while(dateBeingProcessed <= endDate)
                //{
                //    InjuryInfo temp = null;                    
                //    Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);

                //    temp = GetLostTimeForMonth( sql,
                //        sqlYTD, dbHelper,
                //        dateBeingProcessed, refNum, dataSet, factorSet, scenario, currency, uOM);
                //    temp.MonthDataIsFor = dateBeingProcessed.Month;
                //    temp.YearDataIsFor = dateBeingProcessed.Year;
                //    results.Add(temp);
                //    dateBeingProcessed = dateBeingProcessed.AddMonths(1);
                //}
                
                return results;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
*/

        private static InjuryInfo GetLostTimeForMonth(DBHelper dbHelper,
            DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            //Month: : Lost Time Injuries for the month/Total Personnel Workhours for the month x 200,0000.  
            //YTD:: Total Lost Time Injuries YTD / Total Workhours YTD
            string userDefinedHeader = "SafetyLostTimeIncidents";
            InjuryInfo info = new InjuryInfo();
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            String sql = queries.GetTotalPersonnelWorkoursForMonthSql();
            info.TotalPersonnelWorkhoursForMonth = GetFloatDbValue(dbHelper, sql);

            sql = queries.GetTotalPersonnelWorkoursYtdSql();
            info.TotalPersonnelWorkhoursYTD = GetFloatDbValue(dbHelper, sql);

            //GetTotalLostTimeInjuriesForMonth
            sql = queries.CurrentMonthUserDefinedSql(userDefinedHeader);
            info.TotalIncididentsForMonth = GetFloatDbValue(dbHelper, sql);

            ////GetTotalLostTimeInjuriesYTD
            sql = queries.CurrentYearUserDefinedSql(userDefinedHeader);
            DataTable dTable = dbHelper.GetDataTable(sql);
            if (dTable != null)
            {
                float temp = 0;
                foreach (DataRow row in dTable.Rows)
                {
                    if (row[0] != DBNull.Value)
                        temp += float.Parse(row[0].ToString());
                }

                info.TotalIncididentsYTD = temp; // GetFloatDbValue(dbHelper, sql);
            }
            info.LostTimeInjuryRateForMonth = info.TotalIncididentsForMonth / info.TotalPersonnelWorkhoursForMonth * 200000;
            info.LostTimeInjuryRateYTD = info.TotalIncididentsYTD / info.TotalPersonnelWorkhoursYTD;
            return info;
        }

        private static EnviroInfo GetEnviroIncidentsForMonth(DBHelper dbHelper,
            DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            //Month: : Environmental Events for the month/Total Personnel Workhours for the month x 200,0000.    
            //YTD:: Total Environmental Incidents YTD/Total Workhours YTD
            string userDefinedHeader = "ReportableEnvironmentalIncidents";
            EnviroInfo info = new EnviroInfo();
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            String sql = queries.GetTotalPersonnelWorkoursForMonthSql();
            info.TotalPersonnelWorkhoursForMonth = GetFloatDbValue(dbHelper, sql);

            sql = queries.GetTotalPersonnelWorkoursYtdSql();
            info.TotalPersonnelWorkhoursYTD = GetFloatDbValue(dbHelper, sql);

            //GetTotalLostTimeInjuriesForMonth
            sql = queries.CurrentMonthUserDefinedSql(userDefinedHeader);
            info.TotalEnviroEventsForMonth = GetFloatDbValue(dbHelper, sql);

            ////GetTotalLostTimeInjuriesYTD
            sql = queries.CurrentYearUserDefinedSql(userDefinedHeader);
            DataTable dTable = dbHelper.GetDataTable(sql);
            if (dTable != null)
            {
                float temp = 0;
                foreach (DataRow row in dTable.Rows)
                {
                    if (row[0] != DBNull.Value)
                        temp += float.Parse(row[0].ToString());
                }

                info.TotalEnviroEventsYTD = temp; // GetFloatDbValue(dbHelper, sql);
            }
            info.EnviroIncidentRateForMonth = info.TotalEnviroEventsForMonth / info.TotalPersonnelWorkhoursForMonth * 200000;
            info.EnviroIncidentRateYTD = info.TotalEnviroEventsYTD / info.TotalPersonnelWorkhoursYTD;
            return info;
        }

        public static EnviroInfo GetEnviroInfo(DBHelper dbHelper, DateTime startDate, string refNum,
            string dataSet, int factorSet, string scenario, string currency, string uOM)
        {
            EnviroInfo info = new EnviroInfo();
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            String sql = queries.GetTotalPersonnelWorkoursForMonthSql();
            info.TotalPersonnelWorkhoursForMonth = GetFloatDbValue(dbHelper, sql);

            sql = queries.GetTotalPersonnelWorkoursYtdSql();
            info.TotalPersonnelWorkhoursYTD = GetFloatDbValue(dbHelper, sql);

            sql = queries.CurrentMonthEnviroEventsSql();
            info.TotalEnviroEventsForMonth = GetFloatDbValue(dbHelper, sql);

            //sql = queries.CurrentYearEnviroEventsSql();
            //info.TotalEnviroEventsYTD = GetLongDbValue(dbHelper, sql);
            sql = queries.CurrentYearEnviroEventsSql();
            DataTable dTable = dbHelper.GetDataTable(sql);
            if (dTable != null)
            {
                float temp = 0;
                foreach (DataRow row in dTable.Rows)
                {
                    temp += float.Parse(row[0].ToString());
                }

                info.TotalEnviroEventsYTD = temp; // GetFloatDbValue(dbHelper, sql);
            }

            info.EnviroIncidentRateForMonth = info.TotalEnviroEventsForMonth / info.TotalPersonnelWorkhoursForMonth * 200000;
            info.EnviroIncidentRateYTD = info.TotalEnviroEventsYTD / info.TotalPersonnelWorkhoursYTD;
            return info;
        }

        public static LossValueInfo GetLossValue9(string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM, bool isFuels)
        {
            try
            {
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                int daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);
                int daysInYear = 0;
                for (int i = 1; i < 13; i++)
                {
                    daysInYear += DateTime.DaysInMonth(startDate.Year, i);
                }
                Single avgRawMatCost = GetLongDbValue(conxString, queries.UserDefinedRptValue("AvgRawMatCost"));  //input form RefLevelInput G57
                Single avgVariableCashOpEx = GetLongDbValue(conxString, queries.UserDefinedRptValue("AvgVariableCashOpEx")); //input form RefLevelInput G58
                Single avgVariableNetCashMargin = GetLongDbValue(conxString, queries.UserDefinedRptValue("AvgVariableNetCashMargin")); //input form RefLevelInput G59
                
                //need to see if lubes, and if so, use LubesLostOpportunityTarget , LubesOperationalAvailabilityTarget, etc.
                Single tgtHydrocarbonLossPcntOfFeed = 0; // Single.Parse(ConfigurationManager.AppSettings["FuelsLostOpportunityTarget"].ToString());
                Single tgtOperationalAvailability = 0;// Single.Parse(ConfigurationManager.AppSettings["FuelsOperationalAvailabilityTarget"].ToString());
                if (isFuels)
                {
                     tgtHydrocarbonLossPcntOfFeed = Single.Parse(ConfigurationManager.AppSettings["FuelsLostOpportunityTarget"].ToString());
                     tgtOperationalAvailability = Single.Parse(ConfigurationManager.AppSettings["FuelsOperationalAvailabilityTarget"].ToString());
                }
                else
                {
                    tgtHydrocarbonLossPcntOfFeed = Single.Parse(ConfigurationManager.AppSettings["LubesLostOpportunityTarget"].ToString());
                    tgtOperationalAvailability = Single.Parse(ConfigurationManager.AppSettings["LubesOperationalAvailabilityTarget"].ToString());
                }
                Single temp = GetDbValue(conxString, queries.UserDefinedRptValue("RawMatInputBblPerMoTot"));
                long rawMatInputBblPerMoTot =long.Parse( Math.Round(temp, 0).ToString());
                Single rawMatInputkBPD = rawMatInputBblPerMoTot / daysInMonth;
                //long rawMatInputBblPerMoTot = GetLongDbValue(conxString, queries.UserDefinedRptValue("RawMatInputBblPerMoTot"));

                temp = GetDbValue(conxString, queries.UserDefinedRptValue("CrudeChargeDetailsBblPerMo"));
                long crudeChargeDetailsBblPerMo =  long.Parse(Math.Round(temp, 0).ToString());  // GetLongDbValue(conxString, queries.UserDefinedRptValue("CrudeChargeDetailsBblPerMo"));
                Single refineryNetMaterialInput = rawMatInputkBPD + crudeChargeDetailsBblPerMo;

                Single utilization = GetDbValue(conxString, queries.GensumUtilPcnt());
                Single operationalAvailability = GetDbValue(conxString, queries.GensumOpAvail());
                Single lossMassPctnOfFeed = GetDbValue(conxString, queries.LossMassPctnOfFeed());
                LossValueInfo result = LossValueInfoCalc(daysInYear, daysInMonth, avgRawMatCost, avgVariableCashOpEx, avgVariableNetCashMargin, tgtHydrocarbonLossPcntOfFeed, tgtOperationalAvailability,
                    refineryNetMaterialInput, utilization, operationalAvailability, lossMassPctnOfFeed);

                //Single currentMonthoperationalAvailability    now a param
                //Single currentMonthLossMassPctnOfFeed  now a param
                //Single currentMonthValuesTonne = 0;  doesn't seem to be used
                //Single currentMonthValuesUtilization = 0;  now a param

                return result;

            }
            catch (Exception ex)
            { throw ex; }
        }

        public static Single GetLossValueVsTargetkDollars(string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM, bool isFuels, Single lossValueTarget)
        {
            try
            {
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);                
                Single totalNetRawMaterialInput = GetDbValue(conxString, queries.GetLossValueTotalNetRawMaterialInputSql());
                Single finishedProdAddit = GetDbValue(conxString, queries.GetLossValueFinishedProdAdditSql());
                Single subtotalVolumeRelatedExpense = GetDbValue(conxString, queries.GetLossValueSubtotalVolumeRelatedExpenseSql());
                Single rawMaterialCost = GetDbValue(conxString, queries.GetLossValueRawMaterialCostSql());
                Single lossValueDollarsPerBbl = ((subtotalVolumeRelatedExpense * 1000) / (totalNetRawMaterialInput - finishedProdAddit)) + rawMaterialCost;
                Single rawMaterialInputkBPD = GetDbValue(conxString, queries.GetLossValueRawMaterialInputkBPDSql());
                int numDaysInYr = NumberOfDaysInThisYear(startDate.Year);
                Single lossWeightPcnt = GetLossWtPcnt(conxString,startDate,refNum,dataSet,factorSet,scenario,currency,uOM,isFuels);
                Single lossValueVsTargetkDollars = lossValueDollarsPerBbl * (lossWeightPcnt - lossValueTarget) / 100 * rawMaterialInputkBPD * 1000 * numDaysInYr / 1000;
                //lossValueTarget
                /*
                Loss Value vs Target (k$) =

                Loss Value ($/Bbl) * 
                (Loss (wt%)  - Loss Target)
                /100*
                Raw Material Input (kBPD)  [select g.NetInputBPD from dbo.GenSum g where SubmissionID = 12089] *
                1000*
                (# days in this year)/1000
                */
                return lossValueVsTargetkDollars;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public static Single GetLossWtPcnt(string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM, bool isFuels)
        {
            Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
            Single lossValue = GetDbValue(conxString, queries.GetLossValueSql());   
            Single barrelsPerDay = GetDbValue(conxString, queries.GetLossValueBarrelsPerDay());
            int numDaysInThisMonth = Util.numberOfDaysInThisMonth(startDate.Month, startDate.Year);
            return lossValue / numDaysInThisMonth / (barrelsPerDay * 1000 * .137f) * 100;
            /*
             Loss (wt%)
            DECLARE @lossValue float;
            declare @bpd float;
            set @lossValue = (select RptValue from dbo.UserDefined where HeaderText = 'Loss' and SubmissionID = 12089);
            set @bpd  = (select  TOP 1 NetInputBPD from dbo.GenSum where SubmissionID = 12089)
            --SELECT (t.Loss/ gs.DaysInPeriod / (gs.NetInputBPD * 1000 * .137))*100 AS LossPcnt
            --SELECT (6060.08891113528/ 28 / (292.1192 * 1000 * .137))*100 AS LossPcnt
            select @lossValue / 28 / (@bpd * 1000 * .137) * 100
            0.541

             * */
        }

        private static Single GetOperationalAvailLostOpportunityValueDollarsPerBbl(string conxString, DateTime startDate, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM, bool isFuels)
        {
            try
            {
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);
                //47.83
                Single gPV = GetDbValue(conxString, queries.GetGpv2016Sql());
                //Raw Material Cost, USD/bbl Net Input 40.44
                Single rawMaterialCost = GetDbValue(conxString, queries.GetLossValueRawMaterialCostSql());
                //1.54 =  (from other calc: Subtotal Volume-Related Expenses) * 100)/ (From other calc: Total Net Raw Material Input + Finished Prod. Addit.)
                //149685
                Single subtotalVolumeRelatedExpense = GetDbValue(conxString, queries.GetLossValueSubtotalVolumeRelatedExpenseSql());
                //97480080.43
                Single totalNetRawMaterialInput = GetDbValue(conxString, queries.GetLossValueTotalNetRawMaterialInputSql());
                //2750
                Single finishedProdAddit = GetDbValue(conxString, queries.GetLossValueFinishedProdAdditSql());                
                Single c23 = gPV - rawMaterialCost;                
                Single c26 = subtotalVolumeRelatedExpense * 1000 / (totalNetRawMaterialInput - finishedProdAddit);
                return c23 - c26;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public static Single GetOperationalAvailLostOpportunityValue(string conxString, DateTime startDate, string refNum, string dataSet,
    int factorSet, string scenario, string currency, string uOM, bool isFuels, Single oATarget)
        {
            try
            {
                Queries queries = new Queries(startDate, refNum, dataSet, factorSet, scenario, currency, uOM);

                //=AN70*(AN69-AN60)/100*AN61/(AN62/100)*1000*IF(YEAR(AN58)/4=INT(YEAR(AN58)/4),366,365)/1000
                /*
                = OA Lost Opportunity Value($/ Bbl) * (OA Target - OA) / 100 * Raw Material Input (kBPD) / (Utilization / 100) * 1000 * # days in year / 1000
                OA Lost Opportunity Value($/ Bbl)(5.86) = Gross Margin(7.39) - Variable Cash Opex(1.54)
                OA = Operational Availablity = select[OpAvail] from dbo.GenSum where submissionid = 12089
                 */
                Single operationalAvailLostOpportunityValueDollarsPerBbl = GetOperationalAvailLostOpportunityValueDollarsPerBbl(conxString, startDate, refNum, dataSet, factorSet, scenario, currency, uOM, isFuels);
             
                Single oA = GetDbValue(conxString, queries.GensumOpAvail());
                Single rawMaterialInputkBPD = GetDbValue(conxString, queries.GetLossValueRawMaterialInputkBPDSql());
                Single utilization = GetDbValue(conxString, queries.GensumUtilPcnt());
                int numDaysInYr = NumberOfDaysInThisYear(startDate.Year);
                Single returnValue = operationalAvailLostOpportunityValueDollarsPerBbl * (oATarget - oA) / 100 * rawMaterialInputkBPD / (utilization / 100) * 1000 * numDaysInYr / 1000;
                return returnValue;

            }
            catch (Exception ex)
            { throw ex; }
        }

        public static float GetUserDefinedYTD(string conxString,DateTime startDate,
             string header, string refNum, string dataSet,
            int factorSet, string scenario, string currency, string uOM)
        {
            float result = 0.0f;
            List<float> resultsList = new List<float>();
            try
            {
                //DateTime lastJanuary = new DateTime(startDate.Year, 1, 1);
                for(int monthToUse = 1; monthToUse <= startDate.Month;monthToUse++)
                {
                    DateTime temp = new DateTime(startDate.Year, monthToUse, 1);
                    DBHelper dbHelper = new DBHelper(conxString);
                    Queries queries = new Queries(temp, refNum, dataSet, factorSet, scenario, currency, uOM);
                    string sql = queries.UserDefinedThisMonth(header);
                    //DataTable dTable = dbHelper.GetDataTable(sql);
                    float tempResult = GetFloatDbValue(dbHelper, sql);
                    resultsList.Add(tempResult);
                }
                float resultsSum = 0.0f;
                foreach (float month in resultsList)
                {
                    resultsSum += month;
                }
                return resultsSum / startDate.Month;
            }
            catch(Exception ex)
            { throw ex; }
        }



        private static Single GetDbValue(string conxString, string sql)
        {
            try
            {
                Single result = 0;
                DBHelper _dbHelper = new DBHelper(conxString);
                DataTable dTable = _dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count > 0)
                    result = Single.Parse(dTable.Rows[0][0].ToString());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static long GetLongDbValue(string conxString, string sql)
        {
            try
            {
                long result = 0;
                DBHelper _dbHelper = new DBHelper(conxString);
                DataTable dTable = _dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count > 0)
                    result = long.Parse(dTable.Rows[0][0].ToString());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static long GetLongDbValue(DBHelper dbHelper, string sql)
        {
            try
            {
                long result = 0;
                DataTable dTable = dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count > 0)
                    result = long.Parse(dTable.Rows[0][0].ToString());
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static float GetFloatDbValue(DBHelper dbHelper, string sql)
        {
            try
            {
                float result = 0;
                DataTable dTable = dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count > 0)
                {
                    if (dTable.Rows[0][0] != DBNull.Value)
                        result = float.Parse(dTable.Rows[0][0].ToString());
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static Common.LossValueInfo LossValueInfoCalc(int daysInYear, int daysInMonth, Single avgRawMatCost,
            Single avgVariableCashOpEx, Single avgVariableNetCashMargin, Single tgtHydrocarbonLossPcntOfFeed,
            Single tgtOperationalAvailability, Single refineryNetMaterialInput, Single utilization,
            Single operationalAvailability, Single lossMassPctnOfFeed)
        {

            //Single currentMonthoperationalAvailability    now a param
            //Single currentMonthLossMassPctnOfFeed  now a param
            //Single currentMonthValuesTonne = 0;  doesn't seem to be used
            //Single currentMonthValuesUtilization = 0;  now a param
            //Single currentMonthValuesValueCalculations = 0;  doesn't seem to be used

            Common.LossValueInfo results = new Common.LossValueInfo();
            results.RefineryLossActualLoss = lossMassPctnOfFeed / 100 * refineryNetMaterialInput;
            results.RefineryLossTargetLoss = tgtHydrocarbonLossPcntOfFeed * refineryNetMaterialInput;
            results.RefineryLossValueOfLossPerBblFeed = avgRawMatCost + avgVariableCashOpEx;
            results.RefineryLossValueOfLossForTheMonthVersusTargetLoss = results.RefineryLossTargetLoss * results.RefineryLossValueOfLossPerBblFeed / 1000;
            results.RefineryLossAnnualizedValueOfLossVersusTarget = results.RefineryLossValueOfLossForTheMonthVersusTargetLoss * daysInYear / daysInMonth;

            results.LOAVTTargetOA = tgtOperationalAvailability * 100;
            results.LOAVTActualOA = operationalAvailability;
            results.LOAVTValueOfLostOAPerBbl = avgVariableNetCashMargin;
            results.LOAVTLostFeedEquivalentversusTargetOA = (results.LOAVTTargetOA - results.LOAVTActualOA) / 100 *
                (refineryNetMaterialInput / (utilization / 100));
            results.LOAVTValueOfLostOAForTheMonth = (results.LOAVTTargetOA - results.LOAVTActualOA) / 100 *
                (refineryNetMaterialInput / (utilization / 100)) * results.LOAVTValueOfLostOAPerBbl / 1000;
            results.LOAVTAnnualizedValueOfLostOA = results.LOAVTValueOfLostOAForTheMonth * daysInYear / daysInMonth;
            return results;
        }

        public static List<float> AverageOfYtdAnnualizedLossValueForMonth(List<float> yTDLossValues, int year)
        {
            //receive list of Loss Values starting with January of current year, ending with the month we
            //are reporting for.
            //Get Annualized Loss Value For (each) Month by multiplying each month * 12
            //Multipy each month's Annualized Loss Value For Month * # days in that month
            //Add all these, and divide by sum of count of days in each month 
            List<float> result = new List<float>();
            float thisMonthAverageOfYtdAnnualizedLossValueForMonth = 0;
            int countOFDaysInTheseMonths = 0;
            int countOFDaysInThisMonth = 0;
            try
            {
                int monthOrdinal = 1;
                foreach (float lossValueForMonth in yTDLossValues)
                {
                    float annualizedLossValueForMonth = lossValueForMonth * 12;
                    countOFDaysInThisMonth = numberOfDaysInThisMonth(monthOrdinal, year);
                    countOFDaysInTheseMonths += countOFDaysInThisMonth;
                    thisMonthAverageOfYtdAnnualizedLossValueForMonth += annualizedLossValueForMonth * countOFDaysInThisMonth;
                    result.Add(thisMonthAverageOfYtdAnnualizedLossValueForMonth / countOFDaysInTheseMonths);
                    monthOrdinal++;
                }                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static float LossValueVsTargetKDollars__2018_08_27(int yearToUse, float lossValue,
            float opAvail, float netInputBPD, float utilPcnt, float lossTarget,
            float rawMaterialCostUsdPerBblNetInput,
            float grossMarginUsdPerBblNetInput,
            float variableCashOpex)
        {
            //opAvail : not _YTD
            float result = 0;
            float lossValueDollarsPerBbl = 0;
            try
            {
                lossValueDollarsPerBbl = variableCashOpex + rawMaterialCostUsdPerBblNetInput;
                result = lossValueDollarsPerBbl * (lossValue - lossTarget) / 100 * netInputBPD * 1000 * NumberOfDaysInThisYear(yearToUse) / 1000;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static float OALostOpportunityValue__2018_08_27(int yearToUse, float lossValue,
            float opAvail, float netInputBPD, float utilPcnt, float lossTarget,
            float rawMaterialCostUsdPerBblNetInput,
            float grossMarginUsdPerBblNetInput,
            float oaTarget, float variableCashOpex)
        {
            //opAvail : not _YTD
            float result = 0;
            float oaLostOpportunityValueDollarsPerBbl = 0;
            try
            {
                oaLostOpportunityValueDollarsPerBbl = grossMarginUsdPerBblNetInput - variableCashOpex;
                //result = oaLostOpportunityValueDollarsPerBbl * (oaTarget - opAvail) / 100 * netInputBPD / (utilPcnt / 100) * 1000 * NumberOfDaysInThisYear(yearToUse) / 1000;
                result = oaLostOpportunityValueDollarsPerBbl * (oaTarget - opAvail) / 100 * netInputBPD / (opAvail / 100) * 1000 * NumberOfDaysInThisYear(yearToUse) / 1000;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }

        public static float ReadConfig(string setting)
        {
            try
            {
                float result = Single.Parse(ConfigurationManager.AppSettings[setting]);
                return result;
            }
            catch
            {
                throw new Exception("Unable to find Config setting " + setting);
            }
        }

        private static int numberOfDaysInThisMonth(int month, int year)
        {
            int result = 0;
            int count = 1;
            try
            {
                while (count <= 31)
                {
                    DateTime temp = new DateTime(year, month, count);
                    result = count;
                    count++;
                }
            }
            catch { }
            return result;
        }

        private static int NumberOfDaysInThisYear(int year)
        {
            int result = 0;
            int count = 1;
            try
            {
                while (count <= 12)
                {
                    result += numberOfDaysInThisMonth(count, year);
                    count++;
                }
            }
            catch { }
            return result;
        }


        public static bool VerifyOutput(ref Excel.Worksheet sheet)
        {
            //ensure things didn't get out of place
            ValidOutput output = new ValidOutput();
            int row = 1;
            foreach (string expectedValue in output.OutputFields)
            {
                string foundValue = sheet.Cells[row, 1].Value ?? "";
                if (foundValue.Trim() != expectedValue.Trim())
                    return false;
                row++;
            }
            return true;
        }
    }
}
