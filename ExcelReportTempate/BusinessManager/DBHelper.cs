﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace BusinessManager
{
    public class DBHelper
    {
        string _conxString = string.Empty;
        SqlConnection _conx = null;

        public DBHelper(string connectionString)
        {
            _conxString = connectionString;
            _conx = new SqlConnection(_conxString);
        }

        public System.Data.DataRow GetFirstRow(string sql)
        {
            try
            {
                DataSet ds = QueryDB(sql);
                if(ds!=null && ds.Tables.Count==1 && ds.Tables[0].Rows.Count==1)
                {
                    return ds.Tables[0].Rows[0];
                }
                return null;
            }
            catch(Exception ex)
            { throw ex; }
        }

        public DataTable GetDataTable(string sql)
        {
            try
            {
                DataSet ds = QueryDB(sql);
                if (ds != null && ds.Tables.Count == 1 && ds.Tables[0].Rows.Count >= 1)
                {
                    return ds.Tables[0];
                }
                return null;
            }
            catch (Exception ex)
            { throw ex; }
        }

        public DataSet QueryDB(string sql)
        {
            DataSet ds = new DataSet();
            try
            {
                using (_conx = new SqlConnection(_conxString))
                {
                    //if (sql.Trim().ToUpper().StartsWith("SELECT") || sql.Trim().ToUpper().StartsWith("EXEC"))
                    //{
                        SqlDataAdapter da = new SqlDataAdapter(sql, _conx);
                        da.Fill(ds);
                    //}
                }
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


    }
}
