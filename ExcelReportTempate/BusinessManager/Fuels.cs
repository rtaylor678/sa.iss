﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;
using ExcelService;
using System.Data;

namespace BusinessManager
{
    public class Fuels
    {
        string _conxString = System.Configuration.ConfigurationManager.ConnectionStrings["ProfileFuelsConnectionString"].ToString();
        string _conxStringDBS1 = System.Configuration.ConfigurationManager.ConnectionStrings["Refining14ConnectionString"].ToString();
        Excel.Application _xl = null;
        Excel.Workbook _wb = null;
        //Excel.Worksheet _ws = null;
        DateTime _startDate = DateTime.Today;
        DBHelper _dbHelper = null;
        DBHelper _dbHelperDBS1 = null;
        ExcelService.Refinery _xlFuels = null;
        string _refNum = "161EUR";
        Queries _routines = null;
        string _dataSet = "Actual";
        private int _factorSet = 2012;  //change from 2014 to 2012 per Bruce 2018-0-10-31
        private string _scenario = "CLIENT";
        private string _currency = "USD";
        private string _uOM = "MET";

        public Fuels(DateTime startDate)
        {
            _xl = new Microsoft.Office.Interop.Excel.Application();
            _xl.ScreenUpdating = true;
            _startDate = startDate;
            _dbHelper = new DBHelper(_conxString);
            _dbHelperDBS1 = new DBHelper(_conxStringDBS1);
            _routines = new Queries(_startDate,_refNum,_dataSet, _factorSet,_scenario, _currency, _uOM);
        }

        public void Run()
        {          
            _xlFuels = new ExcelService.Refinery();
            string conxString = System.Configuration.ConfigurationManager.ConnectionStrings["ProfileFuelsConnectionString"].ToString();
            string testPath = System.Configuration.ConfigurationManager.AppSettings["PathToFuelsReportTemplate"];
            _xlFuels.Open(ref _xl, ref _wb, System.Configuration.ConfigurationManager.AppSettings["PathToFuelsReportTemplate"]);
            Excel.Worksheet summary = _xlFuels.SetSheet(ref _xl, ref _wb, "Summary");
            Excel.Worksheet net = _xlFuels.SetSheet(ref _xl, ref _wb, "Net");
            net.Activate();
            _xlFuels.ClearSheet(ref net);
            string processEDC = _xlFuels.Read(ref summary, 12, 1);
            //Clear and then Add data to new Net sheet, month, 1 yr history, YTD, Trends, Avg, Target.

            //Enviro
            int xlrow = 3;
            Util.WriteUserDefinedByHeader(ref net, "ReportableEnvironmentalIncidents",ref  xlrow, conxString,_startDate,_refNum,_dataSet,_factorSet,_scenario,_currency,_uOM);

            //Safety   
            //NOTE: This probably won't be used, but I cant remove it because the charts must be hardcoded.
            Util.WriteUserDefinedByHeader(ref net, "SafetyLostTimeIncidents", ref xlrow, conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);

            //Hours            
            Util.WriteUserDefinedByHeader(ref net, "STHMaintHours", ref xlrow, conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.WriteUserDefinedByHeader(ref net, "ContractMaintHours", ref xlrow, conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.WriteUserDefinedByHeader(ref net, "GAMaintHours", ref xlrow, conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.WriteUserDefinedByHeader(ref net, "STHNonMaintHours",ref xlrow, conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.WriteUserDefinedByHeader(ref net, "ContractNonMaintHours", ref xlrow, conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.WriteUserDefinedByHeader(ref net, "GANonMaintHours", ref xlrow, conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);

            //mPEI
            List<string> mpeiFieldNames = new List<string>() { "MaintPEI" };
            WriteGenSumByHeader(ref net, mpeiFieldNames, ref xlrow);

            //quartile
            string quartileDbName = "Refining";
            string refListName = "EUR14";
            Single top2 = 0;
            Single tile1Break = 0;
            Single tile2Break = 0;
            Single tile3Break = 0;
            Single bottom2 = 0;

            DataTable dTable = _dbHelper.GetDataTable(_routines.GenSumThisMonth("MaintPEI", _factorSet, _scenario, _currency, _uOM));
            Single currentMonth = float.Parse(dTable.Rows[0][0].ToString());

            List<float> kpiPriorValues = new List<float>();
            //Console.WriteLine("loop thru prior 24 months and add these to kpiPriorValues list ");
            int kpiPriorValuesLine = FindRowWhereThisHeaderIs(  ref net, "MaintPEILast24Months");
            for (int c=2; c<=25;c++)
            {
                try
                {
                    kpiPriorValues.Add(Single.Parse(_xlFuels.Read(ref net, kpiPriorValuesLine, c)));
                }
                catch
                {
                    kpiPriorValues.Add(0);
                }               
            }
            string sql = _routines.Quartile(quartileDbName, "MaintPersEffIndex", refListName);
            dTable = _dbHelperDBS1.GetDataTable(sql);
            bool success = _routines.PopulateQuartiles(dTable.Rows[0], ref top2, ref tile1Break, ref tile2Break, ref tile3Break, ref bottom2);
            Common.QuartileResults qmPEI = _routines.GetQuartilesForKpi(top2, tile1Break, tile2Break, tile3Break, bottom2, currentMonth, "mPEI", kpiPriorValues);
            Common.QuartileResults qmPEIYTDRadar = _routines.GetQuartilesForKpi(top2, tile1Break, tile2Break, tile3Break, bottom2, currentMonth, "mPEI YTD Radar" , kpiPriorValues);
            WriteQuartiles(ref net, qmPEI, ref xlrow);

            List<string> nmpeiFieldNames = new List<string>() { "NonMaintPEI" };
            WriteGenSumByHeader(ref net, nmpeiFieldNames, ref xlrow);
            
            //nmPEI
            dTable = _dbHelper.GetDataTable(_routines.GenSumThisMonth("NonMaintPEI", _factorSet, _scenario, _currency, _uOM));
            currentMonth = float.Parse(dTable.Rows[0][0].ToString());
            kpiPriorValues = new List<float>();
            kpiPriorValuesLine = FindRowWhereThisHeaderIs(  ref net, "NonMaintPEILast24Months");
            //Console.WriteLine("loop thru prior 24 months and add these to kpiPriorValues list ");
            for (int c = 2; c <= 25; c++)
            {
                try
                {
                    kpiPriorValues.Add(Single.Parse(_xlFuels.Read(ref net, 72, c)));
                }
                catch
                {
                    kpiPriorValues.Add(0);
                }
            }
            sql = _routines.Quartile(quartileDbName, "NonMaintPersEffIndex", refListName);
            dTable = _dbHelperDBS1.GetDataTable(sql);
            success = _routines.PopulateQuartiles(dTable.Rows[0], ref top2, ref tile1Break, ref tile2Break, ref tile3Break, ref bottom2);
            Common.QuartileResults qnmPEI = _routines.GetQuartilesForKpi(top2, tile1Break, tile2Break, tile3Break, bottom2, currentMonth, "nmPEI", kpiPriorValues);
            Common.QuartileResults qnmPEIYTDRadar = _routines.GetQuartilesForYTD(top2, tile1Break, tile2Break, tile3Break, bottom2, currentMonth, "nmPEI YTD Radar", kpiPriorValues);
            WriteQuartiles(ref net, qnmPEI, ref xlrow);

            //====== this is where we get out of order =========================
            WriteQuartiles(ref net, qmPEIYTDRadar, ref xlrow);            
            WriteQuartiles(ref net, qnmPEIYTDRadar, ref xlrow);

            WriteLostTimeInjury(ref net, "Safety Lost Time Incidents", ref xlrow);

            //now write the current month+1 name, and then the following months for a through current month twice
            _xlFuels.Write(ref net, xlrow, 1, "24MonthNames");
            List<string> trailing24MonthNames = Util.Get24MonthNames(_startDate.Month);
            Util.WriteList(ref _xlFuels, ref net, trailing24MonthNames, ref xlrow);

            //Common.LossValueInfo lossValueInfo = Util.GetLossValue(_conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            
            SaveTargetInfo("LossValueTgt");
            SaveTargetInfo("OperationalAvailabilityTgt");

            
            //now we need these too !?!?!?!?  . . . pass them out as a param instead of rewrite all the rows again.
            List<Single> currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss = new List<Single>();
            List<Single> currentYearLOAVTValueOfLostOAForTheMonth = new List<Single>();

            //string yearForConfig = _startDate.Year.ToString();
            //if (yearForConfig == "2016" || yearForConfig == "2017")
            //    yearForConfig = "20162017";
            //string refineryTypeForConfig = "Fuels";
            //float rawMaterialCostUsdPerBblNetInput = Util.ReadConfig(refineryTypeForConfig + "rawMaterialCostUsdPerBblNetInput" + yearForConfig);
            //float grossMarginUsdPerBblNetInput = Util.ReadConfig(refineryTypeForConfig + "grossMarginUsdPerBblNetInput" + yearForConfig);
            //float lossTarget = Util.ReadConfig(refineryTypeForConfig + "lossTarget" + yearForConfig);
            //float oaTarget = Util.ReadConfig(refineryTypeForConfig + "oaTarget" + yearForConfig);
            //float hydrocarbonLossValue = Util.ReadConfig(refineryTypeForConfig + "hydrocarbonLossValue" + yearForConfig);
            //float variableCashOpex = Util.ReadConfig(refineryTypeForConfig + "variableCashOpex" + yearForConfig);

            Util.WriteLossValueInfoByHeader(ref net, "LossValue", ref xlrow, _conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM,
                ref currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss, ref currentYearLOAVTValueOfLostOAForTheMonth, true);
            List<Single> discard =new List<Single>();
            List<Single> discard2 = new List<Single>();
            
            //Util.WriteLossValueInfoByHeader(ref net, "OperationalAvailability", ref xlrow, _conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM, ref discard, ref discard2,true);
            Util.WriteOperationalAvailInfoByHeader(ref net, "OperationalAvailability", ref xlrow, _conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM, ref discard, ref discard2, true);
            
            Util.Write24MonthTargets(ref net, ref xlrow,"dbo.GenSum", "MaintPEI_Target" , _conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.Write24MonthTargets(ref net, ref xlrow, "dbo.GenSum", "NonMaintPEI_Target" , _conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.Write24MonthUserDefinedTargets(ref net, ref xlrow, "ReportableEnvironmentalIncidents", _conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.Write24MonthUserDefinedTargets(ref net, ref xlrow, "SafetyLostTimeIncidents", _conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.Write24MonthUserDefinedTargets(ref net, ref xlrow, "LossValueTgt", _conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            Util.Write24MonthUserDefinedTargets(ref net, ref xlrow, "OperationalAvailabilityTgt", _conxString, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);


            //need curretn mo, currenty yr, last 12, last 24,YTD current yr (calc)
            /*
            Common.InjuryInfo injuryInfo =Util.WriteLostTime(ref net,  xlrow,_dbHelper,"","",_startDate,_startDate, _refNum,_dataSet,_factorSet,_scenario,_currency,_uOM);
            _xlFuels.Write(ref net, xlrow, 1, "LostTimeInjuryRateForMonth");
            _xlFuels.Write(ref net, xlrow, 2, injuryInfo.LostTimeInjuryRateForMonth.ToString());
            xlrow++;
            _xlFuels.Write(ref net, xlrow, 1, "LostTimeInjuryRateYTD");
            _xlFuels.Write(ref net, xlrow, 2, injuryInfo.LostTimeInjuryRateYTD.ToString());
            xlrow++;

            Common.EnviroInfo enviroInfo = Util.GetEnviroInfo(_dbHelper, _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            _xlFuels.Write(ref net, xlrow, 1, "EnviroIncidentRateForMonth");
            _xlFuels.Write(ref net, xlrow, 2, enviroInfo.EnviroIncidentRateForMonth.ToString());
            xlrow++;
            _xlFuels.Write(ref net, xlrow, 1, "EnviroIncidentRateYTD");
            _xlFuels.Write(ref net, xlrow, 2, enviroInfo.EnviroIncidentRateYTD.ToString());
            xlrow++;
            */

            Util.WriteLostTime(ref net,
                ref xlrow, _dbHelper, "SafetyLostTimeIncidents", 
                _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            xlrow++;
            Util.WriteEnviroIncidents(ref net,
                ref xlrow, _dbHelper, "ReportableEnvironmentalIncidents",
                _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);

             Util.WriteLostTimeLastCalendarYear(ref net,
                ref xlrow, _dbHelper, "SafetyLostTimeIncidents", 
                _startDate, _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);

            //To Do 
            //now, find all the current year's  (jan current year throuhg end of data) #s for these (populated above):
            //  currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss , currentYearLOAVTValueOfLostOAForTheMonth
            //then, figure out the YTD for all these #s. Simple? Weighted somehow?
            //then, put the final YTD here:
            //Summary sheet:
            //  iii.	Value of Loss versus Target Loss (row 21)  (currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss)
            //iv.	Value of Lost OA versus Target (row 41)  (currentYearLOAVTValueOfLostOAForTheMonth)


            xlrow++;
            //List<float> WhateverWritesThis = null; // " = Net!$B$84:$M$84"   "LossValueLastCalendarYear" is it :: ??             currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss
            _xlFuels.Write(ref net, xlrow, 1, "AverageOfYtdAnnualizedLossValueForMonth");
            List<float> averageOfYtdAnnualizedLossValueForMonth = Util.AverageOfYtdAnnualizedLossValueForMonth(currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss, _startDate.Year);
            Util.WriteListToRow(ref net, averageOfYtdAnnualizedLossValueForMonth, ref xlrow);

            //current month in currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss
            _xlFuels.Write(ref net, xlrow, 1, "CurrentMonth_RefineryLossValueOfLossForTheMonthVersusTargetLoss");
            //not used anymore _xlFuels.Write(ref net, xlrow,2, currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss[currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss.Count-1].ToString());

            //ytd  currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss
            xlrow++;
            _xlFuels.Write(ref net, xlrow, 1, "CurrentYear_RefineryLossValueOfLossForTheMonthVersusTargetLoss");
            Util.WriteListToRow(ref net, currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss, ref xlrow);

            //xlrow++;
            _xlFuels.Write(ref net, xlrow, 1, "AverageOfYtdAnnualizedLOAVTValueOfLostOAForTheMonth");
            List<float> averageOfYtdAnnualizedLOAVTValueOfLostOAForTheMonth = Util.AverageOfYtdAnnualizedLossValueForMonth(currentYearLOAVTValueOfLostOAForTheMonth, _startDate.Year);
            Util.WriteListToRow(ref net, averageOfYtdAnnualizedLOAVTValueOfLostOAForTheMonth, ref xlrow);

            // current month  in currentYearLOAVTValueOfLostOAForTheMonth 
            _xlFuels.Write(ref net, xlrow, 1, "CurrentMonth_LOAVTValueOfLostOAForTheMonth");
            //not used anymore _xlFuels.Write(ref net, xlrow, 2, currentYearLOAVTValueOfLostOAForTheMonth[currentYearLOAVTValueOfLostOAForTheMonth.Count - 1].ToString());

            //ytd  currentYearRefineryLossValueOfLossForTheMonthVersusTargetLoss
            xlrow++;
            _xlFuels.Write(ref net, xlrow, 1, "CurrentYear__LOAVTValueOfLostOAForTheMonth");
            Util.WriteListToRow(ref net, currentYearLOAVTValueOfLostOAForTheMonth, ref xlrow);

           
            _xlFuels.Write(ref net, xlrow, 1, "SafetyLostTimeIncidentsYTD");
            float ytd = Util.GetUserDefinedYTD(conxString, _startDate, "SafetyLostTimeIncidents", 
                _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            _xlFuels.Write(ref net, xlrow, 2, ytd.ToString());

            xlrow++;
            _xlFuels.Write(ref net, xlrow, 1, "ReportableEnvironmentalIncidentsYTD");
            ytd = Util.GetUserDefinedYTD(conxString, _startDate, "ReportableEnvironmentalIncidents",
                _refNum, _dataSet, _factorSet, _scenario, _currency, _uOM);
            _xlFuels.Write(ref net, xlrow, 2, ytd.ToString());

            _xlFuels.Save(ref _wb);
            if (!Util.VerifyOutput(ref net))
                throw new Exception("Output order of headings is no longer valid");
            _xlFuels = null;
        }

        private bool SaveTargetInfo(string headerText)
        {
            try
            {
                string HeaderText = string.Empty;
                string sql = _routines.GetSubmissionID();
                //see if already populated or not
                DataRow row = _dbHelper.GetFirstRow(sql);
                sql = "Select HeaderText from ProfileFuels12.dbo.UserDefined where ";
                sql += "SubmissionID = " + row[0].ToString();
                sql += " and UPPER(HeaderText) = '" + headerText.ToUpper().Trim() + "';";
                try
                {
                    DataSet ds = _dbHelper.QueryDB(sql);
                    HeaderText = ds.Tables[0].Rows[0][0].ToString();
                }
                catch (Exception ex)
                {
                    HeaderText = string.Empty;
                }
                if (HeaderText.Length > 0)
                    return true; //populated, no need to insert

                string configValue = string.Empty;
                if(headerText.ToUpper()=="LOSSVALUETGT")
                {
                    configValue = ConfigurationManager.AppSettings["FuelsLostOpportunityTarget"].ToString();
                }
                else if(headerText.ToUpper() == "OPERATIONALAVAILABILITYTGT")
                {
                    configValue = ConfigurationManager.AppSettings["FuelsOperationalAvailabilityTarget"].ToString();
                }

                float tgt = float.Parse(configValue);
                sql = _routines.InsertUserDefined(int.Parse(row[0].ToString()), headerText, headerText, 0, 0, tgt, 0, 0);
                try
                {
                    _dbHelper.QueryDB(sql);
                }
                catch { }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public int FindRowWhereThisHeaderIs(ref Excel.Worksheet sheet, string cellText)
        {
            int result = -1;
            for (int i = 1; i < 1000; i++)
            {
                string txt = string.Empty;
                try
                {
                    txt = _xlFuels.Read(ref sheet, i, 1).ToString().Trim();
                }
                catch (Exception ex)
                {
                    string breakpoint = string.Empty;
                }
                if (txt != null)
                {
                    if (txt == cellText)
                    {
                        result = i;
                        break;
                    }
                }
            }
            return result;
        }

        private bool WriteQuartiles(ref Excel.Worksheet sheet, Common.QuartileResults values, ref int row)
        {
            try
            {
                _xlFuels.Write(ref sheet, row, 1, values.KpiName + " Quartiles");
                _xlFuels.Write(ref sheet, row, 2, values.top2);
                _xlFuels.Write(ref sheet, row, 3, values.tile1Break);
                _xlFuels.Write(ref sheet, row, 4, values.tile2Break);
                _xlFuels.Write(ref sheet, row, 5, values.tile3Break);
                _xlFuels.Write(ref sheet, row, 6, values.bottom2);
                _xlFuels.Write(ref sheet, row, 7, values.currentMonth);
                _xlFuels.Write(ref sheet, row, 8, values.RollingAvg);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                row += 1;
            }
        }

        private bool WriteGenSumByHeader(ref Excel.Worksheet sheet, List<string> fieldNames, ref int startXlsRowNumber)
        {
            try
            {
                int count = 0;
                //mPEI current month
                //string sql = _routines.GenSum("MaintPEI, MaintPEI_Avg, MaintPEI_YTD", _factorSet, _scenario, _currency, _uOM);
                string fieldName = fieldNames[0]; // "MaintPEI";
                string sql = _routines.GenSumThisMonth(fieldName,_factorSet,_scenario,_currency,_uOM);
                DataTable dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber, 1, fieldName + "CurrentMonth");
                WriteRows(ref sheet, dTable, startXlsRowNumber);
                count++;

                //mPEI Current Year
                sql = _routines.GenSumCurrentYear(fieldName, _factorSet, _scenario, _currency, _uOM);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, fieldName + "CurrentYear");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                sql = _routines.GenSumLastCalendarYear(fieldName, _factorSet, _scenario, _currency, _uOM);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, fieldName + "LastCalendarYear");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                //prior year
                sql = _routines.GenSumLast12Months(fieldName, _factorSet, _scenario, _currency, _uOM);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, fieldName + "Last12Months");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                //mPEI prior 2 years
                sql = _routines.GenSumLast24Months(fieldName, _factorSet, _scenario, _currency, _uOM);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, fieldName + "Last24Months");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                sql = _routines.GenSumCurrentYear(fieldName + "_YTD", _factorSet, _scenario, _currency, _uOM);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, fieldName + "_YTD");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                sql = _routines.GenSumCurrentYear(fieldName + "_Target", _factorSet, _scenario, _currency, _uOM);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, fieldName + "_Target");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;
                /*
                //mPEITarget
                string header = "mPEITarget";
                //xlrow += 1;
                sql = _routines.UserDefinedTargets(header);
                dTable = _dbHelper.GetDataTable(sql);
                _xlLubes.Write(ref net, xlrow, 1, header + "Target");
                WriteRows(ref net, dTable, xlrow, 24);
                */


                sql = _routines.GenSumCurrentYear(fieldName + "_Avg", _factorSet, _scenario, _currency, _uOM);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, fieldName + "_Avg");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;
                
                startXlsRowNumber = startXlsRowNumber + count;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        private bool WriteLostTimeInjury(ref Excel.Worksheet sheet, string header, ref int startXlsRowNumber)
        {//#########  THIS IS OBSOLETE  #########################
            try
            {
                /*
                int count = 0;
                Single currentMonthTotalPersonnelWorkhours = 0;
                int currentMonthLostTimeInjuries = 0;
                string sql = _routines.GetTotalPersonnelWorkoursForMonthSql();
                DataTable dTable = _dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count == 1 &&  dTable.Rows[0][0] != DBNull.Value)
                {
                    object test = dTable.Rows[0][0];
                    System.Diagnostics.Debug.WriteLine(test.ToString());
                    currentMonthTotalPersonnelWorkhours = Single.Parse(dTable.Rows[0][0].ToString());
                }
                sql = _routines.CurrentMonthLostTimeInjuriesSql();
                dTable = _dbHelper.GetDataTable(sql);
                if (dTable != null && dTable.Rows.Count == 1 && dTable.Rows[0][0] != DBNull.Value)
                {
                    currentMonthLostTimeInjuries = int.Parse(dTable.Rows[0][0].ToString());
                }

                //month Lost Time Injury Rate (i.e. Lost Time Injuries per 200,000 workhours)
                //if()
                Single twoHundredThousandWorkHours = currentMonthTotalPersonnelWorkhours / 200000;
                Single lostTimeInjuryRate =0;
                if (twoHundredThousandWorkHours>0)
                    lostTimeInjuryRate=currentMonthLostTimeInjuries / twoHundredThousandWorkHours;
                */
                _xlFuels.Write(ref sheet, startXlsRowNumber, 1, header + "CurrentMonth-OBSOLETE");
                //OBSOLETE  WriteList(ref sheet, new List<float>() { lostTimeInjuryRate }, startXlsRowNumber);
                startXlsRowNumber++;
                /*
                //YTD Lost Time Injury Rate – This is 
                //      Sum (monthly Lost Time Injuries)/Sum(monthly Total Personnel Workhours) for the current year.
                Single currentYearTotalPersonnelWorkhoursSqlSum = 0;
                int currentYearLostTimeInjuriesSum = 0;
                sql = _routines.GetTotalPersonnelWorkoursYtdSql();
                dTable = _dbHelper.GetDataTable(sql);
                if (dTable != null) // && dTable.Rows.Count == 1)
                {
                    foreach (DataRow row in dTable.Rows)
                    {
                        if(row[0] != DBNull.Value)
                            currentYearTotalPersonnelWorkhoursSqlSum += Single.Parse(row[0].ToString());
                    }
                }
                sql = _routines.CurrentYearLostTimeInjuriesSql();
                dTable = _dbHelper.GetDataTable(sql);
                if (dTable != null) // && dTable.Rows.Count == 1)
                {
                    foreach (DataRow row in dTable.Rows)
                    {
                        if (row[0] != DBNull.Value)
                            currentYearLostTimeInjuriesSum += int.Parse(row[0].ToString());
                    }
                }
                Single yTDLostTimeInjuryRate = 0;
                if (currentYearTotalPersonnelWorkhoursSqlSum > 0)
                    yTDLostTimeInjuryRate = currentYearLostTimeInjuriesSum / currentYearTotalPersonnelWorkhoursSqlSum;
                */
                _xlFuels.Write(ref sheet, startXlsRowNumber, 1, header + "YTD-OBSOLETE");
                //OBSOLETE  WriteList(ref sheet, new List<float>() { yTDLostTimeInjuryRate }, startXlsRowNumber);
                startXlsRowNumber++;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*
        private bool WriteUserDefinedByHeader9(ref Excel.Worksheet sheet, string header, ref int startXlsRowNumber)
        {
            try
            {
                int count = 0;
                string sql = _routines.UserDefinedThisMonth(header);
                DataTable dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber, 1, header + "CurrentMonth");
                WriteRows(ref sheet, dTable, startXlsRowNumber);
                count++;

                sql = _routines.UserDefinedCurrentYear(header);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, header + "CurrentYear");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;
                                
                sql = _routines.UserDefinedLastCalendarYear(header);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, header + "LastCalendarYear");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count, 12);
                count++;

                sql = _routines.UserDefinedLast12Months(header);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1, header + "Last12Months");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count,12);
                count++;

                sql = _routines.UserDefinedLast24Months(header);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber + count, 1,header+ "Last24Months");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count,24);
                count++;

                sql = _routines.UserDefinedTargets(header);
                dTable = _dbHelper.GetDataTable(sql);
                _xlFuels.Write(ref sheet, startXlsRowNumber+ count, 1,header+"Target");
                WriteRows(ref sheet, dTable, startXlsRowNumber + count);
                count++;

                sql = _routines.UserDefinedCurrentYear(header);
                dTable = _dbHelper.GetDataTable(sql);
                List<float> lst = AvgFromRows(dTable, "RptValue");
                _xlFuels.Write(ref sheet, startXlsRowNumber+ count, 1,header+"Avg");
                WriteList(ref sheet, lst, startXlsRowNumber + count);
                count++;

                
                //YTD is just a set of charts that start in THIS YEAR and end THIS YEAR, it is not a sum of the values so far.
                //sql = _routines.UserDefinedCurrentYear(header);
                //dTable = _dbHelper.GetDataTable(sql);
                //lst = YtdFromRows(dTable, "RptValue");
                //_xlLubes.Write(ref sheet, startXlsRowNumber+ count, 1,header+"Ytd");
                //WriteList(ref sheet, lst, startXlsRowNumber + count);
                //count++;
                
                startXlsRowNumber = startXlsRowNumber + count;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */
        private List<Single> AvgFromRows(DataTable dtable, string fieldName)
        {//use where have multiple values on diff datarows
            List<Single> results = new List<Single>();
            Single sum = 0;
            try
            {
                if (dtable != null && dtable.Rows.Count > 0)
                {
                    foreach (DataRow row in dtable.Rows)
                    {
                        if (row[fieldName] != DBNull.Value)
                        {
                            Single val = Single.Parse(row[fieldName].ToString());
                            //Single val = (Single)row[fieldName];
                            sum += val;
                            int denominator = results.Count + 1;
                            Single avg = sum / denominator;
                            if (avg.ToString() == "NaN")
                                avg = 0;
                            results.Add(avg);
                        }
                        else
                        {
                            Single val =0;
                            sum += val;
                            Single avg = sum / results.Count;
                            results.Add(avg);
                        }                      
                    }
                }
                return results;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public bool WriteLine(ref Excel.Worksheet sheet, DataRow drow, int xlrow )
        {   //use when you only get 1 row and you want to write each column horizontally
            try
            {
                if (drow != null)
                {
                    object[] arr = drow.ItemArray;
                    for (int c = 0; c < arr.Length; c++)
                    {
                        _xlFuels.Write(ref sheet, xlrow, c+2, drow[c].ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool WriteRows(ref Excel.Worksheet sheet, DataTable dTable, string columnName, int xlrow, int minRows = 0)
        {   //use when your query returns multipoe rows that you want to write horizontally

            //use minRows to pad missing data with 0s

            try
            {
                int pad = 0;
                int c = 2;
                if (minRows > 0)
                {
                    if (dTable == null)
                    {
                        pad = minRows;
                    }
                    else
                    {
                        pad = minRows - dTable.Rows.Count;
                    }
                }
                while (pad > 1)
                {
                    _xlFuels.Write(ref sheet, xlrow, c, "0");
                    pad -= 1;
                    c += 1;
                }

                if (dTable != null)
                {
                    for (int drow = 0; drow < dTable.Rows.Count; drow++)
                    {
                        _xlFuels.Write(ref sheet, xlrow, c + drow, dTable.Rows[drow][columnName].ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool WriteRows(ref Excel.Worksheet sheet, DataTable dTable, int xlrow, int minRows = 0)
        {   //use when your query returns multipoe rows that you want to write horizontally

            //use minRows to pad missing data with 0s

            try
            {
                int pad = 0;
                int c = 2;
                if (minRows > 0)
                {
                    if (dTable == null)
                    {
                        pad = minRows;
                    }
                    else
                    {
                        pad = minRows - dTable.Rows.Count;
                    }
                }
                while (pad > 0)
                {
                    _xlFuels.Write(ref sheet, xlrow, c, "0");
                    pad -= 1;
                    c += 1;
                }
                
                if (dTable != null)
                {
                    for (int drow = 0; drow < dTable.Rows.Count; drow++)
                    {
                        _xlFuels.Write(ref sheet, xlrow, c + drow, dTable.Rows[drow][0].ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool WriteList(ref Excel.Worksheet sheet, List<float> lst, int xlrow)
        {
            try
            {
                if (lst != null)
                {
                    int c = 2;
                    for (int lrow = 0; lrow < lst.Count; lrow++)
                    {
                        _xlFuels.Write(ref sheet, xlrow, c + lrow, lst[lrow].ToString());
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        ~Fuels()
        {
            try
            {
                _wb.Close(true, Type.Missing, Type.Missing);
            }
            catch { }
            try
            {
                _xl.Quit();
                try
                {
                    _xl = null;
                }
                catch { }
            }
            catch { }
        }
    }
}
