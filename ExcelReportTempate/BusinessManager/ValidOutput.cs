﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessManager
{
    internal class ValidOutput
    {
        public List<string> OutputFields = new List<string>();
        public ValidOutput()
        {
            OutputFields.Add("");
            OutputFields.Add("");
            //OutputFields.Add("ReportableEnvironmentalIncidentsCurrentMonth");
            OutputFields.Add("ReportableEnvironmentalIncidentsCurrentMonth");
            OutputFields.Add("ReportableEnvironmentalIncidentsCurrentYear");
            OutputFields.Add("ReportableEnvironmentalIncidentsLastCalendarYear");
            OutputFields.Add("ReportableEnvironmentalIncidentsLast12Months");
            OutputFields.Add("ReportableEnvironmentalIncidentsLast24Months");
            OutputFields.Add("ReportableEnvironmentalIncidentsTarget");
            OutputFields.Add("ReportableEnvironmentalIncidentsAvg");
            OutputFields.Add("SafetyLostTimeIncidentsCurrentMonth");
            OutputFields.Add("SafetyLostTimeIncidentsCurrentYear");
            OutputFields.Add("SafetyLostTimeIncidentsLastCalendarYear");
            OutputFields.Add("SafetyLostTimeIncidentsLast12Months");
            OutputFields.Add("SafetyLostTimeIncidentsLast24Months");
            OutputFields.Add("SafetyLostTimeIncidentsTarget");
            OutputFields.Add("SafetyLostTimeIncidentsAvg");
            OutputFields.Add("STHMaintHoursCurrentMonth");
            OutputFields.Add("STHMaintHoursCurrentYear");
            OutputFields.Add("STHMaintHoursLastCalendarYear");
            OutputFields.Add("STHMaintHoursLast12Months");
            OutputFields.Add("STHMaintHoursLast24Months");
            OutputFields.Add("STHMaintHoursTarget");
            OutputFields.Add("STHMaintHoursAvg");
            OutputFields.Add("ContractMaintHoursCurrentMonth");
            OutputFields.Add("ContractMaintHoursCurrentYear");
            OutputFields.Add("ContractMaintHoursLastCalendarYear");
            OutputFields.Add("ContractMaintHoursLast12Months");
            OutputFields.Add("ContractMaintHoursLast24Months");
            OutputFields.Add("ContractMaintHoursTarget");
            OutputFields.Add("ContractMaintHoursAvg");
            OutputFields.Add("GAMaintHoursCurrentMonth");
            OutputFields.Add("GAMaintHoursCurrentYear");
            OutputFields.Add("GAMaintHoursLastCalendarYear");
            OutputFields.Add("GAMaintHoursLast12Months");
            OutputFields.Add("GAMaintHoursLast24Months");
            OutputFields.Add("GAMaintHoursTarget");
            OutputFields.Add("GAMaintHoursAvg");
            OutputFields.Add("STHNonMaintHoursCurrentMonth");
            OutputFields.Add("STHNonMaintHoursCurrentYear");
            OutputFields.Add("STHNonMaintHoursLastCalendarYear");
            OutputFields.Add("STHNonMaintHoursLast12Months");
            OutputFields.Add("STHNonMaintHoursLast24Months");
            OutputFields.Add("STHNonMaintHoursTarget");
            OutputFields.Add("STHNonMaintHoursAvg");
            OutputFields.Add("ContractNonMaintHoursCurrentMonth");
            OutputFields.Add("ContractNonMaintHoursCurrentYear");
            OutputFields.Add("ContractNonMaintHoursLastCalendarYear");
            OutputFields.Add("ContractNonMaintHoursLast12Months");
            OutputFields.Add("ContractNonMaintHoursLast24Months");
            OutputFields.Add("ContractNonMaintHoursTarget");
            OutputFields.Add("ContractNonMaintHoursAvg");
            OutputFields.Add("GANonMaintHoursCurrentMonth");
            OutputFields.Add("GANonMaintHoursCurrentYear");
            OutputFields.Add("GANonMaintHoursLastCalendarYear");
            OutputFields.Add("GANonMaintHoursLast12Months");
            OutputFields.Add("GANonMaintHoursLast24Months");
            OutputFields.Add("GANonMaintHoursTarget");
            OutputFields.Add("GANonMaintHoursAvg");
            OutputFields.Add("MaintPEICurrentMonth");
            OutputFields.Add("MaintPEICurrentYear");
            OutputFields.Add("MaintPEILastCalendarYear");
            OutputFields.Add("MaintPEILast12Months");
            OutputFields.Add("MaintPEILast24Months");
            OutputFields.Add("MaintPEI_YTD");
            OutputFields.Add("MaintPEI_Target");
            OutputFields.Add("MaintPEI_Avg");
            OutputFields.Add("mPEI Quartiles");
            OutputFields.Add("NonMaintPEICurrentMonth");
            OutputFields.Add("NonMaintPEICurrentYear");
            OutputFields.Add("NonMaintPEILastCalendarYear");
            OutputFields.Add("NonMaintPEILast12Months");
            OutputFields.Add("NonMaintPEILast24Months");
            OutputFields.Add("NonMaintPEI_YTD");
            OutputFields.Add("NonMaintPEI_Target");
            OutputFields.Add("NonMaintPEI_Avg");
            OutputFields.Add("nmPEI Quartiles");
            OutputFields.Add("mPEI YTD Radar Quartiles");
            OutputFields.Add("nmPEI YTD Radar Quartiles");
            OutputFields.Add("Safety Lost Time IncidentsCurrentMonth-OBSOLETE");
            OutputFields.Add("Safety Lost Time IncidentsYTD-OBSOLETE");
            OutputFields.Add("24MonthNames");
            OutputFields.Add("LossValueCurrentMonth");
            OutputFields.Add("LossValueCurrentYear");
            OutputFields.Add("LossValueLastCalendarYear");
            OutputFields.Add("LossValueLast12Months");
            OutputFields.Add("LossValueLast24Months");
            OutputFields.Add("LossValueYTD");
            OutputFields.Add("LossValueTarget");
            OutputFields.Add("LossValueAverage");
            OutputFields.Add("OperationalAvailabilityCurrentMonth");
            OutputFields.Add("OperationalAvailabilityCurrentYear");
            OutputFields.Add("OperationalAvailabilityLastCalendarYear");
            OutputFields.Add("OperationalAvailabilityLast12Months");
            OutputFields.Add("OperationalAvailabilityLast24Months");
            OutputFields.Add("OperationalAvailabilityYTD");
            OutputFields.Add("OperationalAvailabilityTarget");
            OutputFields.Add("OperationalAvailabilityAverage");
            OutputFields.Add("MaintPEI_TargetLast24Targets");
            OutputFields.Add("NonMaintPEI_TargetLast24Targets");
            //OutputFields.Add("ReportableEnvironmentalIncidentsLast24Targets");
            OutputFields.Add("ReportableEnvironmentalIncidentsLast24Targets");
            OutputFields.Add("SafetyLostTimeIncidentsLast24Targets");
            OutputFields.Add("LossValueTgtLast24Targets");
            OutputFields.Add("OperationalAvailabilityTgtLast24Targets");
            //OutputFields.Add("LostTimeInjuryRateForMonth");
            //OutputFields.Add("LostTimeInjuryRateYTD");
            //OutputFields.Add("EnviroIncidentRateForMonth");
            //OutputFields.Add("EnviroIncidentRateYTD");
            OutputFields.Add("LostTimeInjuryRateForMonthCurrentMonth");
            OutputFields.Add("LostTimeInjuryRateForMonthYTD");
            OutputFields.Add("LostTimeInjuryRateForCurrentYear");
            OutputFields.Add("LostTimeInjuryRateForCurrentYearYTD");
            OutputFields.Add("LostTimeInjuryRateLast12Months");
            OutputFields.Add("LostTimeInjuryRateLast12MonthsYTD");
            OutputFields.Add("LostTimeInjuryRateLast24Months");
            OutputFields.Add("LostTimeInjuryRateLast24MonthsYTD");
            OutputFields.Add("EnviroIncidentRateForMonthCurrentMonth");
            OutputFields.Add("EnviroIncidentRateForMonthYTD");
            OutputFields.Add("EnviroIncidentRateForCurrentYear");
            OutputFields.Add("EnviroIncidentRateForCurrentYearYTD");
            OutputFields.Add("EnviroIncidentRateLast12Months");
            OutputFields.Add("EnviroIncidentRateLast12MonthsYTD");
            OutputFields.Add("EnviroIncidentRateLast24Months");
            OutputFields.Add("EnviroIncidentRateLast24MonthsYTD");
            OutputFields.Add("LostTimeInjuryRateLastCalendarYear");
            OutputFields.Add("AverageOfYtdAnnualizedLossValueForMonth");
            OutputFields.Add("CurrentMonth_RefineryLossValueOfLossForTheMonthVersusTargetLoss");
            OutputFields.Add("CurrentYear_RefineryLossValueOfLossForTheMonthVersusTargetLoss");
            OutputFields.Add("AverageOfYtdAnnualizedLOAVTValueOfLostOAForTheMonth");
            OutputFields.Add("CurrentMonth_LOAVTValueOfLostOAForTheMonth");
            OutputFields.Add("CurrentYear__LOAVTValueOfLostOAForTheMonth");
            OutputFields.Add("SafetyLostTimeIncidentsYTD");
            OutputFields.Add("ReportableEnvironmentalIncidentsYTD");




            OutputFields.Add(""); //always make sure we have run out of labels
        }
    }
}
